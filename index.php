<?php

require_once('assets/init.php');

if (!empty($_GET['ref']) && $sh['loggedin'] == false && !isset($_COOKIE['src'])) {
    $get_ip = get_ip_address();
    if (!isset($_SESSION['ref']) && !empty($get_ip)) {
        $_GET['ref'] = Sh_Secure($_GET['ref']);
        $ref_user_id = Sh_UserIdFromUsername($_GET['ref']);
        $user_data = Sh_UserData($ref_user_id);
        if (!empty($user_data)) {
            if (ip_in_range($user_data['ip_address'], '/24') === false && $user_data['ip_address'] != $get_ip) {
                $_SESSION['ref'] = $user_data['username'];
            }
        }
    }
}
if (!isset($_COOKIE['src'])) {
    @setcookie('src', '1', time() + 31556926, '/');
}
$page = '';

if ($sh['loggedin'] == true && !isset($_GET['link1'])) {
    $page = 'home';
} elseif (isset($_GET['link1'])) {
    $page = $_GET['link1'];
}



if ((!isset($_GET['link1']) && $sh['loggedin'] == false) || (isset($_GET['link1']) && $sh['loggedin'] == false && $page == 'home')) {
    $page = 'welcome';
}


if ($sh['loggedin'] == true) {


    switch ($page) {
      case 'welcome':
        include('sources/welcome.php');
        break;
      case 'home':
        include('sources/welcome.php');
        break;
      case 'logout':
        include('sources/logout.php');
        break;
      case 'user-form':
        include('sources/user-form.php');
        break;
      case '404':
        include('sources/404.php');
        break;
      case 'search-experts':
        include('sources/search-experts.php');
        break;
      case 'timeline':
        include('sources/timeline.php');
        break;
      case 'review':
        include('sources/review.php');
        break;
      case 'activate':
          include('sources/activate.php');
          break;
      case 'unusual-login':
          include('sources/unusual-login.php');
          break;
      case 'contact-us':
          include('sources/contact-us.php');
          break;

    }

}else{

  switch ($page) {
    case 'welcome':
      include('sources/welcome.php');
      break;
    case 'register':
      include('sources/register.php');
      break;
    case 'login':
      include('sources/login.php');
      break;
    case 'user-form':
      include('sources/user-form.php');
      break;
    case '404':
      include('sources/404.php');
      break;
    case 'search-experts':
      include('sources/search-experts.php');
      break;
    case 'timeline':
      include('sources/timeline.php');
      break;
    case 'activate':
        include('sources/activate.php');
        break;
    case 'forgot-password':
        include('sources/forgot-password.php');
        break;
    case 'reset-password':
        include('sources/reset-password.php');
        break;
    case 'unusual-login':
        include('sources/unusual-login.php');
        break;
    case 'contact-us':
        include('sources/contact-us.php');
        break;
  }

}

if ( empty($sh['content']) ) {
  include('sources/404.php');
}


if($page == "register" || $page == "login" || $page == "activate" || $page == "activated" || $page == "reset-password" || $page == "unusual-login" || $page == "forgot-password"){

    echo Sh_LoadPage('login_container');

}else {
    echo Sh_LoadPage('container');
}


mysqli_close($sqlConnect);
unset($sh);
?>
