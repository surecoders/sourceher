<?php

if ( $f == 'form_builder'  ) {

  // Create Form
  if ($s == 'create_form') {

    $error = 0;

    // Regular inputs
    $form_name = Sh_Secure($_POST['form_name']);
    $page_type = Sh_Secure($_POST['page_type']);
    $user_id = Sh_Secure($_POST['user_id']);


    if ($user_id == $sh['user']['user_id']) {

      if ( $form_name == "" || empty($form_name) ) {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['form_name_not_empty']
        );
        $error = 1;

      }


      if ($error == 0) {

        $fcode = Sh_GenerateKey(8, 10, false, false, true, false);

        $formData = array(
          'form_code' => $fcode,
          'title' => $form_name,
          'page_type' => $page_type,
          'description' => "",
          'status' => 1,
          'fname' => Sh_Slugify($form_name),
          'formData' => ''
        );

        $formTable = array(
          'form_code' => $fcode,
          'title' => $form_name,
          'page_name' => strtolower(getPageTypeValue('name',$page_type)),
        );

        $form_id = createForm($formData);

        if ($form_id > 0) {

          // create a temlate page for the form created
          FormPageNameSettings($fcode,1,$page_type,$page_type);

          // create form table
          createFormTable($formTable);

          $data = array(
            'status' => 200,
            'message' => $sh['lang']['general_success_message'],
            'form_id' => $form_id
          );

          // Success Log
          $actionTaken = array(
              'user_id' => $user_id,
              'page' => "form-creator",
              'action_description' => "Created A new Form of ID: {".$form_id."}",
              'status' => "success",
              'action_type' => "create",
          );

        }else{

          // Error log
          $actionTaken = array(
              'user_id' => $user_id,
              'page' => "form-creator",
              'action_description' => "Could not create New form",
              'status' => "error",
              'action_type' => "create",
          );

          $data = array(
            'status' => 400,
            'message' => $sh['lang']['general_error_message']
          );

        }

        // Keep log of what is done
        saveUserActions($actionTaken);

      }


    }else{

      $data = array(
        'status' => 400,
        'message' => $sh['lang']['user_id_not_valid']
      );

    }


    header("Content-type: application/json");
    echo json_encode($data);
    exit();

  }

  // delete form
  if($s == "delete_form"){

        $error = 0;

        $formId = $_POST['id'];

        $user_id = $sh['user']['user_id'];

        if($formId == "" || empty($formId)){
            $error = 1;
            $data['status'] = 400;
            $data['message'] = 'Form ID Not Selected';
        }

        // get page type and form code to delete file from theme folder
        $page_type = getFormValue('page_type',$formId);
        $fcode = getFormValue('form_code',$formId);

        if($error == 0){

            // delete feature from package
            $deleteForm = deleteForm($formId);

            if($deleteForm){

                // create a temlate page for the form created
                FormPageNameSettings($fcode,3,$page_type,$page_type);

                $data['status'] = 200;
                $data['message'] = $sh['lang']['general_success_message'];

                $actionTaken = array(
                    'user_id' => $user_id,
                    'page' => "form-creator",
                    'action_description' => "Deleted a Form of ID: {".$formId."}",
                    'status' => "success",
                    'action_type' => "delete",
                );

            }else{

                $data['status'] = 400;
                $data['message'] = $sh['lang']['general_error_message'];

                $actionTaken = array(
                    'user_id' => $user_id,
                    'page' => "form-creator",
                    'action_description' => "Could not Form of ID: {".$formId."}",
                    'status' => "error",
                    'action_type' => "delete",
                );

            }

            // Keep log of what is done
            saveUserActions($actionTaken);

        }


        header("Content-type: application/json");
        echo json_encode($data);
        exit();

    }

  // make form
  if ($s == "make_form") {

        extract($_POST);
        $resp = array();
        $loop = true;

        // collect all forms
        $formUpdate = array(
          'title' => Sh_Secure($title),
          'description' => Sh_Secure($description),
          'status' => 1,
          'page_type' => $pageType,
          'formData' => $formValues,
        );


        $arryForm = array(
          'form_code' => $form_code,
          'formData' => $formValues,
        );

        $theme = $sh['config']['theme'];

        $TName = strtolower(getPageTypeValue('name',$pageType))."_".$form_code;

        $fname = strtolower(getPageTypeValue('name',$pageType))."_".$form_code.".phtml";
        $create_form = file_put_contents("themes/".$theme."/layout/dynamic_form/".$fname,$form_data);

        if ($create_form) {

            // update form data
            updateForm($formUpdate,$form_code);

            // create fields in form table
            CreateFormFields($TName,$formValues);


            $resp['status'] = 200;
            $resp['message'] = "Fields Successfully created";

        }else {

            $resp['status'] = 400;
            $resp['message'] = 'error occured while saving the form fields';

        }
  
        header("Content-type: application/json");
        echo json_encode($resp);
        exit();


  }

  // update form
  if($s == "update_form"){
    $error = 0;

    // Regular inputs
    $form_name = Sh_Secure($_POST['title']);
    $page_type = Sh_Secure($_POST['pageType']);
    $fcode = Sh_Secure($_POST['form_code']);
    $description = Sh_Secure($_POST['description']);
    $user_id = Sh_Secure($_POST['user_id']);
    $formValues = $_POST['formValues'];

    $form_data = $_POST['form_data'];

    if($user_id == 0 || $user_id < 1){
      $user_id = $sh['user']['user_id'];
    }

    if ( $form_name == "" || empty($form_name) ) {
        $error = 1;
        $data['message'] = $sh['lang']['form_name_not_empty'];
    }

    if ($error == 0) {

      $formData = array(
        'title' => $form_name,
        'page_type' => $page_type,
        'description' => $description,
        'status' => 1,
        'fname' => Sh_Slugify($form_name),
        'formData' => $formValues
      );


      //
      //
      //
      //
      $formTable = array(
        'form_code' => $fcode,
        'title' => $form_name,
        'page_name' => strtolower(getPageTypeValue('name',$page_type)),
      );


      // $form_id = createForm($formData);

      $theme = $sh['config']['theme'];

      $TName = strtolower(getPageTypeValue('name',$page_type))."_".$fcode;

      $fname = strtolower(getPageTypeValue('name',$page_type))."_".$fcode.".phtml";
      $upd_form = file_put_contents("themes/".$theme."/layout/dynamic_form/".$fname,$form_data);


      if ($upd_form) {

        $form_r = UpdateForm($formData, $fcode);

        // create a temlate page for the form created
        FormPageNameSettings($fcode,2,$page_type,$page_type);

        UpdateFormFields($TName,$formValues);

        $data['status'] = 200;
        $data['message'] = $sh['lang']['general_update_success_message'];

        // Success Log
        $actionTaken = array(
            'user_id' => $user_id,
            'page' => "form-creator",
            'action_description' => "Updated Form of Code: {".$fcode."}",
            'status' => "success",
            'action_type' => "update",
        );

      }else{

        // Error log
        $actionTaken = array(
            'user_id' => $user_id,
            'page' => "form-creator",
            'action_description' => "Could not update the form",
            'status' => "error",
            'action_type' => "update",
        );

        $data['status'] = 400;
        $data['message'] = $sh['lang']['general_error_message'];

      }

      // Keep log of what is done
      saveUserActions($actionTaken);

    }

    header("Content-type: application/json");
    echo json_encode($data);
    exit();


  }

  // delete column
  if ($s = "delete_column") {

    $error = 0;

    $column = $_POST['column'];
    $pt = $_POST['page_type'];
    $pc = $_POST['page_code'];

    if($user_id == 0 || $user_id < 1){
      $user_id = $sh['user']['user_id'];
    }else{
      $user_id = 1;
    }

    if($column == "" || empty($column)){
        $error = 1;
        $data['status'] = 400;
        $data['message'] = 'Column or Field Not Selected';
    }

    $TName = strtolower(getPageTypeValue('name',$pt))."_".$pc;

    if (deleteColumn($TName,$column)) {

      $data['status'] = 200;
      $data['message'] = $sh['lang']['general_success_message'];

      // Success Log
      $actionTaken = array(
          'user_id' => $user_id,
          'page' => "form-creator",
          'action_description' => "Deleted Column Name: {".Remove_from_String($column)."} From Form with Code: {".$pc."}",
          'status' => "success",
          'action_type' => "delete",
      );

    }else{

      // Error log
      $actionTaken = array(
          'user_id' => $user_id,
          'page' => "form-creator",
          'action_description' => "Could not delete column Name: {". Remove_from_String($column) ."}",
          'status' => "error",
          'action_type' => "delete",
      );

      $data['status'] = 400;
      $data['message'] = $sh['lang']['general_error_message'];

    }

    // Keep log of what is done
    saveUserActions($actionTaken);


    header("Content-type: application/json");
    echo json_encode($data);
    exit();

  }





}

 ?>
