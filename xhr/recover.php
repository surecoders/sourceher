<?php
if ($f == 'recover') {

    $error_icon = "<i class='icon_close_alt'></i>";
    
    if (empty($_POST['recoveremail'])) {
        $errors = $error_icon . $sh['lang']['please_check_details'];
    } else {
        if (!filter_var($_POST['recoveremail'], FILTER_VALIDATE_EMAIL)) {
            $errors = $error_icon ." ". $sh['lang']['email_invalid_characters'];
        } else if (Sh_EmailExists($_POST['recoveremail']) === false) {
            $errors = $error_icon ." ". $sh['lang']['email_not_found'];
        }
        // else if ($config['reCaptcha'] == 1) {
        //     if (!isset($_POST['g-recaptcha-response']) || empty($_POST['g-recaptcha-response'])) {
        //         $errors = $error_icon . $sh['lang']['reCaptcha_error'];
        //     }
        // }
    }
    if (empty($errors)) {
        $user_recover_data         = Sh_UserData(Sh_UserIdFromEmail($_POST['recoveremail']));
        $subject                   = $config['siteName'] . ' ' . $sh['lang']['password_rest_request'];
        $code              = md5(rand(111, 999) . time());
        $user_recover_data['link'] = Sh_Link('index.php?link1=reset-password&code=' . $user_recover_data['user_id'] . '_' . $code);
        $query                     = mysqli_query($sqlConnect, "UPDATE " . T_USERS . " SET `email_code` = '$code' WHERE `user_id` = {$user_recover_data['user_id']}");
        $sh['recover']             = $user_recover_data;
        $body                      = Sh_LoadPage('emails/recover');
        $send_message_data         = array(
            'from_email' => $sh['config']['siteEmail'],
            'from_name' => $sh['config']['siteName'],
            'to_email' => $_POST['recoveremail'],
            'to_name' => '',
            'subject' => $subject,
            'charSet' => 'utf-8',
            'message_body' => $body,
            'is_html' => true
        );
        $send                      = Sh_SendMessage($send_message_data);
        $data                      = array(
            'status' => 200,
            'message' => $success_icon . $sh['lang']['email_sent']
        );
    }

    header("Content-type: application/json");
    if (isset($errors)) {
        echo json_encode(array(
            'errors' => $errors
        ));
    } else {
        echo json_encode($data);
    }
    exit();
}
