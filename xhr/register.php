<?php
if ($f == 'register') {

  $error_icon = "<i class='icon_close_alt'></i>";
  $success_icon = "<i class='icon_check_alt'></i>";
    // Register Block
    if($s == 'register'){

        if (!empty($_SESSION['user_id'])) {
            $_SESSION['user_id'] = '';
            unset($_SESSION['user_id']);
        }
        if (!empty($_COOKIE['user_id'])) {
            $_COOKIE['user_id'] = '';
            unset($_COOKIE['user_id']);
            setcookie('user_id', null, -1);
            setcookie('user_id', null, -1,'/');
        }
        // $fields = Wo_GetWelcomeFileds();
        if (empty($_POST['email']) || empty($_POST['username']) || empty($_POST['password']) || empty($_POST['confirm_password']) ) { /*|| empty($_POST['gender'])* dvm mods*/
            $errors = $error_icon . $sh['lang']['please_check_details'];
        } else {

            $is_exist = Sh_IsNameExist($_POST['username'], 0);

            if (empty($_POST['phone_num']) && $sh['config']['sms_or_email'] == 'sms') {
                $errors = $error_icon . $sh['lang']['worng_phone_number'];
            }

            if (in_array(true, $is_exist)) {
                $errors = $error_icon . $sh['lang']['username_exists'];
            }

            if (Sh_IsBanned($_POST['username'])) {
                $errors = $error_icon . $sh['lang']['username_is_banned'];
            }


            if (Sh_IsBanned($_POST['email'])) {
                $errors = $error_icon . $sh['lang']['email_is_banned'];
            }

            if (preg_match_all('~@(.*?)(.*)~', $_POST['email'], $matches) && !empty($matches[2]) && !empty($matches[2][0]) && Sh_IsBanned($matches[2][0])) {
                $errors = $error_icon . $sh['lang']['email_provider_banned'];
            }

            if (Sh_CheckIfUserCanRegister($sh['config']['user_limit']) === false) {
                $errors = $error_icon . $sh['lang']['limit_exceeded'];
            }

            if (in_array($_POST['username'], $sh['site_pages'])) {
                $errors = $error_icon . $sh['lang']['username_invalid_characters'];
            }

            if (strlen($_POST['username']) < 5 OR strlen($_POST['username']) > 32) {
                $errors = $error_icon . $sh['lang']['username_characters_length'];
            }

            if (!preg_match('/^[\w]+$/', $_POST['username'])) {
                $errors = $error_icon . $sh['lang']['username_invalid_characters'];
            }

            if (!empty($_POST['phone_num'])) {
                if (!preg_match('/^\+?\d+$/', $_POST['phone_num'])) {
                    $errors = $error_icon . $sh['lang']['worng_phone_number'];
                } else {
                    if (Sh_PhoneExists($_POST['phone_num']) === true) {
                        $errors = $error_icon . $sh['lang']['phone_already_used'];
                    }
                }
            }
            if (Sh_EmailExists($_POST['email']) === true) {
                $errors = $error_icon . $sh['lang']['email_exists'];
            }

            if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $errors = $error_icon . $sh['lang']['email_invalid_characters'];
            }

            if (strlen($_POST['password']) < 6) {
                $errors = $error_icon . $sh['lang']['password_short'];
            }

            if ($_POST['password'] != $_POST['confirm_password']) {
                $errors = $error_icon . $sh['lang']['password_mismatch'];
            }

            $gender = '';

        }




        if (empty($errors)) {

            $code = md5(rand(1111, 9999) . time());

            $expert_code = rand(111111, 999999);
            $hash_expert_code = md5($expert_code);

            $re_data  = array(
                'first_name' => Sh_Secure($_POST['first_name']),
                'last_name' => Sh_Secure($_POST['last_name']),
                'professional' => Sh_Secure($_POST['professional']),
                'mood_status' => 1,
                'cat_id' => Sh_Secure($_POST['cat_id']),
                'email' => Sh_Secure($_POST['email'], 0),
                'username' => Sh_Secure($_POST['username'], 0),
                'password' => $_POST['password'],
                'email_code' => Sh_Secure($code, 0),
                'expert_access_code' => $hash_expert_code,
                'src' => 'site',
                'gender' => Sh_Secure($gender),
                'lastseen' => time(),
                'birthday' => '0000-00-00',
            );

            if (!empty($_POST['phone_num'])) {
                $re_data['phone_number'] = Sh_Secure($_POST['phone_num']);
            }

            if ($sh['config']['emailValidation'] == '1') {

              $re_data['active'] = '0';
              $typeReg = "";

              if ($sh['config']['sms_or_email'] == 'email') {

                  $sh['user']        = $_POST;
                  $sh['code']        = $code;
                  $body = Sh_LoadPage('emails/activate');
                  $send_message_data = array(
                      'from_email' => $sh['config']['siteEmail'],
                      'from_name' => $sh['config']['siteName'],
                      'to_email' => $_POST['email'],
                      'to_name' => $_POST['first_name']." ".$_POST['last_name'],
                      'subject' => $sh['lang']['account_activation'],
                      'charSet' => 'utf-8',
                      'message_body' => $body,
                      'is_html' => true
                  );
                  $send  = Sh_SendMessage($send_message_data);

              }


            }else {

              $re_data['active'] = '1';
              $typeReg = "autoactive";

              $login = Sh_Login($_POST['username'], $_POST['password']);
                if ($login === true) {
                    $session = Sh_CreateLoginSession(Sh_UserIdFromUsername($_POST['username']));
                    $_SESSION['user_id'] = $session;
                    setcookie("user_id", $session, time() + (10 * 365 * 24 * 60 * 60));
                }


            }

            $register = Sh_RegisterUser($re_data);

            if ($register > 0) {

              $data  = array(
                  'status' => 200,
                  'user_id' => $register,
                  'typeReg' => $typeReg,
                  'location' => Sh_Link(''),
                  'message' => $success_icon . $sh['lang']['successfully_joined_label']
              );


            }else {

              $errors = $error_icon . $sh['lang']['general_error_message'];

            }


        }



        header("Content-type: application/json");
        if (isset($errors) || !empty($errors)) {
            echo json_encode(array(
                'errors' => $errors
            ));
        } else {
            echo json_encode($data);
        }
        exit();


    }

    // Admin Register user page
    if($s == 'admin_register_user'){

        // $fields = Wo_GetWelcomeFileds();
        if (empty($_POST['email']) || empty($_POST['username']) || empty($_POST['phone_num']) ) { /*|| empty($_POST['gender'])* dvm mods*/
            $errors = $error_icon . $sh['lang']['please_check_details'];
        } else {

            $is_exist = Sh_IsNameExist($_POST['username'], 0);

            if (empty($_POST['phone_num']) && $sh['config']['sms_or_email'] == 'sms') {
                $errors = $error_icon . $sh['lang']['worng_phone_number'];
            }

            if (in_array(true, $is_exist)) {
                $errors = $error_icon . $sh['lang']['username_exists'];
            }

            if (Sh_IsBanned($_POST['username'])) {
                $errors = $error_icon . $sh['lang']['username_is_banned'];
            }


            if (Sh_IsBanned($_POST['email'])) {
                $errors = $error_icon . $sh['lang']['email_is_banned'];
            }

            if (preg_match_all('~@(.*?)(.*)~', $_POST['email'], $matches) && !empty($matches[2]) && !empty($matches[2][0]) && Sh_IsBanned($matches[2][0])) {
                $errors = $error_icon . $sh['lang']['email_provider_banned'];
            }

            if (in_array($_POST['username'], $sh['site_pages'])) {
                $errors = $error_icon . $sh['lang']['username_invalid_characters'];
            }

            if (strlen($_POST['username']) < 5 OR strlen($_POST['username']) > 32) {
                $errors = $error_icon . $sh['lang']['username_characters_length'];
            }

            if (!preg_match('/^[\w]+$/', $_POST['username'])) {
                $errors = $error_icon . $sh['lang']['username_invalid_characters'];
            }

            if (!empty($_POST['phone_num'])) {
                if (!preg_match('/^\+?\d+$/', $_POST['phone_num'])) {
                    $errors = $error_icon . $sh['lang']['worng_phone_number'];
                } else {
                    if (Sh_PhoneExists($_POST['phone_num']) === true) {
                        $errors = $error_icon . $sh['lang']['phone_already_used'];
                    }
                }
            }
            if (Sh_EmailExists($_POST['email']) === true) {
                $errors = $error_icon . $sh['lang']['email_exists'];
            }

            if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $errors = $error_icon . $sh['lang']['email_invalid_characters'];
            }

            $gender = '';

        }




        if (empty($errors)) {

            $code = md5(rand(1111, 9999) . time());

            // password generated
            $pass = Sh_GenerateKey(4, 10, false, false, true, false);

            // admin id
            $admin_id = $sh['user']['user_id'];

            $expert_code = rand(111111, 999999);
            $hash_expert_code = md5($expert_code);

            $re_data  = array(
                'first_name' => Sh_Secure($_POST['first_name']),
                'last_name' => Sh_Secure($_POST['last_name']),
                'professional' => Sh_Secure($_POST['professional']),
                'cat_id' => Sh_Secure($_POST['cat_id']),
                'mood_status' => 1,
                'email' => Sh_Secure($_POST['email'], 0),
                'username' => Sh_Secure($_POST['username'], 0),
                'password' => $pass,
                'email_code' => Sh_Secure($code, 0),
                'expert_access_code' => $hash_expert_code,
                'src' => 'site',
                'gender' => Sh_Secure($gender),
                'lastseen' => time(),
                'birthday' => '0000-00-00',
            );

            if (!empty($_POST['phone_num'])) {
                $re_data['phone_number'] = Sh_Secure($_POST['phone_num']);
            }

            if ($sh['config']['emailValidation'] == '1') {
              $re_data['active'] = "0";

              if ($sh['config']['sms_or_email'] == 'email') {

                  $sh['user']        = $_POST;
                  $sh['code']        = $code;
                  $body = Sh_LoadPage('emails/activate');
                  $send_message_data = array(
                      'from_email' => $sh['config']['siteEmail'],
                      'from_name' => $sh['config']['siteName'],
                      'to_email' => $_POST['email'],
                      'to_name' => $_POST['first_name']." ".$_POST['last_name'],
                      'subject' => $sh['lang']['account_activation'],
                      'charSet' => 'utf-8',
                      'message_body' => $body,
                      'is_html' => true
                  );
                  $send  = Sh_SendMessage($send_message_data);
                  $errors = $success_icon . $sh['lang']['successfully_joined_verify_label'];

              }

            }else{
              $re_data['active'] = "1";



            }

            $register = Sh_RegisterUser($re_data);

            if ($register > 0) {

              $sh['user'] = $_POST;
              $sh['password']  = $pass;
              $body = Sh_LoadPage('emails/password_sent');
              $send_message_data = array(
                  'from_email' => $sh['config']['siteEmail'],
                  'from_name' => $sh['config']['siteName'],
                  'to_email' => $_POST['email'],
                  'to_name' => $_POST['first_name']." ".$_POST['last_name'],
                  'subject' => $sh['lang']['password_notification'],
                  'charSet' => 'utf-8',
                  'message_body' => $body,
                  'is_html' => true
              );
              $send  = Sh_SendMessage($send_message_data);

              $data  = array(
                  'status' => 200,
                  'user_id' => $register,
                  'message' => $success_icon . $sh['lang']['success_create']
              );

            }else{

              $errors = $error_icon . $sh['lang']['general_error_message'];

            }


        }



        header("Content-type: application/json");
        if (isset($errors) || !empty($errors)) {
            echo json_encode(array(
                'status' => 400,
                'message' => $errors
            ));
        } else {
            echo json_encode($data);
        }
        exit();


    }

    // get user data for update
    if ($s == 'get_single_user_data') {

      $user_id = Sh_Secure($_GET['user_id']);
      $admin_id = Sh_Secure($_GET['admin_id']);

      if ($admin_id == $sh['user']['user_id']) {

        $UserData = Sh_UserData($user_id);

        if ($UserData) { ?>

          <div class="col-md-12">
            <div class="list-title">
              <select name="cat_id" class="form-control" required>
                <option value="" >Select Category</option>
                <?php
                  $cats = getAllCategoryData('status = 1');
                  foreach ($cats as $cat) {
                 ?>
                <option <?= ($UserData['cat_id'] == $pro['id']) ? 'selected' : '' ?> value="<?= $cat['id'] ?>" ><?= $cat['title'] ?></option>
                <?php } ?>

              </select>
            </div>
          </div>

          <div class="col-md-6">
            <div class="list-title">
              <select name="professional" class="form-control" required>
                  <option value="">Select Professional</option>
                  <?php
                    $prof = getAllProfessionalData('status = 1');
                    foreach ($prof as $pro) {
                   ?>
                   <option <?= ($UserData['professional'] == $pro['id']) ? 'selected' : '' ?> value="<?= $pro['id'] ?>" ><?= $pro['title'] ?></option>
                  <?php } ?>
              </select>
            </div>
          </div>

          <div class="col-md-6">
              <div class="list-title">
                  <input type="hidden" name="user_id" value="<?= $UserData['user_id']; ?>">
                  <input type="hidden" name="admin_id" value="<?= $sh['user']['user_id']; ?>">
                  <input id="formName" type="text" value="<?= $UserData['first_name'] ?>" placeholder="User First Name" class="form-control" name="first_name">
              </div>
              <br>
          </div>
          <div class="col-md-6">
              <div class="list-title">
                  <input id="formName" type="text" value="<?= $UserData['last_name'] ?>" placeholder="User Last Name" class="form-control" name="last_name">
              </div>
              <br>
          </div>

          <div class="col-md-6">
              <div class="list-title">
                  <input id="formName" type="text" value="<?= $UserData['email'] ?>" placeholder="User Email Address" class="form-control" name="email" required>
              </div>
              <br>
          </div>
          <div class="col-md-6">
              <div class="list-title">
                  <input id="formName" type="text" value="<?= $UserData['phone_number'] ?>" placeholder="User Phone Number" class="form-control" name="phone_num" required>
              </div>
              <br>
          </div>


          <div class="col-md-6">
            <div class="list-title">
              <select name="status" class="form-control" required>
                  <option value="1" <?= ($UserData['active'] == '1') ? 'selected' : '' ?> >Active</option>
                  <option value="0" <?= ($sh['user']['user_id'] == $UserData['user_id']) ? 'disabled' : '' ?> <?= ($UserData['active'] == '0') ? 'selected' : '' ?> >In-Active</option>
                  <option value="2" disabled <?= ($UserData['active'] == '2') ? 'selected' : '' ?> >Deleted</option>
              </select>
            </div>
          </div>

          <div class="col-md-12">
            <div class="list-title">
              <select name="verified" class="form-control" required>
                  <option value="">Select Account Verification Type</option>
                  <option value="1" <?= ($UserData['verified'] == '1') ? 'selected' : '' ?> >Approved</option>
                  <option value="0" <?= ($UserData['verified'] == '0') ? 'selected' : '' ?> >Not Approved</option>
              </select>
            </div>
          </div>

      <?php  }else { ?>

            <h3 style="margin-top: 10px; margin-bottom: 10px; text-align: center;"> Sorry System could not process your request </h3>

    <?php    }


      }else { ?>

        <h3 style="margin-top: 10px; margin-bottom: 10px; text-align: center;"> Authentication Error </h3>

    <?php  }


    }


    // update user data
    if($s == "update_user_data"){

      $first_name = Sh_Secure($_POST['first_name']);
      $last_name = Sh_Secure($_POST['last_name']);
      $email = Sh_Secure($_POST['email']);
      $phone_num = Sh_Secure($_POST['phone_num']);
      $professional = Sh_Secure($_POST['professional']);
      $status = Sh_Secure($_POST['status']);
      $verified = Sh_Secure($_POST['verified']);
      $user_id = Sh_Secure($_POST['user_id']);

      $admin_id = Sh_Secure($_POST['admin_id']);

      if ($admin_id == $sh['user']['user_id']) {

        $user_data = array(
          'first_name' => $first_name,
          'last_name' => $last_name,
          'email' => $email,
          'active' => $status,
          'phone_number' => $phone_num,
          'verified' => $verified,
          'professional' => $professional,
        );

        $updateUser = UpdateUserData($user_data,$user_id);

        if ($updateUser) {

          $data = array(
            'status' => 200,
            'message' => "User ".$sh['lang']['general_update_success_message'],
          );

        }else {

          $data = array(
            'status' => 400,
            'message' => $sh['lang']['general_update_error_message'],
          );

        }

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['user_id_not_valid'],
        );

      }

      header("Content-type: application/json");
      echo json_encode($data);
      exit();
    }


    // Account delete / Disable
    if ($s == 'delete_user_account') {

      $user_id = Sh_Secure($_GET['user_id']);

      $admin_id = Sh_Secure($_GET['admin_id']);

      if ($admin_id == $sh['user']['user_id']) {

        $user_data = array(
          'active' => '2',
        );

        $updateUser = UpdateUserData($user_data,$user_id);

        if ($updateUser) {

          $data = array(
            'status' => 200,
            'message' => "User ".$sh['lang']['general_update_success_message'],
          );

        }else {

          $data = array(
            'status' => 400,
            'message' => $sh['lang']['general_update_error_message'],
          );

        }

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['user_id_not_valid'],
        );

      }

      header("Content-type: application/json");
      echo json_encode($data);
      exit();
    }

}
