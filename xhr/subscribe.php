<?php
if ($f == 'subscribe') {

    // Subscribe block
    if($s == 'subscribe'){


      if ( empty($_POST['email_newsletter']) ) {

        $data = array(
          'status' => 400,
          'message' => $_POST['email_newsletter']
        );

      }else {

        if (isset($_POST['email_newsletter'])) {
          $subEmail = Sh_Secure($_POST['email_newsletter']);
        }

        if (isset($_POST['status'])) {
          $status = $_POST['status'];
        }else {
          $status = 1;
        }

        $subscribeData = array(
          'email' => $subEmail,
          'status' => $status
        );

        // check if email has been subscribed
        $check = CheckEmailSubscribed($subEmail);

        if ($check) {

          $data = array(
            'status' => 400,
            'message' => "Email Address already subscribed"
          );

        }else {

          $subnow = CreateSubscribers($subscribeData);

          if ($subnow) {

            $data = array(
              'status' => 200,
              'location' => Sh_Link(''),
              'message' => "Successfully Subscribed"
            );

          }else {

            $data = array(
              'status' => 400,
              'message' => $sh['lang']['general_error_message']
            );

          }


        }


      }


      header("Content-type: application/json");
      echo json_encode($data);
      exit();

    }


    if ($s == "update_subscriber") {

      // get all input form
      $email = Sh_Secure($_POST['email']);
      $status = $_POST['status'];
      $sub_id = $_POST['sub_id'];



      // check if title is existing already
      $exist_email = getSubscriberEmailForUpdate($email,$sub_id);

      if ($exist_email) {

        $data = array(
          'status' => 400,
          'message' => $email." ".$sh['lang']['found_already'],
        );

      }else{

        $array_data = array(
          'email' => $email,
          'status' => $status,
        );


        $update_data = UpdateSubscriberData($array_data, $sub_id);

        if ($update_data) {

          $data = array(
            'status' => 200,
            'message' => "Subscriber ".$sh['lang']['general_update_success_message'],
          );

        }else{

          $data = array(
            'status' => 400,
            'message' => $sh['lang']['general_update_error_message'],
          );

        }


      }

      header("Content-type: application/json");
      echo json_encode($data);
      exit();

    }


    if ($s == "delete_subscriber") {

      $sub_id = Sh_Secure($_GET['sub_id']);

      $admin_id = Sh_Secure($_GET['user_id']);

      if ($admin_id == $sh['user']['user_id']) {

        // delete the professional
        $delete_sub = deleteSubscriber($sub_id);

        if ($delete_sub) {

          $data = array(
            'status' => 200,
            'message' => "Subscriber ".$sh['lang']['general_delete_success_message'],
          );

        }else {

          $data = array(
            'status' => 400,
            'message' => $sh['lang']['general_error_message'],
          );

        }



      }else{

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['user_id_not_valid'],
        );

      }


      header("Content-type: application/json");
      echo json_encode($data);
      exit();

    }


    if ($s == "get_single_subscriber") {

      $sub_id = Sh_Secure($_GET['sub_id']);
      $admin_id = Sh_Secure($_GET['user_id']);

      $getr_catData = getSubscriberSingleData($sub_id);

      if ($getr_catData) { ?>

        <div class="col-md-12">
            <div class="list-title">
                <input type="hidden" name="sub_id" value="<?= $getr_catData['id'] ?>">
                <input id="formName" type="text" value="<?= $getr_catData['email'] ?>" placeholder="Email" class="form-control" name="email">
            </div>
            <br>
        </div>


        <div class="col-md-12">
          <div class="list-title">
            <select name="status" class="form-control" required>
                <option value="1" <?= ($getr_catData['status'] == 1) ? 'selected' : '' ?>>Active</option>
                <option value="0" <?= ($getr_catData['status'] == 0) ? 'selected' : '' ?>>In-Active</option>
            </select>
          </div>
        </div>

  <?php  }else{ ?>

    <h3 style="margin-top: 10px; margin-bottom: 10px; text-align: center;"> Sorry System could not process your request </h3>

  <?php  }


}



}
