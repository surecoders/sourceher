<?php
if ($f == 'page_type') {

  $pageTypes = getPageFormType();

  $pgt = array();
  foreach ($pageTypes as $pt) {
    $pgt[] = $pt['name'];
  }

  $rs = ucfirst($s);

  if(in_array($rs, $pgt)){

    if ($_POST['user_id'] == $sh['user']['user_id']) {

      $usID = $_POST['user_id'];

      $pageTypesData = GetPageTypeData($s);
      $formDataDeatails = getFormBuilderDataByPageType($pageTypesData['id']);

      // form table in the database
      $fcode = $formDataDeatails['form_code'];
      $dbtable = $s."_".$fcode;

      // get all posted form fields
      $UserFormData = array();

      foreach ($_POST as $key => $value) {
        $UserFormData[$key] = $value;
      }

      // remove unused inputs
      unset($UserFormData['button']);

      // get all form in the builder
      $formFields = json_decode($formDataDeatails['formData'], true);


      // check if user already have account with the page type then update if found or create new one if not found
      // check if user acoount exist
      $checkMyAct = CheckUserPageTypeAccountExist($usID,$dbtable);

      if ($checkMyAct) {

        $updateUserPageTypeAct = UpdateMyUserPageTypeAccount($dbtable,$usID,$UserFormData);

        if ($updateUserPageTypeAct) {

          $data = array(
            'status' => 200,
            'message' => $rs." Account ".$sh['lang']['general_update_success_message'],
          );

        }else {

          $data = array(
            'status' => 400,
            'message' => $sh['lang']['general_update_error_message'],
          );

        }

      }else {

        // create the page account and update users status
        $createPgAct = createPageTypeAccount($dbtable,$UserFormData);

        if ($createPgAct) {

          // user main account update
          $userData = array(
            'user_type' => $pageTypesData['id'],
            'relationship_id' => $createPgAct,
            'account_type' => 'pending',
            'expert_access_code' => ""
          );

          UpdateUserData($userData,$usID);

          $data = array(
            'status' => 200,
            'account_id' => $createPgAct,
            'message' => $rs." Account ".$sh['lang']['success_create'],
          );

        }else {

          $data = array(
            'status' => 400,
            'message' => $sh['lang']['general_error_message'],
          );

        }


      }


    }else {

      $data = array(
        'status' => 400,
        'message' => $sh['lang']['user_id_not_valid'],
      );

    }


  }else {

    $data = array(
      'status' => 400,
      'message' => "critical issue",
    );



  }


  header("Content-type: application/json");
  echo json_encode($data);
  exit();


}
