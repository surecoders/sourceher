<?php
if ($f == "bulk_actions") {

  if ($s == "bulk_status_update") {

    // get all input form
    $status = $_POST['status'];

    if(isset($_POST['conts_id'])){
      $contacts_id = trim($_POST['conts_id']);
    }

    $Contsarray = explode(',', $contacts_id);

    $user_id = $sh['user']['user_id'];

    $userData = array('active' => $status);

    foreach ($Contsarray as $cont) {

      $UPDAT = UpdateUserData($userData,$cont);

      if ($UPDAT) {

        $data = array(
          'status' => 200,
          'message' => count($Contsarray) ." Account Status ".$sh['lang']['general_update_success_message'],
        );

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_error_message'],
        );

      }
    }


    header("Content-type: application/json");
    echo json_encode($data);
    exit();

  }

  if ($s == "make_account_admin_access") {

    if(isset($_GET['conts_id'])){
      $contacts_id = trim($_GET['conts_id']);
    }

    $Contsarray = explode(',', $contacts_id);

    $user_id = $sh['user']['user_id'];

    $userData = array('admin' => 1);

    foreach ($Contsarray as $cont) {

      $UPDAT = UpdateUserData($userData,$cont);

      if ($UPDAT) {

        $data = array(
          'status' => 200,
          'message' => count($Contsarray) ." Account Status ".$sh['lang']['general_update_success_message'],
        );

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_error_message'],
        );

      }
    }


    header("Content-type: application/json");
    echo json_encode($data);
    exit();

  }

  if ($s == "remove_account_admin_access") {

    if(isset($_GET['conts_id'])){
      $contacts_id = trim($_GET['conts_id']);
    }

    $Contsarray = explode(',', $contacts_id);

    $user_id = $sh['user']['user_id'];

    $userData = array('admin' => 0);

    foreach ($Contsarray as $cont) {

      $UPDAT = UpdateUserData($userData,$cont);

      if ($UPDAT) {

        $data = array(
          'status' => 200,
          'message' => count($Contsarray) ." Account Status ".$sh['lang']['general_update_success_message'],
        );

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_error_message'],
        );

      }
    }


    header("Content-type: application/json");
    echo json_encode($data);
    exit();

  }

  if ($s == "make_account_expert_access") {

    if(isset($_GET['conts_id'])){
      $contacts_id = trim($_GET['conts_id']);
    }

    $Contsarray = explode(',', $contacts_id);

    $user_id = $sh['user']['user_id'];

    foreach ($Contsarray as $cont) {

      $usData = Sh_UserData($cont);

      $sh['expert_url'] = $sh['config']['site_url']."/user-form/experts?mycode=".$usData['expert_access_code'];
      $sh['expert_user'] = $usData;
      $body = Sh_LoadPage('emails/expert_access');
      $send_message_data = array(
          'from_email' => $sh['config']['siteEmail'],
          'from_name' => $sh['config']['siteName'],
          'to_email' => $usData['email'],
          'to_name' => $usData['first_name']." ".$usData['last_name'],
          'subject' => $sh['lang']['expert_message_notification'],
          'charSet' => 'utf-8',
          'message_body' => $body,
          'is_html' => true
      );

      $send  = Sh_SendMessage($send_message_data);

      // $UPDAT = UpdateUserData($userData,$cont);

      if ($send) {

        $data = array(
          'status' => 200,
          'message' => count($Contsarray)." Expert Access Request ".$sh['lang']['email_notification_message'],
        );

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_error_message'],
        );

      }
    }

    header("Content-type: application/json");
    echo json_encode($data);
    exit();
  }

  if ($s == "remove_account_expert_access") {

    if(isset($_GET['conts_id'])){
      $contacts_id = trim($_GET['conts_id']);
    }

    $Contsarray = explode(',', $contacts_id);

    $user_id = $sh['user']['user_id'];

    $expert_code = rand(111111, 999999);
    $hash_expert_code = md5($expert_code);

    $userData = array(
      'account_type' => "user",
      'expert_access_code' => $hash_expert_code,
    );


    foreach ($Contsarray as $cont) {

      $usData = Sh_UserData($cont);

      $sh['expert_user'] = $usData;
      $body = Sh_LoadPage('emails/expert_remove_access');
      $send_message_data = array(
          'from_email' => $sh['config']['siteEmail'],
          'from_name' => $sh['config']['siteName'],
          'to_email' => $usData['email'],
          'to_name' => $usData['first_name']." ".$usData['last_name'],
          'subject' => $sh['lang']['expert_remove_message_notification'],
          'charSet' => 'utf-8',
          'message_body' => $body,
          'is_html' => true
      );

      $send  = Sh_SendMessage($send_message_data);

      $UPDAT = UpdateUserData($userData,$cont);

      if ($send) {

        deleteExpertDataRow($cont);

        $data = array(
          'status' => 200,
          'message' => count($Contsarray)." Expert Access Request ".$sh['lang']['email_notification_message'],
        );

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_error_message'],
        );

      }
    }

    header("Content-type: application/json");
    echo json_encode($data);
    exit();
  }

  if ($s == "promote_account") {

    if(isset($_GET['conts_id'])){
      $contacts_id = trim($_GET['conts_id']);
    }

    $Contsarray = explode(',', $contacts_id);

    $user_id = $sh['user']['user_id'];

    $userData = array('promote_account' => 1);

    $checkAcct = array();
    foreach ($Contsarray as $cont) {

      $allIncomingData = Sh_UserData($cont);

      if ($allIncomingData['account_type'] == "experts") {
        $checkAcct[] = $allIncomingData['user_id'];
      }

    }


    if (count($Contsarray) == count($checkAcct)) {

      foreach ($checkAcct as $newid) {
        $UPDAT = UpdateUserData($userData,$newid);

        if ($UPDAT) {

          $data = array(
            'status' => 200,
            'message' => count($checkAcct) ." Account Promoted Successfully ",
          );

        }else {

          $data = array(
            'status' => 400,
            'message' => $sh['lang']['general_error_message'],
          );

        }
      }


    }else {
      $nonExperts = count($Contsarray) - count($checkAcct);

      $data = array(
        'status' => 400,
        'message' => $nonExperts. " Non Expert Found in the list of promotion",
      );

    }


    header("Content-type: application/json");
    echo json_encode($data);
    exit();


  }

  if ($s == "remove_promote_account") {

    if(isset($_GET['conts_id'])){
      $contacts_id = trim($_GET['conts_id']);
    }

    $Contsarray = explode(',', $contacts_id);

    $user_id = $sh['user']['user_id'];

    $userData = array('promote_account' => 0);

    $checkAcct = array();
    foreach ($Contsarray as $cont) {

      $UPDAT = UpdateUserData($userData,$cont);

      if ($UPDAT) {

        $data = array(
          'status' => 200,
          'message' => count($checkAcct) ." Account(s) Promotion Removed Successfully ",
        );

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_error_message'],
        );

      }


    }


    header("Content-type: application/json");
    echo json_encode($data);
    exit();

  }

}


 ?>
