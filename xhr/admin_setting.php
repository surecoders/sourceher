<?php
use Aws\S3\S3Client;
use Google\Cloud\Storage\StorageClient;
if ($f == 'admin_setting' AND (Sh_IsAdmin()) ) {

    if ($s == 'test_message') {
        $send_message_data = array(
            'from_email' => $sh['config']['siteEmail'],
            'from_name' => $sh['config']['siteName'],
            'to_email' => $sh['user']['email'],
            'to_name' => $sh['user']['name'],
            'subject' => 'Test Message From ' . $sh['config']['siteName'],
            'charSet' => 'utf-8',
            'message_body' => 'If you can see this message, then your SMTP configuration is working fine.',
            'is_html' => false
        );
        $send_message      = Sh_SendMessage($send_message_data);
        if ($send_message === true) {
            $data['status'] = 200;
        } else {
            $data['status'] = 400;
            $data['error']  = "Error found while sending the email, the information you provided are not correct, please test the email settings on your local device and make sure they are correct. ";
        }
        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }

    if ($s == "update_site_logo") {
      if (isset($_FILES['site_logo']) && !empty($_FILES['site_logo'])) {

           if (!empty($_FILES['site_logo']["tmp_name"])) {

               $orignalname_cb = $_FILES['site_logo']["name"];
               $fileInfo_cb = array(
                 'file' => $_FILES["site_logo"]["tmp_name"],
                 'name' => $_FILES['site_logo']['name'],
                 'size' => $_FILES["site_logo"]["size"],
                 'type' => $_FILES["site_logo"]["type"],
                 'types' => 'jpeg,jpg,png,gif',
               );

               $media_cb = Sh_ShareFile($fileInfo_cb, 0, false);

               if (!empty($media_cb)) {

                 $filename_cb = $media_cb['filename'];

                 if (isset($sh['config']['site_logo'])) {

                   $saveSetting = Sh_SaveConfig('site_logo', $filename_cb);

                 }else {

                   $query = insertRow(T_CONFIG, [
                       'name' => 'site_logo',
                       'value' => $filename_cb
                   ]);

                   $saveSetting = $sqlConnect->query($query);

                 }


               }

           }

       }

       if ($saveSetting === true) {
           $data['status'] = 200;
       }


       header("Content-type: application/json");
       echo json_encode($data);
       exit();



    }

    if ($s == "update_contact_banner") {
      if (isset($_FILES['contact_banner']) && !empty($_FILES['contact_banner'])) {

           if (!empty($_FILES['contact_banner']["tmp_name"])) {

               $orignalname_cb = $_FILES['contact_banner']["name"];
               $fileInfo_cb = array(
                 'file' => $_FILES["contact_banner"]["tmp_name"],
                 'name' => $_FILES['contact_banner']['name'],
                 'size' => $_FILES["contact_banner"]["size"],
                 'type' => $_FILES["contact_banner"]["type"],
                 'types' => 'jpeg,jpg,png,gif',
               );

               $media_cb = Sh_ShareFile($fileInfo_cb, 0, false);

               if (!empty($media_cb)) {

                 $filename_cb = $media_cb['filename'];

                 if (isset($sh['config']['contact_banner'])) {

                   $saveSetting = Sh_SaveConfig('contact_banner', $filename_cb);

                 }else {

                   $query = insertRow(T_CONFIG, [
                       'name' => 'contact_banner',
                       'value' => $filename_cb
                   ]);

                   $saveSetting = $sqlConnect->query($query);

                 }


               }

           }

       }

       if ($saveSetting === true) {
           $data['status'] = 200;
       }


       header("Content-type: application/json");
       echo json_encode($data);
       exit();



    }

    if ($s == "update_banner_image") {
      if (isset($_FILES['banner_image']) && !empty($_FILES['banner_image'])) {

           if (!empty($_FILES['banner_image']["tmp_name"])) {

               $orignalname_cb = $_FILES['banner_image']["name"];
               $fileInfo_cb = array(
                 'file' => $_FILES["banner_image"]["tmp_name"],
                 'name' => $_FILES['banner_image']['name'],
                 'size' => $_FILES["banner_image"]["size"],
                 'type' => $_FILES["banner_image"]["type"],
                 'types' => 'jpeg,jpg,png,gif',
               );

               $media_cb = Sh_ShareFile($fileInfo_cb, 0, false);

               if (!empty($media_cb)) {

                 $filename_cb = $media_cb['filename'];

                 if (isset($sh['config']['banner_image'])) {

                   $saveSetting = Sh_SaveConfig('banner_image', $filename_cb);

                 }else {

                   $query = insertRow(T_CONFIG, [
                       'name' => 'banner_image',
                       'value' => $filename_cb
                   ]);

                   $saveSetting = $sqlConnect->query($query);

                 }


               }

           }

       }

       if ($saveSetting === true) {
           $data['status'] = 200;
       }


       header("Content-type: application/json");
       echo json_encode($data);
       exit();



    }

    if ($s == "update_partner1_image") {
      if (isset($_FILES['partner1_image']) && !empty($_FILES['partner1_image'])) {

           if (!empty($_FILES['partner1_image']["tmp_name"])) {

               $orignalname_cb = $_FILES['partner1_image']["name"];
               $fileInfo_cb = array(
                 'file' => $_FILES["partner1_image"]["tmp_name"],
                 'name' => $_FILES['partner1_image']['name'],
                 'size' => $_FILES["partner1_image"]["size"],
                 'type' => $_FILES["partner1_image"]["type"],
                 'types' => 'jpeg,jpg,png,gif',
               );

               $media_cb = Sh_ShareFile($fileInfo_cb, 0, false);

               if (!empty($media_cb)) {

                 $filename_cb = $media_cb['filename'];

                 if (isset($sh['config']['partner1_image'])) {

                   $saveSetting = Sh_SaveConfig('partner1_image', $filename_cb);

                 }else {

                   $query = insertRow(T_CONFIG, [
                       'name' => 'partner1_image',
                       'value' => $filename_cb
                   ]);

                   $saveSetting = $sqlConnect->query($query);

                 }


               }

           }

       }

       if ($saveSetting === true) {
           $data['status'] = 200;
       }


       header("Content-type: application/json");
       echo json_encode($data);
       exit();



    }

    if ($s == "update_partner2_image") {
      if (isset($_FILES['partner2_image']) && !empty($_FILES['partner2_image'])) {

           if (!empty($_FILES['partner2_image']["tmp_name"])) {

               $orignalname_cb = $_FILES['partner2_image']["name"];
               $fileInfo_cb = array(
                 'file' => $_FILES["partner2_image"]["tmp_name"],
                 'name' => $_FILES['partner2_image']['name'],
                 'size' => $_FILES["partner2_image"]["size"],
                 'type' => $_FILES["partner2_image"]["type"],
                 'types' => 'jpeg,jpg,png,gif',
               );

               $media_cb = Sh_ShareFile($fileInfo_cb, 0, false);

               if (!empty($media_cb)) {

                 $filename_cb = $media_cb['filename'];

                 if (isset($sh['config']['partner2_image'])) {

                   $saveSetting = Sh_SaveConfig('partner2_image', $filename_cb);

                 }else {

                   $query = insertRow(T_CONFIG, [
                       'name' => 'partner2_image',
                       'value' => $filename_cb
                   ]);

                   $saveSetting = $sqlConnect->query($query);

                 }


               }

           }

       }

       if ($saveSetting === true) {
           $data['status'] = 200;
       }


       header("Content-type: application/json");
       echo json_encode($data);
       exit();



    }

    if ($s == "remove_logo_image"){

      $img = $_GET['img'];
      $user_id = $sh['user']['user_id'];

      $upd = Sh_SaveConfig('site_logo', "");

      if ($upd) {

        $data = array(
          'status' => 200,
          'message' => "Logo Picture ".$sh['lang']['general_update_success_message'],
        );

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_update_error_message'],
        );

      }

      unlink($img);

      header("Content-type: application/json");
      echo json_encode($data);
      exit();

    }

    if ($s == "remove_banner_image"){

      $img = $_GET['img'];
      $user_id = $sh['user']['user_id'];

      $upd = Sh_SaveConfig('banner_image', "");

      if ($upd) {

        $data = array(
          'status' => 200,
          'message' => "Banner Image ".$sh['lang']['general_update_success_message'],
        );

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_update_error_message'],
        );

      }

      unlink($img);

      header("Content-type: application/json");
      echo json_encode($data);
      exit();

    }

    if ($s == "remove_partner1_image"){

      $img = $_GET['img'];
      $user_id = $sh['user']['user_id'];

      $upd = Sh_SaveConfig('partner1_image', "");

      if ($upd) {

        $data = array(
          'status' => 200,
          'message' => "Partner 1 Image ".$sh['lang']['general_update_success_message'],
        );

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_update_error_message'],
        );

      }

      unlink($img);

      header("Content-type: application/json");
      echo json_encode($data);
      exit();

    }

    if ($s == "remove_partner2_image"){

      $img = $_GET['img'];
      $user_id = $sh['user']['user_id'];

      $upd = Sh_SaveConfig('partner2_image', "");

      if ($upd) {

        $data = array(
          'status' => 200,
          'message' => "Partner 2 Image ".$sh['lang']['general_update_success_message'],
        );

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_update_error_message'],
        );

      }

      unlink($img);

      header("Content-type: application/json");
      echo json_encode($data);
      exit();

    }

    if ($s == "remove_contact_banner_image"){

      $img = $_GET['img'];
      $user_id = $sh['user']['user_id'];

      $upd = Sh_SaveConfig('contact_banner', "");

      if ($upd) {

        $data = array(
          'status' => 200,
          'message' => "Contact Banner Image ".$sh['lang']['general_update_success_message'],
        );

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_update_error_message'],
        );

      }

      unlink($img);

      header("Content-type: application/json");
      echo json_encode($data);
      exit();

    }

    if ($s == 'update_general_setting' && Sh_CheckSession($hash_id) === true) {

        $saveSetting         = false;
        $delete_follow_table = 0;

        foreach ($_POST as $key => $value) {

            if (isset($sh['config'][$key])) {

                $saveSetting = Sh_SaveConfig($key, $value);

            }else{

                $query = insertRow(T_CONFIG, [
                    'name' => $key,
                    'value' => $value
                ]);

                $saveSetting = $sqlConnect->query($query);

            }
        }


        if ($saveSetting === true) {
            $data['status'] = 200;
        }


        header("Content-type: application/json");
        echo json_encode($data);
        exit();


    }



}
