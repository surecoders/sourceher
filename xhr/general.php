<?php
if ($f == 'general') {

  // create professional
  if ($s == "add_professionals") {

    // get all input form
    $title = Sh_Secure($_POST['title']);
    $status = $_POST['status'];

    $admin_id = $sh['user']['user_id'];


    // check if title is existing already
    $exist_title = getProfessionalTitle($title);

    if ($exist_title) {

      $data = array(
        'status' => 400,
        'message' => $title." ".$sh['lang']['found_already'],
      );

    }else{

      $array_data = array(
        'title' => $title,
        'status' => $status,
        'admin_id' => $admin_id,
        'user_count' => 0,
      );


      $insert_data = CreateProfessionalData($array_data);

      if ($insert_data) {

        $data = array(
          'status' => 200,
          'message' => $sh['lang']['professional']." ".$sh['lang']['success_create'],
        );

      }else{

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_error_message'],
        );

      }


    }


    header("Content-type: application/json");
    echo json_encode($data);
    exit();

  }


  // get professional data
  if ($s == "get_professional") {

    $pro_id = Sh_Secure($_GET['pro_id']);
    $admin_id = Sh_Secure($_GET['user_id']);

    $getProData = getProfessionSingleData($pro_id);

    if ($getProData) { ?>

      <div class="col-md-12">
          <div class="list-title">
              <input type="hidden" name="pro_id" value="<?= $getProData['id']; ?>">
              <input id="formName" type="text" value="<?= $getProData['title'] ?>" placeholder="Professional Title" class="form-control" name="title">
          </div>
          <br>
      </div>


      <div class="col-md-12">
        <div class="list-title">
          <select name="status" class="form-control" required>
              <option value="1" <?= ($getProData['status'] == 1) ? 'selected' : '' ?>>Active</option>
              <option value="0" <?= ($getProData['status'] == 0) ? 'selected' : '' ?>>In-Active</option>
          </select>
        </div>
      </div>

  <?php }else{ ?>

      <h3 style="margin-top: 10px; margin-bottom: 10px; text-align: center;"> Sorry System could not process your request </h3>

<?php }


  }



  // update professional
  if ($s == "update_professional") {

    // get all input form
    $title = Sh_Secure($_POST['title']);
    $status = $_POST['status'];
    $pro_id = $_POST['pro_id'];

    $admin_id = $sh['user']['user_id'];


    // check if title is existing already
    $exist_title = getProfessionalTitleForUpdate($title,$pro_id);

    if ($exist_title) {

      $data = array(
        'status' => 400,
        'message' => $title." ".$sh['lang']['found_already'],
      );

    }else{

      $array_data = array(
        'title' => $title,
        'status' => $status,
      );


      $update_data = UpdateProfessionalData($array_data,$pro_id);

      if ($update_data) {

        $data = array(
          'status' => 200,
          'message' => $sh['lang']['professional']." ".$sh['lang']['general_update_success_message'],
        );

      }else{

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_update_error_message'],
        );

      }


    }

    header("Content-type: application/json");
    echo json_encode($data);
    exit();

  }


  // delete professional
  if ($s == "delete_professional") {

    $pro_id = Sh_Secure($_GET['pro_id']);

    $admin_id = Sh_Secure($_GET['user_id']);

    if ($admin_id == $sh['user']['user_id']) {

      // delete the professional
      $delete_prof = deleteProfessional($pro_id);

      if ($delete_prof) {

        $data = array(
          'status' => 200,
          'message' => $sh['lang']['professional']." ".$sh['lang']['general_delete_success_message'],
        );

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_error_message'],
        );

      }



    }else{

      $data = array(
        'status' => 400,
        'message' => $sh['lang']['user_id_not_valid'],
      );

    }


    header("Content-type: application/json");
    echo json_encode($data);
    exit();

  }





  // add review category
  if ($s == "add_review_category") {
    // get all input form
    $title = Sh_Secure($_POST['title']);
    $status = $_POST['status'];

    $admin_id = $sh['user']['user_id'];


    // check if title is existing already
    $exist_title = getReviewCategoryTitle($title);

    if ($exist_title) {

      $data = array(
        'status' => 400,
        'message' => $title." ".$sh['lang']['found_already'],
      );

    }else{

      $array_data = array(
        'title' => $title,
        'status' => $status,
        'admin_id' => $admin_id,
      );


      $insert_data = CreateReviewCategoryData($array_data);

      if ($insert_data) {

        $data = array(
          'status' => 200,
          'message' => "Review Category ".$sh['lang']['success_create'],
        );

      }else{

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_error_message'],
        );

      }


    }


    header("Content-type: application/json");
    echo json_encode($data);
    exit();
  }


  // get professional data
  if ($s == "get_reviewcategory") {

    $r_cat_id = Sh_Secure($_GET['r_cat_id']);
    $admin_id = Sh_Secure($_GET['user_id']);

    $getr_catData = getReviewCategorySingleData($r_cat_id);

    if ($getr_catData) { ?>

      <div class="col-md-12">
          <div class="list-title">
              <input type="hidden" name="r_cat_id" value="<?= $getr_catData['id']; ?>">
              <input id="formName" type="text" value="<?= $getr_catData['title'] ?>" placeholder="Professional Title" class="form-control" name="title">
          </div>
          <br>
      </div>


      <div class="col-md-12">
        <div class="list-title">
          <select name="status" class="form-control" required>
              <option value="1" <?= ($getr_catData['status'] == 1) ? 'selected' : '' ?>>Active</option>
              <option value="0" <?= ($getr_catData['status'] == 0) ? 'selected' : '' ?>>In-Active</option>
          </select>
        </div>
      </div>

  <?php }else{ ?>

      <h3 style="margin-top: 10px; margin-bottom: 10px; text-align: center;"> Sorry System could not process your request </h3>

<?php }


  }


  // update Review Category
  if ($s == "update_review_category") {

    // get all input form
    $title = Sh_Secure($_POST['title']);
    $status = $_POST['status'];
    $r_cat_id = $_POST['r_cat_id'];

    $admin_id = $sh['user']['user_id'];


    // check if title is existing already
    $exist_title = getReviewCategoryTitleForUpdate($title,$r_cat_id);

    if ($exist_title) {

      $data = array(
        'status' => 400,
        'message' => $title." ".$sh['lang']['found_already'],
      );

    }else{

      $array_data = array(
        'title' => $title,
        'status' => $status,
      );


      $update_data = UpdateReviewCategoryData($array_data,$r_cat_id);

      if ($update_data) {

        $data = array(
          'status' => 200,
          'message' => "Review Category ".$sh['lang']['general_update_success_message'],
        );

      }else{

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_update_error_message'],
        );

      }


    }

    header("Content-type: application/json");
    echo json_encode($data);
    exit();

  }

  // delete review category
  if ($s == "delete_review_category") {

    $r_cat_id = Sh_Secure($_GET['r_cat_id']);

    $admin_id = Sh_Secure($_GET['user_id']);

    if ($admin_id == $sh['user']['user_id']) {

      // delete the professional
      $delete_r_cat = deleteReviewCategory($r_cat_id);

      if ($delete_r_cat) {

        $data = array(
          'status' => 200,
          'message' => "Review Category ".$sh['lang']['general_delete_success_message'],
        );

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_error_message'],
        );

      }



    }else{

      $data = array(
        'status' => 400,
        'message' => $sh['lang']['user_id_not_valid'],
      );

    }


    header("Content-type: application/json");
    echo json_encode($data);
    exit();

  }



//
//
//
// CATEGORY SECTION
//
//
  // upload category image
  if ($s == "upload_cat_image") {

    $cat_code = $_POST['cat_code'];

    // CHECK IF CAT CODE ALREADY EXIST
    $exist_code = checkCatCodeExistence($cat_code);

    if (isset($_FILES['icon_image']) && !empty($_FILES['icon_image'])) {

         if (!empty($_FILES['icon_image']["tmp_name"])) {

             $orignalname_cb = $_FILES['icon_image']["name"];
             $fileInfo_cb = array(
               'file' => $_FILES["icon_image"]["tmp_name"],
               'name' => $_FILES['icon_image']['name'],
               'size' => $_FILES["icon_image"]["size"],
               'type' => $_FILES["icon_image"]["type"],
               'types' => 'jpeg,jpg,png,gif',
             );

             $media_cb = Sh_ShareFile($fileInfo_cb, 0, false);

             if (!empty($media_cb)) {

               $filename_cb = $media_cb['filename'];

               if($exist_code){

                 $arrData = array(
                   'icon_image' => $filename_cb,
                   'icon_type' => "image"
                 );

                 UpdateCategoryData($arrData, $cat_code);

               }else{

                 $arrData = array(
                   'title' => "",
                   'icon_type' => 'image',
                   'icon_image' => $filename_cb,
                   'cat_code' => $cat_code,
                   'status' => 3
                 );

                 CreateCategoryData($arrData);
               }



             }

         }

     }

     if ($saveSetting === true) {
         $data['status'] = 200;
     }


     header("Content-type: application/json");
     echo json_encode($data);
     exit();

  }

  // create category
  if ($s == "add_category") {

    // get all input form
    $title = Sh_Secure($_POST['title']);
    $status = $_POST['status'];

    $cat_code = $_POST['cat_code'];


    // CHECK IF CAT CODE ALREADY EXIST
    $exist_code = checkCatCodeExistence($cat_code);

    // check if title is existing already
    $exist_title = getCategoryTitle($title);

    if ($exist_title) {

      $data = array(
        'status' => 400,
        'message' => $title." ".$sh['lang']['found_already'],
      );

    }else{


      if ($exist_code) {

          $array_data = array(
            'title' => $title,
            'status' => $status,
          );

          $update_data = UpdateCategoryData($array_data, $cat_code);

      }else {

          $array_data = array(
            'title' => $title,
            'cat_code' => $cat_code,
            'icon_type' => "",
            'icon_image' => "",
            'status' => $status,
          );

          $update_data = CreateCategoryData($array_data);

      }


      if ($update_data) {

        $data = array(
          'status' => 200,
          'message' => $sh['lang']['category']." ".$sh['lang']['success_create'],
        );

      }else{

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_error_message'],
        );

      }


    }








    header("Content-type: application/json");
    echo json_encode($data);
    exit();

  }

  // update category
  if ($s == "update_category") {

    // get all input form
    $title = Sh_Secure($_POST['title']);
    $status = $_POST['status'];
    $cat_code = $_POST['cat_code'];
    $cat_id = $_POST['cat_id'];



    // check if title is existing already
    $exist_title = getCategoryTitleForUpdate($title,$cat_code,$cat_id);

    if ($exist_title) {

      $data = array(
        'status' => 400,
        'message' => $title." ".$sh['lang']['found_already'],
      );

    }else{

      $array_data = array(
        'title' => $title,
        'status' => $status,
      );


      $update_data = UpdateCategoryData($array_data, $cat_code);

      if ($update_data) {

        $data = array(
          'status' => 200,
          'message' => $sh['lang']['category']." ".$sh['lang']['general_update_success_message'],
        );

      }else{

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_update_error_message'],
        );

      }


    }

    header("Content-type: application/json");
    echo json_encode($data);
    exit();

  }

  // delete category data
  if ($s == "delete_category") {

    $cat_id = Sh_Secure($_GET['cat_id']);

    $admin_id = Sh_Secure($_GET['user_id']);

    if ($admin_id == $sh['user']['user_id']) {

      // check and update if account are attached to this category
      updateUserCatId($cat_id);

      // delete the professional
      $delete_cat = deleteCategory($cat_id);

      if ($delete_cat) {

        $data = array(
          'status' => 200,
          'message' => $sh['lang']['category']." ".$sh['lang']['general_delete_success_message'],
        );

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_error_message'],
        );

      }



    }else{

      $data = array(
        'status' => 400,
        'message' => $sh['lang']['user_id_not_valid'],
      );

    }


    header("Content-type: application/json");
    echo json_encode($data);
    exit();

  }

  // Remove category image
  if ($s == "remove_cat_image") {

    $img = $_GET['img'];
    $cat_code = $_GET['cat_code'];
    $user_id = $sh['user']['user_id'];

    $dataupd = array(
      'icon_image' => "",
      'icon_type' => ""
    );

    $upd = UpdateCategoryData($dataupd, $cat_code);

    if ($upd) {

      $data = array(
        'status' => 200,
        'message' => "Category Picture ".$sh['lang']['general_update_success_message'],
      );

    }else {

      $data = array(
        'status' => 400,
        'message' => $sh['lang']['general_update_error_message'],
      );

    }

    unlink($img);

    header("Content-type: application/json");
    echo json_encode($data);
    exit();

  }

  // get single category details
  if ($s == "get_single_category") {

    $cat_id = Sh_Secure($_GET['cat_id']);
    $admin_id = Sh_Secure($_GET['user_id']);

    $getr_catData = getCategorySingleData($cat_id);

    if ($getr_catData) { ?>

      <div class="col-md-12 mb-5">
          <div class="list-title">
            <input type="file" class="filepond" id="edit_icon_image" name="icon_image" accept="image/png, image/jpeg, image/gif"/>
          </div>
          <br>
      </div>

      <div class="col-md-12">
          <div class="list-title">
              <input type="hidden" name="cat_id" value="<?= $getr_catData['id'] ?>">
              <input type="hidden" id="edit_cat_code" name="cat_code" value="<?= $getr_catData['cat_code']; ?>">
              <input id="formName" type="text" value="<?= $getr_catData['title'] ?>" placeholder="Category Title" class="form-control" name="title">
          </div>
          <br>
      </div>


      <div class="col-md-12">
        <div class="list-title">
          <select name="status" class="form-control" required>
              <option value="1" <?= ($getr_catData['status'] == 1) ? 'selected' : '' ?>>Active</option>
              <option value="0" <?= ($getr_catData['status'] == 0) ? 'selected' : '' ?>>In-Active</option>
          </select>
        </div>
      </div>


      <script type="text/javascript">
      FilePond.create(
        document.getElementById('edit_icon_image'),{
          labelIdle: `Drag & Drop your picture or <span class="filepond--label-action">Browse</span>`,
          imagePreviewHeight: 70,
          imageCropAspectRatio: '1:1',
          imageResizeTargetWidth: 100,
          imageResizeTargetHeight: 100,
          allowMultiple: false,
          <?php if($getr_catData['icon_image'] != ""): ?>
          files: [
              {
                  // the server file reference
                  source: '<?= $getr_catData['icon_image'] ?>',
                  // set type to local to indicate an already uploaded file
                  options: {
                      type: 'local',
                      file: {
                          name: "<?= $getr_catData['icon_image'] ?>",
                          size: 3001025,
                          type: 'image/jpg',
                      },

                      metadata: {
                          poster: '<?= $getr_catData['cache_icon_image'] ?>',
                      },
                  },

              },
          ]
          <?php endif; ?>
        }
      );

      var edit_cat_code = $("#edit_cat_code").val();
      FilePond.setOptions({
        server: {
          timeout: 7000,
          process: {
              url: Sh_Ajax_Requests_File() + '?f=general&s=upload_cat_image',
              method: 'POST',
              withCredentials: false,
              onload: (response) => {
                  var json = (response);
                  var obj = JSON.parse(json);
                  var status = obj.status;
                  if (status == 200) {

                  }
              },
              onerror: (response) => response.data,
              ondata: (formData) => {
                  formData.append('cat_code', edit_cat_code);
                  return formData;
              },
          },
          revert: (source, load, error) => {

            const request = new XMLHttpRequest();
            request.open('DELETE', Sh_Ajax_Requests_File() + '?f=general&s=remove_cat_image&img=' + source+'&cat_code='+edit_cat_code);

            request.onload = function () {
              if (request.status == 200) {

                load(request.message);
              } else {

                error('oh no');
              }
            };

            request.send();
          },
          remove: (source, load, error) => {

            const request = new XMLHttpRequest();
            request.open('DELETE', Sh_Ajax_Requests_File() + '?f=general&s=remove_cat_image&img=' + source+'&cat_code='+edit_cat_code);

            request.onload = function () {
              if (request.status == 200) {
                load(request.message);
              } else {
                error('oh no');
              }
            };

            request.send();
          }

        }
      });
      </script>

  <?php }else{ ?>

      <h3 style="margin-top: 10px; margin-bottom: 10px; text-align: center;"> Sorry System could not process your request </h3>

<?php }


  }


}
