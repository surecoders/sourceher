<?php
  if ($f == "experts") {

    if ($s == "get_single_expert_data") {

      $user_id = Sh_Secure($_GET['user_id']);
      $admin_id = Sh_Secure($_GET['admin_id']);

      if ($admin_id == $sh['user']['user_id']) {

        $UserData = Sh_UserData($user_id);

        if ($UserData) {

          $pg_type = "experts";

          $pg_data = GetPageTypeData($pg_type);
          $expertsFormData = getFormBuilderDataByPageType($pg_data['id']);

          // get all form data
          $form_id = $expertsFormData['id'];
          $form_code = $expertsFormData['form_code'];
          $form_title = $expertsFormData['title'];
          $form_description = $expertsFormData['description'];
          $form_data = json_decode($expertsFormData['formData'], true);

          $table_code = $pg_type."_".$form_code;
          $fd_count = count($form_data);

          if ($fd_count > 0){

            $lgt = 1;

            foreach ($form_data as $key => $f_field) {

              if ($lgt === $fd_count) {
          ?>
              <div class="col-md-<?= ($lgt % 2 == 0) ? '6' : '12' ?> add_bottom_25">

   <?php      }else { ?>

              <div class="col-md-6 add_bottom_25">

   <?php     }  ?>
                <div class="">
                  <label><b><?= Remove_from_String($f_field['name']) ?> : </b></label> <br>

                  <span><?= getExpertColForSpecUser($f_field['name'],$user_id,$table_code) ?></span>
                </div>



              </div>



<?php
              $lgt++;
            }


        ?>


   <?php  }else{ ?>

          <h3 style="margin-top: 10px; margin-bottom: 10px; text-align: center;"> Sorry System could not process your request </h3>

  <?php   }

        }

      }



    }

    if ($s == "reject_expert_request") {

        $user_id = Sh_Secure($_GET['user_id']);

        $admin_id = Sh_Secure($_GET['admin_id']);

        if ($admin_id == $sh['user']['user_id']) {

          $expert_code = rand(111111, 999999);
          $hash_expert_code = md5($expert_code);

          $user_data = array(
            'account_type' => "user",
            'expert_access_code' => $hash_expert_code,
          );

          $updateUser = UpdateUserData($user_data,$user_id);

          if ($updateUser) {

            $usData = Sh_UserData($user_id);

            $sh['expert_user'] = $usData;
            $body = Sh_LoadPage('emails/expert_reject');
            $send_message_data = array(
                'from_email' => $sh['config']['siteEmail'],
                'from_name' => $sh['config']['siteName'],
                'to_email' => $usData['email'],
                'to_name' => $usData['first_name']." ".$usData['last_name'],
                'subject' => $sh['lang']['expert_message_notification'],
                'charSet' => 'utf-8',
                'message_body' => $body,
                'is_html' => true
            );

            $send  = Sh_SendMessage($send_message_data);

            $data = array(
              'status' => 200,
              'message' => "Expert ".$sh['lang']['general_update_success_message'],
            );

          }else {

            $data = array(
              'status' => 400,
              'message' => $sh['lang']['general_update_error_message'],
            );

          }


       }else {

         $data = array(
           'status' => 400,
           'message' => $sh['lang']['user_id_not_valid'],
         );

       }

       header("Content-type: application/json");
       echo json_encode($data);
       exit();

    }


    if ($s == "approve_expert_request") {

      $user_id = Sh_Secure($_GET['user_id']);

      $admin_id = Sh_Secure($_GET['admin_id']);

      if ($admin_id == $sh['user']['user_id']) {


        $user_data = array(
          'account_type' => "experts",
          'expert_access_code' => "",
        );

        $updateUser = UpdateUserData($user_data,$user_id);

        if ($updateUser) {

          $usData = Sh_UserData($user_id);

          $sh['expert_user'] = $usData;
          $sh['expert_url'] = $sh['config']['site_url']."/".$usData['username'];
          
          $body = Sh_LoadPage('emails/expert_approve');
          $send_message_data = array(
              'from_email' => $sh['config']['siteEmail'],
              'from_name' => $sh['config']['siteName'],
              'to_email' => $usData['email'],
              'to_name' => $usData['first_name']." ".$usData['last_name'],
              'subject' => $sh['lang']['expert_message_notification'],
              'charSet' => 'utf-8',
              'message_body' => $body,
              'is_html' => true
          );

          $send  = Sh_SendMessage($send_message_data);

          $data = array(
            'status' => 200,
            'message' => "Expert ".$sh['lang']['general_update_success_message'],
          );

        }else {

          $data = array(
            'status' => 400,
            'message' => $sh['lang']['general_update_error_message'],
          );

        }


     }else {

       $data = array(
         'status' => 400,
         'message' => $sh['lang']['user_id_not_valid'],
       );

     }

     header("Content-type: application/json");
     echo json_encode($data);
     exit();


    }


  }




 ?>
