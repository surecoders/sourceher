<?php
if ($f == 'users') {

  if ($s == "review_request") {

    // get all form fields
    $title = Sh_Secure($_POST['title']);
    $review = Sh_Secure($_POST['review']);
    $user_id = Sh_Secure($_POST['user_id']);

    $reciever_username = Sh_Secure($_POST['reciever_username']);
    $reciever_user_id = Sh_UserIdFromUsername($reciever_username);

    $code = Sh_GenerateKey(4, 6, false, false, true, false);

    // review category submitted by user
    $review_category = array();
    foreach ($_POST['r_cat'] as $key => $value) {
      $review_category[$key] = $value;
    }

    $r_cat_data = json_encode($review_category);

    if ($sh['config']['automatic_approve_reviews']) {
      $reviewStatus = 1;
      $addmessage = $sh['lang']['waiting_for_admin_approval'];
    }else {
      $reviewStatus = 2;
      $addmessage = "";
    }

    $all_review_data = array(
      'code' => $code,
      'sender_id' => $user_id,
      'user_id' => $reciever_user_id,
      'review_cat_records' => $r_cat_data,
      'title' => $title,
      'review' => $review,
      'status' => $reviewStatus
    );

    if ($user_id != $reciever_user_id) {

      $insertData = InsertReview($all_review_data);

      if ($insertData) {

        $data = array(
          'status' => 200,
          'location' => $sh['config']['site_url'].'/'.$reciever_username,
          'message' => "Review ".$sh['lang']['success_create'],
        );

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_error_message'],
        );

      }

    }else {

      $data = array(
        'status' => 400,
        'message' => $sh['lang']['review_error_same_user_id'],
      );

    }




    header("Content-type: application/json");
    echo json_encode($data);
    exit();


  }


  if ($s == "update_review") {

    $status = $_POST['status'];
    $r_id = $_POST['r_id'];

    $admin_id = $sh['user']['user_id'];

    $array_data = array(
      'status' => $status,
    );


    $update_data = UpdateReviewData($array_data,$r_id);

    if ($update_data) {

      $data = array(
        'status' => 200,
        'message' => $sh['lang']['general_update_success_message'],
      );

    }else{

      $data = array(
        'status' => 400,
        'message' => $sh['lang']['general_update_error_message'],
      );

    }

    header("Content-type: application/json");
    echo json_encode($data);
    exit();


  }


  if ($s == "get_review_Details") {

    $r_id = Sh_Secure($_GET['r_id']);
    $admin_id = Sh_Secure($_GET['user_id']);

    $getRevData = getReviewSingleData($r_id);

    if ($getRevData) {
      $s_d = Sh_UserData($getRevData['sender_id']);
      $re_d = Sh_UserData($getRevData['user_id']);

      $totalNoRatings = ReviewAndRating('total_ratings',$getRevData['user_id']);
      $totalRatings = ReviewAndRating('total_by_all_r_cat',$getRevData['user_id']);

      if ($totalRatings > 0 && $totalRatings < 4.9) {
        $reviewWord = $sh['lang']['review_rating_poor'];
        $color = "warning";
      }elseif ($totalRatings == 0 || $totalNoRatings == 0) {
        $reviewWord = $sh['lang']['no_review_rating'];
        $color = "danger";
      }elseif ($totalRatings > 4.9 && $totalRatings < 7.5) {
        $reviewWord = $sh['lang']['review_rating_superb'];
        $color = "primary";
      }elseif ($totalRatings > 7.5) {
        $reviewWord = $sh['lang']['review_rating_excellent'];
        $color = "success";
      }


    ?>

  <div class="card component-card_4">
        <div class="card-body">
            <div class="user-profile">
                <img src="<?= $re_d['avatar'] ?>" class="" alt="">
            </div>
            <div class="user-info">
                <h5 class="card-user_name"><?= $re_d['name'] ?></h5>
                <p class="card-user_occupation"><?= $re_d['speciality'] ?></p>
                <div class="card-star_rating">
                   <span class="badge badge-<?= $color ?>"><?= $totalRatings ?> (<?= $reviewWord ?>)</span>
                </div>
                <p class="card-text">Title:<?= $getRevData['title'] ?></p>
                <p class="card-text">Review:<?= $getRevData['review'] ?></p>
            </div>
        </div>
        <div class="card-footer">
          <div class="mx-auto my-3">
            <p>This review was submitted by <b><?= $s_d['name'] ?></b> </p>
          </div>
        </div>
    </div>

<?php }

  }


  if ($s == "change_review_status") {

    $r_id = Sh_Secure($_GET['r_id']);
    $admin_id = Sh_Secure($_GET['user_id']);

    $getRevData = getReviewSingleData($r_id);

    if ($getRevData) { ?>

    <div class="col-md-12">
      <div class="my-3 mx-3">
        <p>This review is currently <b><?= ($getRevData['status'] == 1) ? 'Approved' : 'Not Approved' ?></b> </p>
      </div>
    </div>
    <div class="col-md-12">
      <div class="list-title">
        <input type="hidden" name="r_id" value="<?= $getRevData['id'] ?>">
        <select name="status" class="form-control" required>
            <option value="1" <?= ($getRevData['status'] == 1) ? 'selected' : '' ?>>Approved</option>
            <option value="2" <?= ($getRevData['status'] == 2) ? 'selected' : '' ?>>Not Approved</option>
        </select>
      </div>
    </div>

    <?php }

  }


  if ($s == "send_contact_msg") {

    // get all form fields
    $reciever_id = Sh_Secure($_POST['reciever_id']);
    $user_id = Sh_Secure($_POST['user_id']);
    $name = Sh_Secure($_POST['name']);
    $email = Sh_Secure($_POST['email']);
    $subject = Sh_Secure($_POST['subject']);
    $message = Sh_Secure($_POST['message']);

    $code = Sh_GenerateKey(4, 6, false, false, true, false);

    $all_contact_data = array(
      'code' => $code,
      'sender_id' => $user_id,
      'user_id' => $reciever_id,
      'name' => $name,
      'email' => $email,
      'subject' => $subject,
      'message' => $message,
      'status' => 1
    );

    if ($user_id != $reciever_id) {

      $insertData = InsertContactMessage($all_contact_data);

      if ($insertData) {

        // get userdata
        $usData = Sh_UserData($reciever_id);

        $sh['mail_data'] = $_POST;
        $sh['p_data'] = $usData;
        $body = Sh_LoadPage('emails/contact_msg');
        $send_message_data = array(
            'from_email' => (!empty($email)) ? $email : $sh['config']['siteEmail'],
            'from_name' => (!empty($name)) ? $name : $sh['config']['siteName'],
            'to_email' => $usData['email'],
            'to_name' => $usData['first_name']." ".$usData['last_name'],
            'subject' => $sh['lang']['contact_message_notification'],
            'charSet' => 'utf-8',
            'message_body' => $body,
            'is_html' => true
        );

        $send  = Sh_SendMessage($send_message_data);

        if ($send) {
          $data = array(
            'status' => 200,
            'message' => "Message  ".$sh['lang']['success_create'],
          );
        }else {
          $data = array(
            'status' => 400,
            'message' => "Message Could not send ",
          );
        }



      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_error_message'],
        );

      }

    }else {

      $data = array(
        'status' => 400,
        'message' => $sh['lang']['review_error_same_user_id'],
      );

    }




    header("Content-type: application/json");
    echo json_encode($data);
    exit();


  }


  if ($s == "send_real_contact_msg") {

    // get all form fields
    $name = Sh_Secure($_POST['name_contact']);
    $email = Sh_Secure($_POST['email_contact']);
    $subject = Sh_Secure($_POST['subject_contact']);
    $message = Sh_Secure($_POST['message_contact']);

    $code = Sh_GenerateKey(4, 6, false, false, true, false);

    $all_contact_data = array(
      'code' => $code,
      'name' => $name,
      'email' => $email,
      'subject' => $subject,
      'message' => $message,
      'status' => 1
    );

      $insertData = InsertRealContactMessage($all_contact_data);

      if ($insertData) {

        $sh['mail_data'] = $_POST;
        $body = Sh_LoadPage('emails/contact_real_msg');
        $send_message_data = array(
            'from_email' => $email,
            'from_name' => $name,
            'to_email' => (!empty($sh['config']['contact_email'])) ? $sh['config']['contact_email'] : $sh['config']['siteEmail'],
            'to_name' => $sh['config']['siteName'],
            'subject' => $sh['lang']['contact_message_notification'],
            'charSet' => 'utf-8',
            'message_body' => $body,
            'is_html' => true
        );

        $send  = Sh_SendMessage($send_message_data);

        if ($send) {
          $data = array(
            'status' => 200,
            'message' => "Message  ".$sh['lang']['success_create'],
          );
        }else {
          $data = array(
            'status' => 400,
            'message' => "Message Could not send ",
          );
        }

      }else {

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['general_error_message'],
        );

      }


    header("Content-type: application/json");
    echo json_encode($data);
    exit();


  }


  if ($s == "delete_review") {

      $r_id = Sh_Secure($_GET['r_id']);

      $admin_id = Sh_Secure($_GET['user_id']);

      if ($admin_id == $sh['user']['user_id']) {

        // delete the professional
        $delete_rev = deleteReview($r_id);

        if ($delete_rev) {

          $data = array(
            'status' => 200,
            'message' => $sh['lang']['general_delete_success_message'],
          );

        }else {

          $data = array(
            'status' => 400,
            'message' => $sh['lang']['general_error_message'],
          );

        }



      }else{

        $data = array(
          'status' => 400,
          'message' => $sh['lang']['user_id_not_valid'],
        );

      }


      header("Content-type: application/json");
      echo json_encode($data);
      exit();


  }


  if($s == "update_profile_picture"){

    $user_id = Sh_Secure($_POST['user_id']);

    if (isset($_FILES['profile_img']) && !empty($_FILES['profile_img'])) {

         if (!empty($_FILES['profile_img']["tmp_name"])) {

             $orignalname_cb = $_FILES['profile_img']["name"];
             $fileInfo_cb = array(
               'file' => $_FILES["profile_img"]["tmp_name"],
               'name' => $_FILES['profile_img']['name'],
               'size' => $_FILES["profile_img"]["size"],
               'type' => $_FILES["profile_img"]["type"],
               'types' => 'jpeg,jpg,png,gif',
             );

             $media_cb = Sh_ShareFile($fileInfo_cb, 0, false);
             if (!empty($media_cb)) {

               $filename_cb = $media_cb['filename'];

               $arrd = array(
                 'avatar' => $filename_cb,
               );

              $upd = UpdateUserData($arrd,$user_id);

                if ($upd) {

                  $data = array(
                    'status' => 200,
                    'message' => "Profile Picture ".$sh['lang']['general_update_success_message'],
                  );

                }else {

                  $data = array(
                    'status' => 400,
                    'message' => $sh['lang']['general_update_error_message'],
                  );

                }


             }

         }

     }


     header("Content-type: application/json");
     echo json_encode($data);
     exit();


  }


  if ($s == "remove_profile_picture") {

    $img = $_GET['img'];
    $user_id = $sh['user']['user_id'];

    $dataupd = array(
      'avatar' => "",
    );

    $upd = UpdateUserData($dataupd,$user_id);

    if ($upd) {

      $data = array(
        'status' => 200,
        'message' => "Profile Picture ".$sh['lang']['general_update_success_message'],
      );

    }else {

      $data = array(
        'status' => 400,
        'message' => $sh['lang']['general_update_error_message'],
      );

    }

    unlink($img);

    header("Content-type: application/json");
    echo json_encode($data);
    exit();

  }


  if ($s == "update_profile_data") {

    $user_id = Sh_Secure($_POST['user_id']);

    $UserFormData = array();

    foreach ($_POST as $key => $value) {
      $UserFormData[$key] = Sh_Secure($value);
    }

    // remove unused inputs
    unset($UserFormData['button']);
    unset($UserFormData['user_id']);

    $upd = UpdateUserData($UserFormData,$user_id);

    if ($upd) {

      $data = array(
        'status' => 200,
        'message' => "Account ".$sh['lang']['general_update_success_message'],
      );

    }else {

      $data = array(
        'status' => 400,
        'message' => $sh['lang']['general_update_error_message'],
      );

    }

    header("Content-type: application/json");
    echo json_encode($data);
    exit();

  }


  if ($s == "display_experts") { ?>

    <?php

    $name = "";

    if (!empty($_POST['search_by']) && empty($_POST['search_by_mobile'])) {
      $name = $_POST['search_by'];
    }elseif (empty($_POST['search_by']) && !empty($_POST['search_by_mobile'])) {
      $name = $_POST['search_by_mobile'];
    }elseif (!empty($_POST['search_by']) && !empty($_POST['search_by_mobile'])) {
      $name = $_POST['search_by'];
    }

    $n_query = "";

    if (!empty($_POST['query'])) {
      $n_query = $_POST['query'];
      $nquery = " AND `cat_id` = $n_query";
    }else {
      $nquery = "";
    }

    $queryCondition = "";
    $paramType = "";
    $paramValue = array();


    $orderBy = "username";
    $order = "asc";

    if ($order == "asc") {
        $nextOrder = "desc";
    }

    $href = 'index.php';

    $perPage = 9;
    $page = 1;
    if (isset($_POST['pagination_offset'])) {
        $page = $_POST['pagination_offset'];
    }
    $start = ($page - 1) * $perPage;

    if ($start < 0)
        $start = 0;


    $conditionA = " AND (`first_name` LIKE '%$name%' OR `last_name` LIKE '%$name%') ". $nquery ." ORDER BY ".$orderBy." ".$order;

    $conditionB = $conditionA . " limit " . $start . "," . $perPage;


    $getExperts = getExpertsUserData($conditionB);


    if (!empty($getExperts)) {
        $count = count(getExpertsUserData($conditionA));

        $getExperts["perpage"] = showperpage($count, $perPage, $href);
    }else {
      $count = 0;
    }

     ?>
    <div class="page_header">
        <div class="breadcrumbs">
            <ul>
                <li><a href="<?= Sh_Link('') ?>">Home</a></li>
                <li>Experts</li>
            </ul>
        </div>
        <h1>Experts</h1><span>: <span id="counterResult" > <?= $count ?> </span>found</span>
    </div>
    <!-- /page_header -->

    <div class="row isotope-wrapper" id="experts-list">
      <?php

      if(count($getExperts) > 0 || !empty($getExperts)){

        foreach ($getExperts as $k => $gEps) {

          if (is_numeric($k)) {

          $totalNoRatings = ReviewAndRating('total_ratings',$gEps['user_id']);
          $totalRatings = ReviewAndRating('total_by_all_r_cat',$gEps['user_id']);

          if ($totalRatings > 0 && $totalRatings < 4.9) {
            $reviewWord = $sh['lang']['review_rating_poor'];
          }elseif ($totalRatings == 0 || $totalNoRatings == 0) {
            $reviewWord = $sh['lang']['no_review_rating'];
          }elseif ($totalRatings > 4.9 && $totalRatings < 7.5) {
            $reviewWord = $sh['lang']['review_rating_superb'];
          }elseif ($totalRatings > 7.5) {
            $reviewWord = $sh['lang']['review_rating_excellent'];
          }


        ?>
        <!-- popular or  latest -->

      <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 isotope-item popular">
          <div class="strip">
              <figure>
                  <!-- <a href="#0" class="wish_bt"><i class="icon_heart"></i></a> -->
                  <img src="<?= $gEps['avatar'] ?>" data-src="<?= $gEps['avatar'] ?>" class="img-fluid lazy" alt="">
                  <a href="<?= Sh_Link($gEps['username']) ?>" class="strip_info">
                      <div class="item_title">
                          <h3><?= GetSingleProfessioanlDataByCol('title',$gEps['professional']) ?>. <?= $gEps['name'] ?></h3>
                          <small><?= $gEps['speciality'] ?> ...</small>
                      </div>
                  </a>
              </figure>
              <ul>
                  <li><a href="<?= Sh_Link($gEps['username']) ?>" class="tooltip-1" data-toggle="tooltip" data-placement="bottom" title="Contact Experts"><i class="icon_mail"></i></a></li>
                  <li>
                      <div class="score"><span><?= $reviewWord ?><em><?= ($totalNoRatings > 0) ? $totalNoRatings : "0" ?> Reviews</em></span><strong><?= ($totalRatings > 0) ? $totalRatings : "0" ?></strong></div>
                  </li>
              </ul>
          </div>
      </div>

      <!-- /strip grid -->
      <?php
        }
      }
      ?>
      </div>

      <?php if (isset($getExperts["perpage"])) { ?>
      <div class="pagination_fg">

          <?php echo $getExperts["perpage"]; ?>

      </div>


<?php

    }

    }else{  ?>
    <div class="col-xl-12 col-md-12 col-sm-12">
      <div class="error-display">
        <div class="alert alert-danger">
          <?= $sh['lang']['no_record_found'] ?>
        </div>
      </div>
    </div>

<?php  }


}


if ($s == "update_expertise_data") {

  // get all form fields from expertise form
  $expertise_desc = Sh_Secure($_POST['expert_desc']);
  $expertise_title = $_POST['expertise']['title'];
  $expertise_duration = $_POST['expertise']['duration'];

  $user_id = $_POST['user_id'];

  $arr3 = array();
  foreach ($_POST['expertise']['title'] as $key => $value) {
    $arr3[$key][] = array_merge( (array)$value, (array)$_POST['expertise']['duration'][$key]);
  }

  $rrr = json_encode($arr3);
  $expertise_data = array(
    'expert_desc' => $expertise_desc,
    'expertise_data' => $rrr
  );

  $upd = UpdateUserData($expertise_data,$user_id);

    if ($upd) {

      $data = array(
        'status' => 200,
        'message' => "Expertise ".$sh['lang']['general_update_success_message'],
      );

    }else {

      $data = array(
        'status' => 400,
        'message' => $sh['lang']['general_update_error_message'],
      );

    }



    header("Content-type: application/json");
    echo json_encode($data);
    exit();



}


if ($s == "update_professional_data") {

  // get all form fields from expertise form
  $professional_desc = Sh_Secure($_POST['professional_desc']);
  $professional_title = (array) $_POST['professional'];

  $user_id = $_POST['user_id'];

  // $arr3 = array();
  // foreach ($_POST['expertise']['title'] as $key => $value) {
  //   $arr3[$key][] = array_merge( (array)$value, (array)$_POST['expertise']['duration'][$key]);
  // }

  $rrr = json_encode($professional_title);

  $prof_data = array(
    'professional_desc' => $professional_desc,
    'professional_data' => $rrr
  );

  $upd = UpdateUserData($prof_data,$user_id);

    if ($upd) {

      $data = array(
        'status' => 200,
        'message' => "Professional Association ".$sh['lang']['general_update_success_message'],
      );

    }else {

      $data = array(
        'status' => 400,
        'message' => $sh['lang']['general_update_error_message'],
      );

    }



    header("Content-type: application/json");
    echo json_encode($data);
    exit();



}


if ($s == "update_previous_work_data") {

  // get all form fields from expertise form
  $previous_work_desc = Sh_Secure($_POST['previous_work_desc']);
  $previous_work_title = $_POST['previouswork']['title'];
  $previous_work_url = $_POST['previouswork']['url'];

  $user_id = $_POST['user_id'];

  $arr3 = array();
  foreach ($_POST['previouswork']['title'] as $key => $value) {
    $arr3[$key][] = array_merge( (array)$value, (array)$_POST['previouswork']['url'][$key]);
  }

  $rrr = json_encode($arr3);

  $previous_data = array(
    'previous_work_desc' => $expertise_desc,
    'previous_work_data' => $rrr
  );

  $upd = UpdateUserData($previous_data,$user_id);

    if ($upd) {

      $data = array(
        'status' => 200,
        'message' => "Previous Work ".$sh['lang']['general_update_success_message'],
      );

    }else {

      $data = array(
        'status' => 400,
        'message' => $sh['lang']['general_update_error_message'],
      );

    }



    header("Content-type: application/json");
    echo json_encode($data);
    exit();



}

}
