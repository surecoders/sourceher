<?php
  if ($f == "page_creator") {

    if ($s == "create_new_page") {

      $pageTitle = Sh_Secure($_POST['page_title']);
      $pageLocation = Sh_Secure($_POST['page_location']);
      $pageKeyword = Sh_Secure($_POST['page_keyword']);
      $pageDescription = Sh_Secure($_POST['page_description']);
      $pageStatus = 1;

      // page slug
      $pageSlug = Sh_Slugify($pageTitle);

      // check if page name is already existing
      $titleExisting = getPageTitle($pageTitle);

      if ($titleExisting) {

        $data = array(
          'status' => 400,
          'message' => $title." ".$sh['lang']['found_already'],
        );

      }else{

          $pageData = array(
            'page_title' => $pageTitle,
            'page_keyword' => $pageKeyword,
            'slug' => $pageSlug,
            'page_description' => $pageDescription,
            'status' => $pageStatus,
            'page_location' => $pageLocation
          );

          // insert page data into database
          $insertData = ProcessPageData($pageData);

          if ($insertData) {

            $data = array(
              'status' => 200,
              'message' => "Page ".$sh['lang']['success_create'],
            );

          }else {

            $data = array(
              'status' => 400,
              'message' => $sh['lang']['general_error_message'],
            );

          }

      }

      header("Content-type: application/json");
      echo json_encode($data);
      exit();

    }

    if ($s == "edit_page") {

      $page_id = Sh_Secure($_POST['page_id']);
      $pageTitle = Sh_Secure($_POST['page_title']);
      $pageLocation = Sh_Secure($_POST['page_location']);
      $pageKeyword = Sh_Secure($_POST['page_keyword']);
      $pageDescription = Sh_Secure($_POST['page_description']);
      $pageStatus = Sh_Secure($_POST['status']);

      // page slug
      $pageSlug = Sh_Slugify($pageTitle);

      // check if page name is already existing
      $titleExisting = getPageTitleForUpdate($pageTitle,$page_id);

      if ($titleExisting) {

        $data = array(
          'status' => 400,
          'message' => $title." ".$sh['lang']['found_already'],
        );

      }else{

          $pageData = array(
            'page_title' => $pageTitle,
            'page_keyword' => $pageKeyword,
            'slug' => $pageSlug,
            'page_description' => $pageDescription,
            'status' => $pageStatus,
            'page_location' => $pageLocation
          );

          // insert page data into database
          $UpdateData = ProcessUpdatePageData($pageData,$page_id);

          if ($UpdateData) {

            $data = array(
              'status' => 200,
              'message' => "Page ".$sh['lang']['general_update_success_message'],
            );

          }else {

            $data = array(
              'status' => 400,
              'message' => $sh['lang']['general_error_message'],
            );

          }

      }

      header("Content-type: application/json");
      echo json_encode($data);
      exit();

    }

    if ($s == "delete_page") {

          $page_id = Sh_Secure($_GET['page_id']);

          $admin_id = Sh_Secure($_GET['user_id']);

          if ($admin_id == $sh['user']['user_id']) {

            // delete the professional
            $delete_pg = deleteExtralPage($page_id);

            if ($delete_pg) {

              $data = array(
                'status' => 200,
                'message' => "Page ".$sh['lang']['general_delete_success_message'],
              );

            }else {

              $data = array(
                'status' => 400,
                'message' => $sh['lang']['general_error_message'],
              );

            }



          }else{

            $data = array(
              'status' => 400,
              'message' => $sh['lang']['user_id_not_valid'],
            );

          }


          header("Content-type: application/json");
          echo json_encode($data);
          exit();


    }

  }

 ?>
