<?php



@ini_set('session.cookie_httponly',1);
@ini_set('session.use_only_cookies',1);
@header("X-FRAME-OPTIONS: SAMEORIGIN");
if (!version_compare(PHP_VERSION, '5.5.0', '>=')) {
    exit("Required PHP_VERSION >= 5.5.0 , Your PHP_VERSION is : " . PHP_VERSION . "\n");
}

date_default_timezone_set('UTC');
session_start();

@ini_set('gd.jpeg_ignore_warning', 1);




require_once('includes/cache.php');
require_once('includes/general_functions.php');
require_once('includes/tables.php');
require_once('includes/functions_one.php');
require_once('includes/admin_functions.php');


// basic functions


// function pre(...$data){
//     echo "<pre>"; print_r($data);
// }
//
// function isPost(){
//     return count($_POST);
// }
//
// function filters($keys){
//     $output = [];
//     if(count($keys)){
//         foreach($keys as $key){
//             $output[$key] = filter($key);
//         }
//     }
//
//     return $output;
// }
//
// function filter($key, $default = null){
//     $request = $_GET + $_POST + $_REQUEST;
//
//     if(count($request)){
//         if(isset($request[$key])){
//             return $request[$key];
//         }
//     }
//
//     return $default;
// }
//
// function filterUpload($key){
//     $request = $_FILES;
//     if(isset($request[$key])){
//         return $request[$key];
//     }
//
//     return;
// }
//
// function hasRows($result){
//     return $result->num_rows;
// }
//
// function getTime($timestamp){
//     return date('d/m/y', $timestamp);
// }
//
// function getExtension($file){
//     return pathinfo($file, PATHINFO_EXTENSION);
// }
//
// function url($url){
//     global $config;
//     return $config["url"] . $url;
// }
//
//
if (!empty($_GET['ref']) && $sh['loggedin'] == false) {
    $get_ip = get_ip_address();
    if (!isset($_SESSION['ref']) && !empty($get_ip)) {
        $_GET['ref'] = Sh_Secure($_GET['ref']);
        $ref_user_id = Sh_UserIdFromUsername($_GET['ref']);
        $user_data = Sh_UserData($ref_user_id);
        if (!empty($user_data)) {
                $_SESSION['ref'] = $user_data['username'];
        }
    }
}
