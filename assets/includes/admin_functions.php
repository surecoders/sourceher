<?php

// Create Form
function createForm($data){
    global $sh, $sqlConnect;

    if(empty($data) || !isset($data)){
        return false;
    }

    $fields = '`' . implode('`,`', array_keys($data)) . '`';
    $data   = '\'' . implode('\', \'', $data) . '\'';
    $query  = mysqli_query($sqlConnect, "INSERT INTO " . T_FORM_LIST . " ({$fields}) VALUES ({$data})");
    $form_id = mysqli_insert_id($sqlConnect);

    return $form_id;
}


function UpdateForm($data,$code){
  global $sh, $sqlConnect;

  if(empty($data) || !isset($data)){
      return false;
  }

  if(empty($code) || !isset($code)){
      return false;
  }


  $queryU = "UPDATE ".T_FORM_LIST." SET ";

    foreach($data as $key => $value)
    {
        $i++;

        if(sizeof($data) > $i) {
            $queryU .= "`".$key."`= '{$value}', ";
        } else {
            $queryU .= "`".$key."`= '{$value}' ";
        }

    }
    $queryU .= " WHERE `form_code` = '{$code}' ";

    $queryUp  = mysqli_query($sqlConnect,$queryU);
    return $queryUp;

}

function updateUserCatId($cat_id){
  global $sh, $sqlConnect;

  if(empty($cat_id) || !isset($cat_id)){
      return false;
  }

  $queryU = "UPDATE ".T_USERS." SET `cat_id` = 1 WHERE `cat_id` = $cat_id ";

  $queryUp  = mysqli_query($sqlConnect,$queryU);
  return $queryUp;

}


function CountUserByCol($col,$id){
  global $sqlConnect;

  $query = "SELECT COUNT('id') as counter_id FROM ". T_USERS." WHERE $col = $id";
  $pquery = mysqli_query($sqlConnect,$query);
  $vquery = mysqli_fetch_assoc($pquery);

  return $vquery['counter_id'];
}



function getAllForms(){
    global $sh, $sqlConnect;

    $query = "SELECT * FROM ". T_FORM_LIST." ";
    $result = mysqli_query($sqlConnect, $query);
    while($form = mysqli_fetch_assoc($result)){
        $forms[] = $form;
    }
    return $forms;

}


function getPageFormType(){
    global $sh, $sqlConnect;

    $query = "SELECT * FROM ". T_PAGE_TYPE." ";

    $result = mysqli_query($sqlConnect, $query);
    while($form = mysqli_fetch_assoc($result)){
        $forms[] = $form;
    }
    return $forms;

}

function checkifPageTypeIsUsed($id){
  global $sh, $sqlConnect;

  $query = "SELECT `page_type` FROM ". T_FORM_LIST." WHERE `page_type` = $id AND `status` = 1";
  $pquery = mysqli_query($sqlConnect,$query);
  $vquery = mysqli_fetch_assoc($pquery);

  return $vquery['page_type'];
}

function getPageTypeValue($col,$id){
  global $sqlConnect;

  $query = "SELECT $col FROM ". T_PAGE_TYPE." WHERE `id` = $id";
  $pquery = mysqli_query($sqlConnect,$query);
  $vquery = mysqli_fetch_assoc($pquery);

  return $vquery[$col];
}

function getFormValue($col,$id){
  global $sqlConnect;

  $query = "SELECT $col FROM ". T_FORM_LIST." WHERE `id` = $id";
  $pquery = mysqli_query($sqlConnect,$query);
  $vquery = mysqli_fetch_assoc($pquery);

  return $vquery[$col];
}


function deleteForm($formId){
    global $sqlConnect;

    if($formId == 0 || !isset($formId) || !is_numeric($formId)){
        return false;
    }

    // Delete all registration table for this form
    DeleteFormTable($formId);

    $query = mysqli_query($sqlConnect, "DELETE FROM ". T_FORM_LIST ." WHERE `id` = $formId");

    if($query){
        return 1;
    }else{
        return 0;
    }



}


function DeleteFormTable($formId){
  global $sqlConnect;

  $page_type = getFormValue('page_type',$formId);
  $fcode = getFormValue('form_code',$formId);

  $TName = strtolower(getPageTypeValue('name',$page_type))."_".$fcode;

  $query = "DROP TABLE ".$TName;
  $pquery = mysqli_query($sqlConnect,$query);


}

function getFormBuilderData($id) {
    global $sqlConnect;

    if($id == 0 || !isset($id) || !is_numeric($id)){
        return false;
    }

    $query = "SELECT * FROM ". T_FORM_LIST." WHERE `id` = $id";
    $pquery = mysqli_query($sqlConnect,$query);
    $vquery = mysqli_fetch_assoc($pquery);

    return $vquery;

}

function getFormBuilderDataByPageType($page_type) {
    global $sqlConnect;

    if($page_type == 0 || !isset($page_type) || !is_numeric($page_type)){
        return false;
    }

    $query = "SELECT * FROM ". T_FORM_LIST." WHERE `page_type` = $page_type AND `status` = 1";
    $pquery = mysqli_query($sqlConnect,$query);
    $vquery = mysqli_fetch_assoc($pquery);

    return $vquery;

}


function getExpertColForSpecUser($col,$user_id,$table_name){
  global $sqlConnect;

  if($user_id == 0 || !isset($user_id) || !is_numeric($user_id)){
      return false;
  }

  if(empty($col) || !isset($col)){
      return false;
  }

  $query = "SELECT $col FROM `". $table_name. "` WHERE `user_id` = $user_id";
  $pquery = mysqli_query($sqlConnect,$query);
  $vquery = mysqli_fetch_assoc($pquery);

  return $vquery[$col];


}

function createFormTable($data){
  global $sh, $sqlConnect;

  if(empty($data) || !isset($data)){
      return false;
  }

  $fcode = $data['form_code'];
  $formName = $data['page_name'];

  $tableName = $formName."_".$fcode;

  $query = "CREATE TABLE $tableName (id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, user_id INT(11) NOT NULL, created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)";

  $pquery = mysqli_query($sqlConnect,$query);

}



function FormPageNameSettings($code,$type,$new_name,$old_name = null){
  global $sh, $sqlConnect;

  if(empty($code) || !isset($code)){
      return false;
  }

  if(empty($type) || !isset($type)){
      return false;
  }

  if($new_name == 0 || !isset($new_name) || !is_numeric($new_name)){
      return false;
  }

  // path declaration
  $themepath = "themes/".$sh['config']['theme']."/layout/dynamic_form/";

  $fileName = strtolower(getPageTypeValue('name',$new_name))."_".$code.".phtml";
  $Old_fileName = strtolower(getPageTypeValue('name',$old_name))."_".$code.".phtml";

  // make up file name
  $originalFileName = $themepath."".$fileName;
  $Old_originalFileName = $themepath."".$Old_fileName;

  // type == 1 (Create file), type == 2 (Update/rename file), type == 3 (Delete/remove file)
  if ($type == 1) {

    if (!file_exists($originalFileName)) {
      $f = fopen($originalFileName, 'wb');
      if (!$f) {
        die('Error creating the file ' . $fileName);
      }
    }

  }elseif ($type == 2){

    if(file_exists($originalFileName)){


    }else{

       if(rename( $Old_fileName, $fileName)){


       }else{


       }

    }


  }elseif ($type == 3) {

    if(file_exists($originalFileName)){
      unlink($originalFileName);
    }

  }



}



function CreateFormFields($table,$fieldsData){

    global $sh, $sqlConnect;

    if(empty($table) || !isset($table)){
        return false;
    }

    if(empty($fieldsData) || !isset($fieldsData)){
        return false;
    }

    // convert to json_decode
    $convertedFields = json_decode($fieldsData,true);

    // remove duplicated column name
    $temp = array_unique(array_column($convertedFields, 'name'));
	  $unique_arr = array_intersect_key($convertedFields, $temp);

    $fieldNames = [];
    foreach ($unique_arr as $fd) {

        $fieldNames[] = $fd['name'];


        if ($fd['type'] == "text" || $fd['type'] == "email" || $fd['type'] == "radio" || $fd['type'] == "number") {

          $type = "VARCHAR( 255 )";

        }elseif ($fd['type'] == "textarea" || $fd['type'] == "file") {

          $type = "LONGTEXT";

        }

        if ($fd['require'] == true) {

            $cond = "NOT NULL";

        }else{

            $cond = "NULL";

        }

        // column name
        $column = $fd['name'];

        mysqli_query($sqlConnect, "ALTER TABLE `". $table."` ADD `". $column ."` ". $type." ".$cond);
    }

    return 1;

}


// update tables column in database
function UpdateFormFields($table,$fieldsData){

    global $sh, $sqlConnect;

    if(empty($table) || !isset($table)){
        return false;
    }

    if(empty($fieldsData) || !isset($fieldsData)){
        return false;
    }

    // convert to json_decode
    $convertedFields = json_decode($fieldsData,true);

    // remove duplicated column name
    $temp = array_unique(array_column($convertedFields, 'name'));
	  $unique_arr = array_intersect_key($convertedFields, $temp);

    $fieldNames = [];
    foreach ($unique_arr as $fd) {

        $fieldNames[] = $fd['name'];

        if ($fd['type'] == "text" || $fd['type'] == "email" || $fd['type'] == "radio" || $fd['type'] == "number") {

          $type = "VARCHAR( 255 )";

        }elseif ($fd['type'] == "textarea" || $fd['type'] == "file") {

          $type = "LONGTEXT";

        }

        if ($fd['require'] == true) {

            $cond = "NOT NULL";

        }else{

            $cond = "NULL";

        }

        $pq = mysqli_query($sqlConnect, "SELECT COUNT(*) AS existing FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA= '{$sh['dbname']}' AND TABLE_NAME= '{$table}' AND column_name= '{$fd['name']}' ");
        $rpq = mysqli_fetch_assoc($pq);

        if($rpq['existing'] > 0){

          if ( (isset($fd['name']) && isset($fd['editedName'])) || (!empty($fd['editedName']) && !empty($fd['name'])) ) {


            if ( $fd['name'] != $fd['editedName'] ) {

              $oldName = $fd['editedName'];
              $newName = $fd['name'];


              mysqli_query($sqlConnect, "ALTER TABLE `". $table."` CHANGE COLUMN `". $oldName. "` `". $newName ."` ". $type);

            }



          }else{

            // column name
            $column = $fd['name'];

            mysqli_query($sqlConnect, "ALTER TABLE `". $table."` ADD `". $column ."` ". $type." ".$cond);


          }



        }else {


          // column name
          $column = $fd['name'];

          mysqli_query($sqlConnect, "ALTER TABLE `". $table."` ADD `". $column ."` ". $type." ".$cond);

        }

        // if ($fd['name'] == $fd['editedName']) {
        //
        //   // column name
        //   $column = $fd['name'];
        //
        //   mysqli_query($sqlConnect, "ALTER TABLE `". $table."` ADD `". $column ."` ". $type." ".$cond);
        //
        // }elseif ($fd['name'] != $fd['editedName']) {
        //
        //   $oldName = $fd['editedName'];
        //   $newName = $fd['name'];
        //
        //
        //   mysqli_query($sqlConnect, "ALTER TABLE `". $table."` CHANGE COLUMN `". $oldName. "` `". $newName ."` ". $type);
        //
        //
        // }elseif ( (empty($fd['editedName']) || $fd['editedName'] == "" || !isset($fd['editedName'])) &&  !empty($fd['name'])  ) {
        //
        //   // column name
        //   $column = $fd['name'];
        //
        //   mysqli_query($sqlConnect, "ALTER TABLE `". $table."` ADD `". $column ."` ". $type." ".$cond);
        //
        // }elseif ( (!empty($fd['editedName']) || $fd['editedName'] != "") &&  empty($fd['name'])) {
        //
        //   // column name
        //   $column = $fd['editedName'];
        //
        //   mysqli_query($sqlConnect, "ALTER TABLE `". $table."` ADD `". $column ."` ". $type." ".$cond);
        //
        // }else {
        //   // column name
        //   $column = $fd['name'];
        //
        //   mysqli_query($sqlConnect, "ALTER TABLE `". $table."` ADD `". $column ."` ". $type." ".$cond);
        // }


    }

    return 1;

}



function deleteColumn($table, $col){
  global $sh, $sqlConnect;

  if(empty($table) || !isset($table)){
      return false;
  }

  if(empty($col) || !isset($col)){
      return false;
  }

  mysqli_query($sqlConnect, "ALTER TABLE `". $table."` DROP COLUMN `". $col. "`");

  return 1;
}

function getPageTitle($title){
  global $sqlConnect;
  if (empty($title)) {
      return false;
  }
  $title = Sh_Secure($title);
  $query    = mysqli_query($sqlConnect, "SELECT COUNT(`id`) FROM " . T_PAGES . "  WHERE (`page_title` = '{$title}' AND `page_title` LIKE '%{$title}%') ");
  return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}

function getPageTitleForUpdate($title,$page_id){
  global $sqlConnect;

  if (empty($title)) {
      return false;
  }

  if (empty($page_id)) {
      return false;
  }

  $title = Sh_Secure($title);
  $query = mysqli_query($sqlConnect, "SELECT COUNT(`id`) FROM " . T_PAGES . "  WHERE (`page_title` = '{$title}' AND `page_title` LIKE '%{$title}%') AND `id` != $page_id");
  return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}

function getProfessionalTitle($title){
  global $sqlConnect;
  if (empty($title)) {
      return false;
  }
  $title = Sh_Secure($title);
  $query    = mysqli_query($sqlConnect, "SELECT COUNT(`id`) FROM " . T_PROFESSIONAL_PLUG . "  WHERE (`title` = '{$title}' AND `title` LIKE '%{$title}%') ");
  return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}

function getProfessionalTitleForUpdate($title,$pro_id){
  global $sqlConnect;

  if (empty($title)) {
      return false;
  }

  if (empty($pro_id)) {
      return false;
  }

  $title = Sh_Secure($title);
  $query = mysqli_query($sqlConnect, "SELECT COUNT(`id`) FROM " . T_PROFESSIONAL_PLUG . "  WHERE (`title` = '{$title}' AND `title` LIKE '%{$title}%') AND `id` != $pro_id");
  return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}

function CreateProfessionalData($data){
    global $sh, $sqlConnect;

    if(empty($data) || !isset($data)){
        return false;
    }

    $fields = '`' . implode('`,`', array_keys($data)) . '`';
    $data   = '\'' . implode('\', \'', $data) . '\'';
    $query  = mysqli_query($sqlConnect, "INSERT INTO " . T_PROFESSIONAL_PLUG . " ({$fields}) VALUES ({$data})");
    $form_id = mysqli_insert_id($sqlConnect);

    return $form_id;
}

function ProcessPageData($data){
    global $sh, $sqlConnect;

    if(empty($data) || !isset($data)){
        return false;
    }

    $fields = '`' . implode('`,`', array_keys($data)) . '`';
    $data   = '\'' . implode('\', \'', $data) . '\'';
    $query  = mysqli_query($sqlConnect, "INSERT INTO " . T_PAGES . " ({$fields}) VALUES ({$data})");
    $form_id = mysqli_insert_id($sqlConnect);

    return $form_id;
}

function getAllCategoryData($cont = ""){

  global $sqlConnect;

  if ($cont != "") {
    $query = "SELECT * FROM ". T_CATEGORIES." WHERE $cont";
  }else {
    $query = "SELECT * FROM ". T_CATEGORIES."";
  }

  $result = mysqli_query($sqlConnect, $query);
  $forms = array();
  while($form = mysqli_fetch_assoc($result)){
      $forms[] = getCategorySingleData($form['id']);
  }
  return $forms;

}


function getAllPageListData($cont = ""){

  global $sqlConnect;

  if ($cont != "") {
    $query = "SELECT * FROM ". T_PAGES." WHERE $cont";
  }else {
    $query = "SELECT * FROM ". T_PAGES."";
  }

  $result = mysqli_query($sqlConnect, $query);
  $forms = array();
  while($form = mysqli_fetch_assoc($result)){
      $forms[] = $form;
  }
  return $forms;

}


function getAllSubscriberData($cont = ""){

  global $sqlConnect;

  if ($cont != "") {
    $query = "SELECT * FROM ". T_SUBSCRIBERS." WHERE $cont";
  }else {
    $query = "SELECT * FROM ". T_SUBSCRIBERS."";
  }

  $result = mysqli_query($sqlConnect, $query);
  $forms = array();
  while($form = mysqli_fetch_assoc($result)){
      $forms[] = $form;
  }
  return $forms;

}


function getCategoryTitle($title){
  global $sqlConnect;
  if (empty($title)) {
      return false;
  }
  $title = Sh_Secure($title);
  $query    = mysqli_query($sqlConnect, "SELECT COUNT(`id`) FROM " . T_CATEGORIES . "  WHERE (`title` = '{$title}' AND `title` LIKE '%{$title}%') ");
  return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}

function checkCatCodeExistence($code){
  global $sqlConnect;
  if (empty($code)) {
      return false;
  }
  $code = Sh_Secure($code);
  $query    = mysqli_query($sqlConnect, "SELECT COUNT(`id`) FROM " . T_CATEGORIES . "  WHERE (`cat_code` = '{$code}' AND `cat_code` LIKE '%{$code}%') ");
  return (Sh_Sql_Result($query, 0) == 1) ? true : false;

}

function CheckEmailSubscribed($email){

  global $sqlConnect;
  if (empty($email)) {
      return false;
  }
  $email = Sh_Secure($email);
  $query    = mysqli_query($sqlConnect, "SELECT COUNT(`id`) FROM " . T_SUBSCRIBERS . "  WHERE (`email` = '{$email}' AND `email` LIKE '%{$email}%') ");
  return (Sh_Sql_Result($query, 0) == 1) ? true : false;

}

function getCategoryTitleForUpdate($title,$cat_code, $cat_id){
  global $sqlConnect;

  if (empty($title)) {
      return false;
  }

  if (empty($cat_code)) {
      return false;
  }

  if (empty($cat_id)) {
      return false;
  }

  $title = Sh_Secure($title);
  $query = mysqli_query($sqlConnect, "SELECT COUNT(`id`) FROM " . T_CATEGORIES . "  WHERE (`title` = '{$title}' AND `title` LIKE '%{$title}%') AND `cat_code` != {$cat_code} AND id != $cat_id");
  return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}


function getSubscriberEmailForUpdate($email,$sub_id){
  global $sqlConnect;

  if (empty($email)) {
      return false;
  }

  if (empty($sub_id)) {
      return false;
  }

  $email = Sh_Secure($email);
  $query = mysqli_query($sqlConnect, "SELECT COUNT(`id`) FROM " . T_SUBSCRIBERS . "  WHERE (`email` = '{$email}' AND `email` LIKE '%{$email}%') AND id != $sub_id");
  return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}

function deleteCategory($cat_id){
    global $sqlConnect;

    if($cat_id == 0 || !isset($cat_id) || !is_numeric($cat_id)){
        return false;
    }

    $cat_data = getCategorySingleData($cat_id);
    unlink($cat_data['icon_image']);

    $query = mysqli_query($sqlConnect, "DELETE FROM ". T_CATEGORIES ." WHERE `id` = $cat_id");

    if($query){
        return 1;
    }else{
        return 0;
    }

}


function deleteSubscriber($sub_id){
    global $sqlConnect;

    if($sub_id == 0 || !isset($sub_id) || !is_numeric($sub_id)){
        return false;
    }

    $query = mysqli_query($sqlConnect, "DELETE FROM ". T_SUBSCRIBERS ." WHERE `id` = $sub_id");

    if($query){
        return 1;
    }else{
        return 0;
    }

}

function UpdateCategoryData($data,$code_id){
  global $sh, $sqlConnect;

  if(empty($data) || !isset($data)){
      return false;
  }

  if(empty($code_id) || !isset($code_id)){
      return false;
  }


  $queryU = "UPDATE ".T_CATEGORIES." SET ";

    foreach($data as $key => $value)
    {
        $i++;

        if(sizeof($data) > $i) {
            $queryU .= "`".$key."`= '{$value}', ";
        } else {
            $queryU .= "`".$key."`= '{$value}' ";
        }

    }
    $queryU .= " WHERE `cat_code` = $code_id ";

    $queryUp  = mysqli_query($sqlConnect,$queryU);
    return $queryUp;

}


function UpdateSubscriberData($data,$sub_id){
  global $sh, $sqlConnect;

  if(empty($data) || !isset($data)){
      return false;
  }

  if(empty($sub_id) || !isset($sub_id)){
      return false;
  }


  $queryU = "UPDATE ".T_SUBSCRIBERS." SET ";

    foreach($data as $key => $value)
    {
        $i++;

        if(sizeof($data) > $i) {
            $queryU .= "`".$key."`= '{$value}', ";
        } else {
            $queryU .= "`".$key."`= '{$value}' ";
        }

    }
    $queryU .= " WHERE `id` = $sub_id ";

    $queryUp  = mysqli_query($sqlConnect,$queryU);
    return $queryUp;

}


function ProcessUpdatePageData($data,$page_id){
  global $sh, $sqlConnect;

  if(empty($data) || !isset($data)){
      return false;
  }

  if(empty($page_id) || !isset($page_id)){
      return false;
  }


  $queryU = "UPDATE ".T_PAGES." SET ";

    foreach($data as $key => $value)
    {
        $i++;

        if(sizeof($data) > $i) {
            $queryU .= "`".$key."`= '{$value}', ";
        } else {
            $queryU .= "`".$key."`= '{$value}' ";
        }

    }
    $queryU .= " WHERE `id` = $page_id ";

    $queryUp  = mysqli_query($sqlConnect,$queryU);
    return $queryUp;

}

function CreateCategoryData($data){
    global $sh, $sqlConnect;

    if(empty($data) || !isset($data)){
        return false;
    }

    $fields = '`' . implode('`,`', array_keys($data)) . '`';
    $data   = '\'' . implode('\', \'', $data) . '\'';
    $query  = mysqli_query($sqlConnect, "INSERT INTO " . T_CATEGORIES . " ({$fields}) VALUES ({$data})");
    $form_id = mysqli_insert_id($sqlConnect);

    return $form_id;
}


function CreateSubscribers($data){

  global $sh, $sqlConnect;

  if(empty($data) || !isset($data)){
      return false;
  }

  $fields = '`' . implode('`,`', array_keys($data)) . '`';
  $data   = '\'' . implode('\', \'', $data) . '\'';
  $query  = mysqli_query($sqlConnect, "INSERT INTO " . T_SUBSCRIBERS . " ({$fields}) VALUES ({$data})");
  $form_id = mysqli_insert_id($sqlConnect);

  return $form_id;

}


function getCategorySingleData($cat_id){

  global $sqlConnect;

  if($cat_id == 0 || !isset($cat_id) || !is_numeric($cat_id)){
      return false;
  }

  $query_one = "SELECT * FROM ". T_CATEGORIES. " WHERE `id` = $cat_id";

  $hashed_cat_Id = md5($cat_id);
  if ($sh['config']['cacheSystem'] == 1) {
      $fetched_data = $cache->read($hashed_cat_Id . '_Cat_Data.tmp');
      if (empty($fetched_data)) {
          $sql          = mysqli_query($sqlConnect, $query_one);
          $fetched_data = mysqli_fetch_assoc($sql);
          $cache->write($hashed_cat_Id . '_Cat_Data.tmp', $fetched_data);
      }
  } else {
      $sql          = mysqli_query($sqlConnect, $query_one);
      $fetched_data = mysqli_fetch_assoc($sql);
  }

  if (empty($fetched_data)) {
      return array();
  }

  $fetched_data['cache_icon_image'] = Sh_GetMedia($fetched_data['icon_image']) . '?cache=' . $fetched_data['date_created'];

  return $fetched_data;

}


function getAllProfessionalData($cont = ""){
    global $sqlConnect;

    if ($cont != "") {
      $query = "SELECT * FROM ". T_PROFESSIONAL_PLUG." WHERE $cont";
    }else {
      $query = "SELECT * FROM ". T_PROFESSIONAL_PLUG."";
    }

    $result = mysqli_query($sqlConnect, $query);
    while($form = mysqli_fetch_assoc($result)){
        $forms[] = $form;
    }
    return $forms;

}


function getProfessionSingleData($pro_id){
    global $sqlConnect;

    if($pro_id == 0 || !isset($pro_id) || !is_numeric($pro_id)){
        return false;
    }

    $query = mysqli_query($sqlConnect, "SELECT * FROM ". T_PROFESSIONAL_PLUG. " WHERE `id` = $pro_id");
    $pquery = mysqli_fetch_assoc($query);

    return $pquery;
}

function getSinglePageData($page_id){
  global $sqlConnect;

  if($page_id == 0 || !isset($page_id) || !is_numeric($page_id)){
      return false;
  }

  $query = mysqli_query($sqlConnect, "SELECT * FROM ". T_PAGES. " WHERE `id` = $page_id");
  $pquery = mysqli_fetch_assoc($query);

  return $pquery;

}


function getSubscriberSingleData($sub_id){
    global $sqlConnect;

    if($sub_id == 0 || !isset($sub_id) || !is_numeric($sub_id)){
        return false;
    }

    $query = mysqli_query($sqlConnect, "SELECT * FROM ". T_SUBSCRIBERS. " WHERE `id` = $sub_id");
    $pquery = mysqli_fetch_assoc($query);

    return $pquery;
}


function UpdateProfessionalData($data,$pro_id){
  global $sh, $sqlConnect;

  if(empty($data) || !isset($data)){
      return false;
  }

  if(empty($pro_id) || !isset($pro_id)){
      return false;
  }


  $queryU = "UPDATE ".T_PROFESSIONAL_PLUG." SET ";

    foreach($data as $key => $value)
    {
        $i++;

        if(sizeof($data) > $i) {
            $queryU .= "`".$key."`= '{$value}', ";
        } else {
            $queryU .= "`".$key."`= '{$value}' ";
        }

    }
    $queryU .= " WHERE `id` = $pro_id ";

    $queryUp  = mysqli_query($sqlConnect,$queryU);
    return $queryUp;

}

function deleteProfessional($pro_id){
    global $sqlConnect;

    if($pro_id == 0 || !isset($pro_id) || !is_numeric($pro_id)){
        return false;
    }

    $query = mysqli_query($sqlConnect, "DELETE FROM ". T_PROFESSIONAL_PLUG ." WHERE `id` = $pro_id");

    if($query){
        return 1;
    }else{
        return 0;
    }

}

function deleteExtralPage($page_id){
    global $sqlConnect;

    if($page_id == 0 || !isset($page_id) || !is_numeric($page_id)){
        return false;
    }

    $query = mysqli_query($sqlConnect, "DELETE FROM ". T_PAGES ." WHERE `id` = $page_id");

    if($query){
        return 1;
    }else{
        return 0;
    }

}


function getallUserAccountData($cond = ""){
  global $sqlConnect;

  if( isset($cond) || !empty($cond) ){
    $query = "SELECT * FROM ". T_USERS." ".$cond;
  }else {
    $query = "SELECT * FROM ". T_USERS." ";
  }

  $result = mysqli_query($sqlConnect, $query);
  while($user_data = mysqli_fetch_assoc($result)){
      $users[] = Sh_UserData($user_data['user_id']);
  }
  return $users;

}


function UpdateUserData($data,$user_id){
  global $sh, $sqlConnect;

  if(empty($data) || !isset($data)){
      return false;
  }

  if(empty($user_id) || !isset($user_id)){
      return false;
  }


  $queryU = "UPDATE ".T_USERS." SET ";

    foreach($data as $key => $value)
    {
        $i++;

        if(sizeof($data) > $i) {
            $queryU .= "`".$key."`= '{$value}', ";
        } else {
            $queryU .= "`".$key."`= '{$value}' ";
        }

    }
    $queryU .= " WHERE `user_id` = $user_id ";

    $queryUp  = mysqli_query($sqlConnect,$queryU);
    return $queryUp;

}


function getReviewCategoryTitle($title){
  global $sqlConnect;
  if (empty($title)) {
      return false;
  }
  $title = Sh_Secure($title);
  $query    = mysqli_query($sqlConnect, "SELECT COUNT(`id`) FROM " . T_REVIEW_CAT . "  WHERE (`title` = '{$title}' AND `title` LIKE '%{$title}%') ");
  return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}

function CreateReviewCategoryData($data){
    global $sh, $sqlConnect;

    if(empty($data) || !isset($data)){
        return false;
    }

    $fields = '`' . implode('`,`', array_keys($data)) . '`';
    $data   = '\'' . implode('\', \'', $data) . '\'';
    $query  = mysqli_query($sqlConnect, "INSERT INTO " . T_REVIEW_CAT . " ({$fields}) VALUES ({$data})");
    $form_id = mysqli_insert_id($sqlConnect);

    return $form_id;
}


function getAllReviewCategoryData($cont = ""){
    global $sqlConnect;

    if ($cont != "") {
      $query = "SELECT * FROM ". T_REVIEW_CAT." WHERE $cont";
    }else {
      $query = "SELECT * FROM ". T_REVIEW_CAT."";
    }

    $result = mysqli_query($sqlConnect, $query);
    while($form = mysqli_fetch_assoc($result)){
        $forms[] = $form;
    }
    return $forms;

}

function getAllReviewData($cont = ""){
    global $sqlConnect;

    if ($cont != "") {
      $query = "SELECT * FROM ". T_REVIEWS." WHERE $cont";
    }else {
      $query = "SELECT * FROM ". T_REVIEWS."";
    }

    $result = mysqli_query($sqlConnect, $query);
    while($form = mysqli_fetch_assoc($result)){
        $forms[] = $form;
    }
    return $forms;

}

function getReviewSingleData($r_id){

  global $sqlConnect;

  if($r_id == 0 || !isset($r_id) || !is_numeric($r_id)){
      return false;
  }

  $query = mysqli_query($sqlConnect, "SELECT * FROM ". T_REVIEWS. " WHERE `id` = $r_id");
  $pquery = mysqli_fetch_assoc($query);

  return $pquery;

}


function getReviewCategorySingleData($r_cat_id){

  global $sqlConnect;

  if($r_cat_id == 0 || !isset($r_cat_id) || !is_numeric($r_cat_id)){
      return false;
  }

  $query = mysqli_query($sqlConnect, "SELECT * FROM ". T_REVIEW_CAT. " WHERE `id` = $r_cat_id");
  $pquery = mysqli_fetch_assoc($query);

  return $pquery;

}


function UpdateReviewCategoryData($data,$r_cat_id){
  global $sh, $sqlConnect;

  if(empty($data) || !isset($data)){
      return false;
  }

  if(empty($r_cat_id) || !isset($r_cat_id)){
      return false;
  }


  $queryU = "UPDATE ".T_REVIEW_CAT." SET ";

    foreach($data as $key => $value)
    {
        $i++;

        if(sizeof($data) > $i) {
            $queryU .= "`".$key."`= '{$value}', ";
        } else {
            $queryU .= "`".$key."`= '{$value}' ";
        }

    }
    $queryU .= " WHERE `id` = $r_cat_id ";

    $queryUp  = mysqli_query($sqlConnect,$queryU);
    return $queryUp;

}

function UpdateReviewData($data,$r_id){
  global $sh, $sqlConnect;

  if(empty($data) || !isset($data)){
      return false;
  }

  if(empty($r_id) || !isset($r_id)){
      return false;
  }


  $queryU = "UPDATE ".T_REVIEWS." SET ";

    foreach($data as $key => $value)
    {
        $i++;

        if(sizeof($data) > $i) {
            $queryU .= "`".$key."`= '{$value}', ";
        } else {
            $queryU .= "`".$key."`= '{$value}' ";
        }

    }
    $queryU .= " WHERE `id` = $r_id ";

    $queryUp  = mysqli_query($sqlConnect,$queryU);
    return $queryUp;

}

function deleteReviewCategory($r_cat_id){
    global $sqlConnect;

    if($r_cat_id == 0 || !isset($r_cat_id) || !is_numeric($r_cat_id)){
        return false;
    }

    $query = mysqli_query($sqlConnect, "DELETE FROM ". T_REVIEW_CAT ." WHERE `id` = $r_cat_id");

    if($query){
        return 1;
    }else{
        return 0;
    }

}

function deleteReview($r_id){
    global $sqlConnect;

    if($r_id == 0 || !isset($r_id) || !is_numeric($r_id)){
        return false;
    }

    $query = mysqli_query($sqlConnect, "DELETE FROM ". T_REVIEWS ." WHERE `id` = $r_id");

    if($query){
        return 1;
    }else{
        return 0;
    }

}


function getReviewCategoryTitleForUpdate($title,$r_cat_id){
  global $sqlConnect;

  if (empty($title)) {
      return false;
  }

  if (empty($r_cat_id)) {
      return false;
  }

  $title = Sh_Secure($title);
  $query = mysqli_query($sqlConnect, "SELECT COUNT(`id`) FROM " . T_REVIEW_CAT . "  WHERE (`title` = '{$title}' AND `title` LIKE '%{$title}%') AND `id` != $r_cat_id");
  return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}


function deleteExpertDataRow($user_id){

  global $sqlConnect;


  if (empty($user_id)) {
      return false;
  }

  $pg_type = "experts";

  $pg_data = GetPageTypeData($pg_type);
  $expertsFormData = getFormBuilderDataByPageType($pg_data['id']);

  // get all form data
  $form_id = $expertsFormData['id'];
  $form_code = $expertsFormData['form_code'];

  $table_code = $pg_type."_".$form_code;

  $query = mysqli_query($sqlConnect, "DELETE FROM ". $table_code ." WHERE `user_id` = $user_id");

  if($query){
      return 1;
  }else{
      return 0;
  }

}

 ?>
