<?php

function Sh_GetConfig() {
    global $sqlConnect;
    $data  = array();
    $query = mysqli_query($sqlConnect, "SELECT * FROM " . T_CONFIG);
    while ($fetched_data = mysqli_fetch_assoc($query)) {
        $data[$fetched_data['name']] = $fetched_data['value'];
    }
    return $data;
}

function Sh_GetNotifyMessages() {
    global $sqlConnect;
    $data  = array();
    $query = mysqli_query($sqlConnect, "SELECT * FROM " . T_NOTIFY_MESSAGES);
    while ($fetched_data = mysqli_fetch_assoc($query)) {
        $data[$fetched_data['name']] = $fetched_data['value'];
    }
    return $data;
}

function Sh_LangsNamesFromDB($lang = 'english') {
    global $sqlConnect, $sh;
    $data  = array();
    $query = mysqli_query($sqlConnect, "SHOW COLUMNS FROM " . T_LANGS);
    while ($fetched_data = mysqli_fetch_assoc($query)) {
        $data[] = $fetched_data['Field'];
    }
    unset($data[0]);
    unset($data[1]);
    unset($data[2]);
    return $data;
}

function Sh_UserIdFromEmail($email) {
    global $sqlConnect;
    if (empty($email)) {
        return false;
    }
    $email = Sh_Secure($email);
    $query = mysqli_query($sqlConnect, "SELECT `user_id` FROM " . T_USERS . " WHERE `email` = '{$email}'");
    return Sh_Sql_Result($query, 0, 'user_id');
}
function Sh_UserIDFromEmailCode($email_code) {
    global $sqlConnect;
    if (empty($email_code)) {
        return false;
    }
    $email_code = Sh_Secure($email_code);
    $query      = mysqli_query($sqlConnect, "SELECT `user_id` FROM " . T_USERS . " WHERE `email_code` = '{$email_code}'");
    return Sh_Sql_Result($query, 0, 'user_id');
}

function Sh_CustomCode($a = false,$code = array()){
    global $sh;
    $theme       = $sh['config']['theme'];
    $data        = array();
    $result      = false;
    $custom_code = array(
        "themes/$theme/custom/js/head.js",
        "themes/$theme/custom/js/footer.js",
        "themes/$theme/custom/css/style.css",
    );
    if ($a == 'g') {
        foreach ($custom_code as $key => $filepath) {
            if (is_readable($filepath)) {
                $data[$key] = file_get_contents($filepath);
            }
        }
        $result = $data;
    }
    else if($a == 'p' && !empty($code)){
        foreach ($code as $key => $content) {
            if (is_writable($custom_code[$key])) {
                @file_put_contents($custom_code[$key],$content);
            }
        }
        $result = true;
    }
    return $result;
}

function Sh_IsLogged() {
    if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])) {
        $id = Sh_GetUserFromSessionID($_SESSION['user_id']);
        if (is_numeric($id) && !empty($id)) {
            return true;
        }
    } else if (!empty($_COOKIE['user_id']) && !empty($_COOKIE['user_id'])) {
        $id = Sh_GetUserFromSessionID($_COOKIE['user_id']);
        if (is_numeric($id) && !empty($id)) {
            return true;
        }
    } else {
        return false;
    }
}
//
function Sh_Secure($string, $censored_words = 1, $br = true, $strip = 0) {
    global $sqlConnect;
    $string = trim($string);
    $string = cleanString($string);
    $string = mysqli_real_escape_string($sqlConnect, $string);
    $string = htmlspecialchars($string, ENT_QUOTES);
    if ($br == true) {
        $string = str_replace('\r\n', " <br>", $string);
        $string = str_replace('\n\r', " <br>", $string);
        $string = str_replace('\r', " <br>", $string);
        $string = str_replace('\n', " <br>", $string);
    } else {
        $string = str_replace('\r\n', "", $string);
        $string = str_replace('\n\r', "", $string);
        $string = str_replace('\r', "", $string);
        $string = str_replace('\n', "", $string);
    }
    if ($strip == 1) {
        $string = stripslashes($string);
    }
    $string = str_replace('&amp;#', '&#', $string);
    return $string;
}

function cleanString($string) {
    return $string = preg_replace("/&#?[a-z0-9]+;/i","", $string);
}

function Sh_GetUserFromSessionID($session_id, $platform = 'web') {
    global $sqlConnect, $db;
    if (empty($session_id)) {
        return false;
    }

    $session_id = Sh_Secure($session_id);
    $query      = mysqli_query($sqlConnect, "SELECT * FROM " . T_APP_SESSIONS . " WHERE `session_id` = '{$session_id}' LIMIT 1");
    $fetched_data = mysqli_fetch_assoc($query);
    if (empty($fetched_data['platform_details']) && $fetched_data['platform'] == 'web') {
        $ua = json_encode(getBrowser());
        if (isset($fetched_data['platform_details'])) {
            $update_session = $db->where('id', $fetched_data['id'])->update(T_APP_SESSIONS, array('platform_details' => $ua));
        }
    }
    return $fetched_data['user_id'];
}


function getBrowser() {
      $u_agent = $_SERVER['HTTP_USER_AGENT'];
      $bname = 'Unknown';
      $platform = 'Unknown';
      $version= "";
      // First get the platform?
      if (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
      } elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
      } elseif (preg_match('/iphone|IPhone/i', $u_agent)) {
        $platform = 'IPhone Web';
      } elseif (preg_match('/android|Android/i', $u_agent)) {
        $platform = 'Android Web';
      } else if (preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $u_agent)) {
        $platform = 'Mobile';
      } else if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
      }
      // Next get the name of the useragent yes seperately and for good reason
      if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
      } elseif(preg_match('/Firefox/i',$u_agent)) {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
      } elseif(preg_match('/Chrome/i',$u_agent)) {
        $bname = 'Google Chrome';
        $ub = "Chrome";
      } elseif(preg_match('/Safari/i',$u_agent)) {
        $bname = 'Apple Safari';
        $ub = "Safari";
      } elseif(preg_match('/Opera/i',$u_agent)) {
        $bname = 'Opera';
        $ub = "Opera";
      } elseif(preg_match('/Netscape/i',$u_agent)) {
        $bname = 'Netscape';
        $ub = "Netscape";
      }
      // finally get the correct version number
      $known = array('Version', $ub, 'other');
      $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
      if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
      }
      // see how many we have
      $i = count($matches['browser']);
      if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
          $version= $matches['version'][0];
        } else {
          $version= $matches['version'][1];
        }
      } else {
        $version= $matches['version'][0];
      }
      // check if we have a number
      if ($version==null || $version=="") {$version="?";}
      return array(
          'userAgent' => $u_agent,
          'name'      => $bname,
          'version'   => $version,
          'platform'  => $platform,
          'pattern'    => $pattern,
          'ip_address' => get_ip_address()
      );
}

function get_ip_address() {
    if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED']))
        return $_SERVER['HTTP_X_FORWARDED'];
    if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR']))
        return $_SERVER['HTTP_FORWARDED_FOR'];
    if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED']))
        return $_SERVER['HTTP_FORWARDED'];
    return $_SERVER['REMOTE_ADDR'];
}

function validate_ip($ip) {
    if (strtolower($ip) === 'unknown')
        return false;
    $ip = ip2long($ip);
    if ($ip !== false && $ip !== -1) {
        $ip = sprintf('%u', $ip);
        if ($ip >= 0 && $ip <= 50331647)
            return false;
        if ($ip >= 167772160 && $ip <= 184549375)
            return false;
        if ($ip >= 2130706432 && $ip <= 2147483647)
            return false;
        if ($ip >= 2851995648 && $ip <= 2852061183)
            return false;
        if ($ip >= 2886729728 && $ip <= 2887778303)
            return false;
        if ($ip >= 3221225984 && $ip <= 3221226239)
            return false;
        if ($ip >= 3232235520 && $ip <= 3232301055)
            return false;
        if ($ip >= 4294967040)
            return false;
    }
    return true;
}

function Sh_Redirect($url) {
    return header("Location: {$url}");
}

function Sh_Link($string) {
    global $site_url;
    return $site_url . '/' . $string;
}

function Sh_Sql_Result($res, $row = 0, $col = 0) {
    $numrows = mysqli_num_rows($res);
    if ($numrows && $row <= ($numrows - 1) && $row >= 0) {
        mysqli_data_seek($res, $row);
        $resrow = (is_numeric($col)) ? mysqli_fetch_row($res) : mysqli_fetch_assoc($res);
        if (isset($resrow[$col])) {
            return $resrow[$col];
        }
    }
    return false;
}

function Sh_CleanCache($user_id = '', $where = 'sidebar') {
    global $sh;
    if ($sh['config']['cache_sidebar'] == 0 || $sh['loggedin'] == false) {
        return false;
    }
    $file_path = './cache/sidebar-' . $sh['user']['user_id'] . '.tpl';
    if (file_exists($file_path)) {
        unlink($file_path);
    }
}

function ip_in_range($ip, $range) {
    if (strpos($range, '/') == false) {
        $range .= '/32';
    }
    // $range is in IP/CIDR format eg 127.0.0.1/24
    list($range, $netmask) = explode('/', $range, 2);
    $range_decimal    = ip2long($range);
    $ip_decimal       = ip2long($ip);
    $wildcard_decimal = pow(2, (32 - $netmask)) - 1;
    $netmask_decimal  = ~$wildcard_decimal;
    return (($ip_decimal & $netmask_decimal) == ($range_decimal & $netmask_decimal));
}

function Sh_LoadPage($page_url = '') {
    global $sh,$db;
    $create_file = false;
    if ($page_url == 'sidebar/content' && $sh['loggedin'] == true && $sh['config']['cache_sidebar'] == 1) {
        $file_path = './cache/sidebar-' . $sh['user']['user_id'] . '.tpl';
        if (file_exists($file_path)) {
           $get_file = file_get_contents($file_path);
           if (!empty($get_file)) {
               return $get_file;
           }
        } else {
            $create_file = true;
        }
    }
    $page         = './themes/' . $sh['config']['theme'] . '/layout/' . $page_url . '.phtml';
    $page_content = '';
    ob_start();
    require($page);
    $page_content = ob_get_contents();
    ob_end_clean();
    if ($create_file == true && $sh['config']['cache_sidebar'] == 1) {
        $create_sidebar_file = file_put_contents($file_path, $page_content);
        setcookie("last_sidebar_update", time(), time() + (10 * 365 * 24 * 60 * 60));
    }
    return $page_content;
}

function Sh_LangsFromDB($lang = 'english') {
    global $sqlConnect, $sh;
    $data  = array();
    $query = mysqli_query($sqlConnect, "SELECT `lang_key`, `$lang` FROM " . T_LANGS);
    while ($fetched_data = mysqli_fetch_assoc($query)) {
        $data[$fetched_data['lang_key']] = htmlspecialchars_decode($fetched_data[$lang]);
    }
    return $data;
}

function Sh_LoadAdminPage($page_url = '') {
    global $sh,$db;
    $page = './admin-panel/pages/' . $page_url . '.phtml';
    $page_content = '';
    ob_start();
    require($page);
    $page_content = ob_get_contents();
    ob_end_clean();
    return $page_content;
}

function Sh_LoadAdminLink($link = '') {
    global $site_url;
    return $site_url . '/admin-panel/' . $link;
}

function Sh_LoadAdminLinkSettings($link = '') {
    global $site_url;
    return $site_url . '/admin-cp/' . $link;
}

function Sh_Slugify($text, string $divider = '-')
{
  // replace non letter or digits by divider
  $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, $divider);

  // remove duplicate divider
  $text = preg_replace('~-+~', $divider, $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

function url_slug($str, $options = array()) {
    // Make sure string is in UTF-8 and strip invalid UTF-8 characters
    $str      = mb_convert_encoding((string) $str, 'UTF-8', mb_list_encodings());
    $defaults = array(
        'delimiter' => '-',
        'limit' => null,
        'lowercase' => true,
        'replacements' => array(),
        'transliterate' => true
    );
    // Merge options
    $options  = array_merge($defaults, $options);
    $char_map = array(
        // Latin
        'À' => 'A',
        'Á' => 'A',
        'Â' => 'A',
        'Ã' => 'A',
        'Ä' => 'A',
        'Å' => 'A',
        'Æ' => 'AE',
        'Ç' => 'C',
        'È' => 'E',
        'É' => 'E',
        'Ê' => 'E',
        'Ë' => 'E',
        'Ì' => 'I',
        'Í' => 'I',
        'Î' => 'I',
        'Ï' => 'I',
        'Ð' => 'D',
        'Ñ' => 'N',
        'Ò' => 'O',
        'Ó' => 'O',
        'Ô' => 'O',
        'Õ' => 'O',
        'Ö' => 'O',
        'Ő' => 'O',
        'Ø' => 'O',
        'Ù' => 'U',
        'Ú' => 'U',
        'Û' => 'U',
        'Ü' => 'U',
        'Ű' => 'U',
        'Ý' => 'Y',
        'Þ' => 'TH',
        'ß' => 'ss',
        'à' => 'a',
        'á' => 'a',
        'â' => 'a',
        'ã' => 'a',
        'ä' => 'a',
        'å' => 'a',
        'æ' => 'ae',
        'ç' => 'c',
        'è' => 'e',
        'é' => 'e',
        'ê' => 'e',
        'ë' => 'e',
        'ì' => 'i',
        'í' => 'i',
        'î' => 'i',
        'ï' => 'i',
        'ð' => 'd',
        'ñ' => 'n',
        'ò' => 'o',
        'ó' => 'o',
        'ô' => 'o',
        'õ' => 'o',
        'ö' => 'o',
        'ő' => 'o',
        'ø' => 'o',
        'ù' => 'u',
        'ú' => 'u',
        'û' => 'u',
        'ü' => 'u',
        'ű' => 'u',
        'ý' => 'y',
        'þ' => 'th',
        'ÿ' => 'y',
        // Latin symbols
        '©' => '(c)',
        // Greek
        'Α' => 'A',
        'Β' => 'B',
        'Γ' => 'G',
        'Δ' => 'D',
        'Ε' => 'E',
        'Ζ' => 'Z',
        'Η' => 'H',
        'Θ' => '8',
        'Ι' => 'I',
        'Κ' => 'K',
        'Λ' => 'L',
        'Μ' => 'M',
        'Ν' => 'N',
        'Ξ' => '3',
        'Ο' => 'O',
        'Π' => 'P',
        'Ρ' => 'R',
        'Σ' => 'S',
        'Τ' => 'T',
        'Υ' => 'Y',
        'Φ' => 'F',
        'Χ' => 'X',
        'Ψ' => 'PS',
        'Ω' => 'W',
        'Ά' => 'A',
        'Έ' => 'E',
        'Ί' => 'I',
        'Ό' => 'O',
        'Ύ' => 'Y',
        'Ή' => 'H',
        'Ώ' => 'W',
        'Ϊ' => 'I',
        'Ϋ' => 'Y',
        'α' => 'a',
        'β' => 'b',
        'γ' => 'g',
        'δ' => 'd',
        'ε' => 'e',
        'ζ' => 'z',
        'η' => 'h',
        'θ' => '8',
        'ι' => 'i',
        'κ' => 'k',
        'λ' => 'l',
        'μ' => 'm',
        'ν' => 'n',
        'ξ' => '3',
        'ο' => 'o',
        'π' => 'p',
        'ρ' => 'r',
        'σ' => 's',
        'τ' => 't',
        'υ' => 'y',
        'φ' => 'f',
        'χ' => 'x',
        'ψ' => 'ps',
        'ω' => 'w',
        'ά' => 'a',
        'έ' => 'e',
        'ί' => 'i',
        'ό' => 'o',
        'ύ' => 'y',
        'ή' => 'h',
        'ώ' => 'w',
        'ς' => 's',
        'ϊ' => 'i',
        'ΰ' => 'y',
        'ϋ' => 'y',
        'ΐ' => 'i',
        // Turkish
        'Ş' => 'S',
        'İ' => 'I',
        'Ç' => 'C',
        'Ü' => 'U',
        'Ö' => 'O',
        'Ğ' => 'G',
        'ş' => 's',
        'ı' => 'i',
        'ç' => 'c',
        'ü' => 'u',
        'ö' => 'o',
        'ğ' => 'g',
        // Russian
        'А' => 'A',
        'Б' => 'B',
        'В' => 'V',
        'Г' => 'G',
        'Д' => 'D',
        'Е' => 'E',
        'Ё' => 'Yo',
        'Ж' => 'Zh',
        'З' => 'Z',
        'И' => 'I',
        'Й' => 'J',
        'К' => 'K',
        'Л' => 'L',
        'М' => 'M',
        'Н' => 'N',
        'О' => 'O',
        'П' => 'P',
        'Р' => 'R',
        'С' => 'S',
        'Т' => 'T',
        'У' => 'U',
        'Ф' => 'F',
        'Х' => 'H',
        'Ц' => 'C',
        'Ч' => 'Ch',
        'Ш' => 'Sh',
        'Щ' => 'Sh',
        'Ъ' => '',
        'Ы' => 'Y',
        'Ь' => '',
        'Э' => 'E',
        'Ю' => 'Yu',
        'Я' => 'Ya',
        'а' => 'a',
        'б' => 'b',
        'в' => 'v',
        'г' => 'g',
        'д' => 'd',
        'е' => 'e',
        'ё' => 'yo',
        'ж' => 'zh',
        'з' => 'z',
        'и' => 'i',
        'й' => 'j',
        'к' => 'k',
        'л' => 'l',
        'м' => 'm',
        'н' => 'n',
        'о' => 'o',
        'п' => 'p',
        'р' => 'r',
        'с' => 's',
        'т' => 't',
        'у' => 'u',
        'ф' => 'f',
        'х' => 'h',
        'ц' => 'c',
        'ч' => 'ch',
        'ш' => 'sh',
        'щ' => 'sh',
        'ъ' => '',
        'ы' => 'y',
        'ь' => '',
        'э' => 'e',
        'ю' => 'yu',
        'я' => 'ya',
        // Ukrainian
        'Є' => 'Ye',
        'І' => 'I',
        'Ї' => 'Yi',
        'Ґ' => 'G',
        'є' => 'ye',
        'і' => 'i',
        'ї' => 'yi',
        'ґ' => 'g',
        // Czech
        'Č' => 'C',
        'Ď' => 'D',
        'Ě' => 'E',
        'Ň' => 'N',
        'Ř' => 'R',
        'Š' => 'S',
        'Ť' => 'T',
        'Ů' => 'U',
        'Ž' => 'Z',
        'č' => 'c',
        'ď' => 'd',
        'ě' => 'e',
        'ň' => 'n',
        'ř' => 'r',
        'š' => 's',
        'ť' => 't',
        'ů' => 'u',
        'ž' => 'z',
        // Polish
        'Ą' => 'A',
        'Ć' => 'C',
        'Ę' => 'e',
        'Ł' => 'L',
        'Ń' => 'N',
        'Ó' => 'o',
        'Ś' => 'S',
        'Ź' => 'Z',
        'Ż' => 'Z',
        'ą' => 'a',
        'ć' => 'c',
        'ę' => 'e',
        'ł' => 'l',
        'ń' => 'n',
        'ó' => 'o',
        'ś' => 's',
        'ź' => 'z',
        'ż' => 'z',
        // Latvian
        'Ā' => 'A',
        'Č' => 'C',
        'Ē' => 'E',
        'Ģ' => 'G',
        'Ī' => 'i',
        'Ķ' => 'k',
        'Ļ' => 'L',
        'Ņ' => 'N',
        'Š' => 'S',
        'Ū' => 'u',
        'Ž' => 'Z',
        'ā' => 'a',
        'č' => 'c',
        'ē' => 'e',
        'ģ' => 'g',
        'ī' => 'i',
        'ķ' => 'k',
        'ļ' => 'l',
        'ņ' => 'n',
        'š' => 's',
        'ū' => 'u',
        'ž' => 'z'
    );
    // Make custom replacements
    $str      = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);
    // Transliterate characters to ASCII
    if ($options['transliterate']) {
        $str = str_replace(array_keys($char_map), $char_map, $str);
    }
    // Replace non-alphanumeric characters with our delimiter
    $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);
    // Remove duplicate delimiters
    $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);
    // Truncate slug to max. characters
    $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');
    // Remove delimiter from ends
    $str = trim($str, $options['delimiter']);
    return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
}

function Sh_GenerateKey($minlength = 20, $maxlength = 20, $uselower = true, $useupper = true, $usenumbers = true, $usespecial = false) {
    $charset = '';
    if ($uselower) {
        $charset .= "abcdefghijklmnopqrstuvwxyz";
    }
    if ($useupper) {
        $charset .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    }
    if ($usenumbers) {
        $charset .= "123456789";
    }
    if ($usespecial) {
        $charset .= "~@#$%^*()_+-={}|][";
    }
    if ($minlength > $maxlength) {
        $length = mt_rand($maxlength, $minlength);
    } else {
        $length = mt_rand($minlength, $maxlength);
    }
    $key = '';
    for ($i = 0; $i < $length; $i++) {
        $key .= $charset[(mt_rand(0, strlen($charset) - 1))];
    }
    return $key;
}



function Remove_from_String($state){

  $state = str_replace("_"," ",$state);
  $state = ucwords($state);

  return $state;
}

function add_to_String($state){

  $state = str_replace(" ","_",$state);
  $state = strtolower($state);

  return $state;
}

function fetchDataFromURL($url = '') {
    if (empty($url)) {
        return false;
    }
    $ch = curl_init($url);
    curl_setopt( $ch, CURLOPT_POST, false );
    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
    curl_setopt( $ch, CURLOPT_HEADER, false );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt( $ch, CURLOPT_TIMEOUT, 5);
    return curl_exec( $ch );
}

function Sh_SendMessage($data = array()){
    global $sh, $sqlConnect;
    include_once('assets/libraries/PHPMailer-Master/vendor/autoload.php');
    $mail = new PHPMailer\PHPMailer\PHPMailer;
    $email_from      = $data['from_email'] = Sh_Secure($data['from_email']);
    $to_email        = $data['to_email'] = Sh_Secure($data['to_email']);
    $subject         = $data['subject'];
    $message_body    = mysqli_real_escape_string($sqlConnect, $data['message_body']);
    $data['charSet'] = Sh_Secure($data['charSet']);
    if (isset($data['insert_database'])) {
        if ($data['insert_database'] == 1) {
            $user_id   = Sh_Secure($sh['user']['user_id']);
            $query_one = mysqli_query($sqlConnect, "INSERT INTO " . T_EMAILS . " (`email_to`, `user_id`, `subject`, `message`) VALUES ('{$to_email}', '{$user_id}', '{$subject}', '{$message_body}')");
            if ($query_one) {
                return true;
            }
        }
        return true;
        exit();
    }
    if ($sh['config']['smtp_or_mail'] == 'mail') {
        $mail->IsMail();
    } else if ($sh['config']['smtp_or_mail'] == 'smtp') {
        $mail->isSMTP();
        $mail->Host        = $sh['config']['smtp_host']; // Specify main and backup SMTP servers
        $mail->SMTPAuth    = true; // Enable SMTP authentication
        $mail->Username    = $sh['config']['smtp_username']; // SMTP username
        $mail->Password    = $sh['config']['smtp_password']; // SMTP password
        $mail->SMTPSecure  = $sh['config']['smtp_encryption']; // Enable TLS encryption, `ssl` also accepted
        $mail->Port        = $sh['config']['smtp_port'];
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
    } else {
        return false;
    }
    $mail->IsHTML($data['is_html']);
    $mail->setFrom($data['from_email'], $data['from_name']);
    $mail->addAddress($data['to_email'], $data['to_name']); // Add a recipient
    $mail->Subject = $data['subject'];
    $mail->CharSet = $data['charSet'];
    $mail->MsgHTML($data['message_body']);
    if (!empty($data['reply-to'])) {
        $mail->ClearReplyTos();
        $mail->AddReplyTo($data['reply-to'], $data['from_name']);
    }
    if ($mail->send()) {
        $mail->ClearAddresses();
        return true;
    }
}



function Sh_RedirectSmooth($url) {
    global $sh;
    if ($sh['config']['smooth_loading'] == 0) {
        return header("Location: $url");
        exit();
    } else {
        return $sh['redirect'] = 1;
    }
}


function Sh_RegisterNotification($data = array()) {
    global $sh, $sqlConnect;

    if (empty($data['session_id'])) {
        if ($sh['loggedin'] == false AND $data["from_api"]!=1) {
         return false;
        }
    }
    if (!isset($data['recipient_id']) or empty($data['recipient_id']) or !is_numeric($data['recipient_id']) or $data['recipient_id'] < 1) {
        return false;
    }
    if (Sh_IsBlocked($data['recipient_id'])) {
        return false;
    }
    if (!isset($data['post_id']) or empty($data['post_id'])) {
        $data['post_id'] = 0;
    }
    if (!is_numeric($data['post_id']) or $data['recipient_id'] < 0) {
        return false;
    }
    if (empty($data['notifier_id']) or $data['notifier_id'] == 0) {
        $data['notifier_id'] = Sh_Secure($sh['user']['user_id']);
    }
    if (!is_numeric($data['notifier_id']) or $data['notifier_id'] < 1) {
        return false;
    }
    if ($data['notifier_id'] == $sh['user']['user_id']) {
        $notifier = $sh['user'];
    }
    else {
        $data['notifier_id'] = Sh_Secure($data['notifier_id']);
        $notifier            = Sh_UserData($data['notifier_id']);
        if (!isset($notifier['user_id'])) {
            return false;
        }
    }

    if (!isset($data['comment_id']) or empty($data['comment_id'])) {
        $data['comment_id'] = 0;
    }else{
        $data['comment_id']      = Sh_Secure($data['comment_id']);
    }

    if (!isset($data['reply_id']) or empty($data['reply_id'])) {
        $data['reply_id'] = 0;
    }else{
        $data['reply_id']      = Sh_Secure($data['reply_id']);
    }

    if ($notifier['user_id'] != $sh['user']['user_id'] AND $data["from_api"]!=1) {
        return false;
    }
    if ($data['recipient_id'] == $data['notifier_id']) {
        return false;
    }
    if (!isset($data['text'])) {
        $data['text'] = '';
    }
    if (!isset($data['type']) or empty($data['type'])) {
        return false;
    }
    if (!isset($data['url']) and empty($data['url']) and !isset($data['full_link']) and empty($data['full_link'])) {
        return false;
    }
    $recipient = Sh_UserData($data['recipient_id']);
    if (!isset($recipient['user_id'])) {
        return false;
    }

    $url                  = $data['url'];
    $recipient['user_id'] = Sh_Secure($recipient['user_id']);
    $data['post_id']      = Sh_Secure($data['post_id']);
    $data['type']         = Sh_Secure($data['type']);
    if (!empty($data['type2'])) {
        $data['type2'] = Sh_Secure($data['type2']);
    } else {
        $data['type2'] = '';
    }
    if ($data['text'] != strip_tags($data['text'])) {
        $data['text'] = '';
    }
    $data['text']            = Sh_Secure($data['text']);
    $notifier['user_id']     = Sh_Secure($notifier['user_id']);
    $page_notifcation_query  = '';
    $page_notifcation_query2 = '';

    $send_notification = true;





    if (!empty($recipient['notification_settings'])) {
        $recipient['notification_settings'] = unserialize(html_entity_decode($recipient['notification_settings']));
			/*	dvm mods till $notification_settings vars */
			$data2 = Sh_UserData($recipient['user_id']);
			$recipient['notification_settings'] = (array)json_decode($data2["notification_settings"]);
    } else {
        $recipient['notification_settings'] = array();
    }
    if (($data['type'] == 'liked_post' || $data['type'] == 'reaction') && $recipient['notification_settings']['e_liked'] != 1) {
        $send_notification = false;
    }
    if ($data['type'] == 'share_post' && $recipient['notification_settings']['e_shared'] != 1) {
        $send_notification = false;
    }
    if ($data['type'] == 'comment' && $recipient['notification_settings']['e_commented'] != 1) {
        $send_notification = false;
    }
    if ($data['type'] == 'following' && $recipient['notification_settings']['e_accepted'] != 1) {
        $send_notification = false;
    }
    if ($data['type'] == 'wondered_post' && $recipient['notification_settings']['e_wondered']!= 1) {
        $send_notification = false;
    }
    if (($data['type'] == 'comment_mention' || $data['type'] == 'post_mention') && $recipient['notification_settings']['e_mentioned'] != 1) {
        $send_notification = false;
    }
    if ($data['type'] == 'accepted_request') {/*dvm mods*/
        $send_notification = true;
		$recipient['notification_settings']['e_accepted']=1;
    }
    if ($data['type'] == 'visited_profile' && $recipient['notification_settings']['e_visited'] != 1) {
        $send_notification = false;
    }
    if ($data['type'] == 'joined_group' && $recipient['notification_settings']['e_joined_group'] != 1) {
        $send_notification = false;
    }
    if ($data['type'] == 'liked_page' && $recipient['notification_settings']['e_liked_page'] =! 1) {
        $send_notification = false;
    }
    if ($data['type'] == 'profile_wall_post' && $recipient['notification_settings']['e_profile_wall_post']!= 1) {
        $send_notification = false;
    }


    if ($send_notification == false) {
        return false;
    }



    if (!empty($data['page_id']) && $data['page_id'] > 0) {
        $page = Sh_PageData($data['page_id']);
        if (!isset($page['page_id'])) {
            return false;
        }
        $page_id = Sh_Secure($page['page_id']);
        if (isset($data['page_enable'])) {
            if ($data['page_enable'] !== false) {
                $notifier['user_id'] = 0;
            }
        } else {
            $notifier['user_id'] = 0;
        }
        $page_notifcation_query  = '`page_id`,';
        $page_notifcation_query2 = "{$page_id}, ";
    }


    $group_notifcation_query  = '';
    $group_notifcation_query2 = '';
    if (!empty($data['group_id']) && $data['group_id'] > 0) {
        $group = Sh_GroupData($data['group_id']);
        if (!isset($group['id'])) {
        }
        $group_id                 = Sh_Secure($group['id']);
        $group_notifcation_query  = '`group_id`,';
        $group_notifcation_query2 = "{$group_id}, ";
    }


    $event_notifcation_query  = '';
    $event_notifcation_query2 = '';
    if (!empty($data['event_id']) && $data['event_id'] > 0) {
        $event                    = Sh_EventData($data['event_id']);
        $event_id                 = Sh_Secure($event['id']);
        $event_notifcation_query  = '`event_id`,';
        $event_notifcation_query2 = "{$event_id}, ";
    }


    $thread_notifcation_query  = '';
    $thread_notifcation_query2 = '';
    if (!empty($data['thread_id']) && $data['thread_id'] > 0) {
        $thread_id                 = Sh_Secure($data['thread_id']);
        $thread_notifcation_query  = '`thread_id`,';
        $thread_notifcation_query2 = "{$thread_id}, ";
    }

    $story_notifcation_query  = '';
    $story_notifcation_query2 = '';
    if (!empty($data['story_id']) && $data['story_id'] > 0) {
        $story_id                 = Sh_Secure($data['story_id']);
        $story_notifcation_query  = '`story_id`,';
        $story_notifcation_query2 = "{$story_id}, ";
    }

    $blog_notifcation_query  = '';
    $blog_notifcation_query2 = '';
    if (!empty($data['blog_id']) && $data['blog_id'] > 0) {
        $blog_id                 = Sh_Secure($data['blog_id']);
        $blog_notifcation_query  = '`blog_id`,';
        $blog_notifcation_query2 = "{$blog_id}, ";
    }

	$group_chat_notifcation_query  = '';
    $group_chat_notifcation_query2 = '';
    if (!empty($data['group_chat_id']) && $data['group_chat_id'] > 0) {
        $group_chat_id                 = Sh_Secure($data['group_chat_id']);
        $group_chat_notifcation_query  = ',`group_chat_id`';
        $group_chat_notifcation_query2 = ",{$group_chat_id} ";
    }

    $query_one     = " SELECT `id` FROM " . T_NOTIFICATION . " WHERE `recipient_id` = " . $recipient['user_id'] . " AND `post_id` = " . $data['post_id'] . " AND `type` = '" . $data['type'] . "'";
    $sql_query_one = mysqli_query($sqlConnect, $query_one);
    if (mysqli_num_rows($sql_query_one) > 0) {
        if ($data['type'] != "following" ) {
            if ( $data['type'] != "reaction" ) {
                $query_two     = " DELETE FROM " . T_NOTIFICATION . " WHERE `recipient_id` = " . $recipient['user_id'] . " AND `post_id` = " . $data['post_id'] . " AND `type` = '" . $data['type'] . "'";
                $sql_query_two = mysqli_query($sqlConnect, $query_two);
            }
        }

    }

    if (!isset($data['undo']) or $data['undo'] != true) {
        $query_three     = "INSERT INTO " . T_NOTIFICATION . " (`recipient_id`, `notifier_id`, {$page_notifcation_query} {$group_notifcation_query} {$story_notifcation_query} {$blog_notifcation_query} {$event_notifcation_query} {$thread_notifcation_query} `post_id`, `comment_id`, `reply_id`, `type`, `type2`, `text`, `url`, `time`) VALUES (" . $recipient['user_id'] . "," . $notifier['user_id'] . ",{$page_notifcation_query2} {$group_notifcation_query2} {$story_notifcation_query2} {$blog_notifcation_query2} {$event_notifcation_query2} {$thread_notifcation_query2} " . $data['post_id'] . ",'" . $data['comment_id'] . "','" . $data['reply_id'] . "','" . $data['type'] . "','" . $data['type2'] . "','" . $data['text'] . "','{$url}'," . time() . ")";
        $sql_query_three = mysqli_query($sqlConnect, $query_three);

        $post_data = array();
        $admin_ids = array();

        if (!empty($data['post_id'])) {
            $post_data = Sh_PostData($data['post_id']);
        }

        $my_id = $sh['user']['user_id'];
        if (!empty($post_data['page_id'])) {
            $admin_post_id = $post_data['id'];
            $admins = Sh_GetPageAdmins($post_data['page_id'], 'user_id');
            if (!empty($admins)) {
                foreach ($admins as $admin) {
                    if ($admin['user_id'] != $sh['user']['user_id']) {
                        $admin_id = $admin['user_id'];
                        $admin_ids[] = "('$admin_id', '$my_id', '$admin_post_id','" . $data['comment_id'] . "','" . $data['reply_id'] . "','" . $data['type'] . "','" . $data['type2'] . "','" . $data['text'] . "','{$url}'," . time() . ")";
                    }
                }
            }
        }
        if (!empty($admin_ids)) {
            $implode_query = implode(',', $admin_ids);
            $query_admins = "INSERT INTO " . T_NOTIFICATION . " (`recipient_id`, `notifier_id`, `post_id`, `comment_id`, `reply_id`, `type`, `type2`, `text`, `url`, `time`) VALUES ";
            $sql_query_three = mysqli_query($sqlConnect, $query_admins . $implode_query);
        }

        if ($sql_query_three) {
            if ($sh['config']['emailNotification'] == 1 && $recipient['emailNotification'] == 1) {
                $send_mail = false;
                if ($data['type'] == 'liked_post' && $recipient['e_liked'] == 1) {
                    $send_mail = true;
                }
                if ($data['type'] == 'share_post' && $recipient['e_shared'] == 1) {
                    $send_mail = true;
                }
                if ($data['type'] == 'comment' && $recipient['e_commented'] == 1) {
                    $send_mail = true;
                }
                if ($data['type'] == 'following' && $recipient['e_followed'] == 1) {
                    $send_mail = true;
                }
                if ($data['type'] == 'wondered_post' && $recipient['e_wondered'] == 1) {
                    $send_mail = true;
                }
                if (($data['type'] == 'comment_mention' || $data['type'] == 'post_mention') && $recipient['e_mentioned'] == 1) {
                    $send_mail = true;
                }
                if ($data['type'] == 'accepted_request' && $recipient['e_accepted'] == 1) {
                    $send_mail = true;
                }
                if ($data['type'] == 'visited_profile' && $recipient['e_visited'] == 1) {
                    $send_mail = true;
                }
                if ($data['type'] == 'joined_group' && $recipient['e_joined_group'] == 1) {
                    $send_mail = true;
                }
                if ($data['type'] == 'liked_page' && $recipient['e_liked_page'] == 1) {
                    $send_mail = true;
                }
                if ($data['type'] == 'profile_wall_post' && $recipient['e_profile_wall_post'] == 1) {
                    $send_mail = true;
                }

                if ($data['type'] == 'e_makeoffer') { /*dvm mods up e_sentme_msg*/
                    $send_mail = true;
                }

                if ($data['type'] == 'e_schedulevisit') {
                    $send_mail = true;
                }

                if ($data['type'] == 'e_sentme_msg') {
                    $send_mail = true;
                }


				/*$f2 = fopen("not_sent_mail_line2569.txt","w");
				 fwrite($f2,$data['type']."-".$recipient['email']);
				 fclose($f2);*/

                if ($send_mail == true) {


                    $post_data_id      = $post_data;
                    $post_data['text'] = '';
                    if (!empty($post_data_id['postText'])) {
                        $post_data['text'] = substr($post_data_id['postText'], 0, 20);
                    }
                    $data['notifier']        = $notifier;
                    $data['url']             = Sh_SeoLink($url);
                    $data['post_data']       = $post_data;
                    $sh['emailNotification'] = $data;
                    $send_message_data       = array(
                        'from_email' => 'no_reply@strastic.com',
                        'from_name' => $sh['config']['siteName'],
                        'to_email' => $recipient['email'],
                        'to_name' => $recipient['name'],
                        'subject' => 'New notification',
                        'charSet' => 'utf-8',
                        'message_body' => Sh_LoadPage('emails/notifiction-email'),
                        'is_html' => true
                    );
                    if ($sh['config']['smtp_or_mail'] == 'smtp') {
                        $send_message_data['insert_database'] = 1;
                    }
                    $send = Sh_SendMessage($send_message_data);

                }
            }
            if ($sh['config']['push_notifications'] == 1) {
                Sh_NotificationWebPushNotifier();
            }
            return true;
        }
    }
}




function dataTimeFetch($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function Sh_CompressImage($source_url, $destination_url, $quality) {
    $imgsize = getimagesize($source_url);
    $finfof  = $imgsize['mime'];
    $image_c = 'imagejpeg';
    if ($finfof == 'image/jpeg') {
        $image = @imagecreatefromjpeg($source_url);
    } else if ($finfof == 'image/gif') {
        $image = @imagecreatefromgif($source_url);
    } else if ($finfof == 'image/png') {
        $image = @imagecreatefrompng($source_url);
    } else {
        $image = @imagecreatefromjpeg($source_url);
    }
    $quality = 50;
    if (function_exists('exif_read_data')) {
        $exif = @exif_read_data($source_url);
        if (!empty($exif['Orientation'])) {
            switch ($exif['Orientation']) {
                case 3:
                    $image = @imagerotate($image, 180, 0);
                    break;
                case 6:
                    $image = @imagerotate($image, -90, 0);
                    break;
                case 8:
                    $image = @imagerotate($image, 90, 0);
                    break;
            }
        }
    }
    @imagejpeg($image, $destination_url, $quality);
    return $destination_url;
}


function Sh_Resize_Crop_Image($max_width, $max_height, $source_file, $dst_dir, $quality = 80) {
    $imgsize = @getimagesize($source_file);
    $width   = $imgsize[0];
    $height  = $imgsize[1];
    $mime    = $imgsize['mime'];
    $image   = "imagejpeg";
    switch ($mime) {
        case 'image/gif':
            $image_create = "imagecreatefromgif";
            break;
        case 'image/png':
            $image_create = "imagecreatefrompng";
            break;
        case 'image/jpeg':
            $image_create = "imagecreatefromjpeg";
            break;
        default:
            return false;
            break;
    }
    $dst_img = @imagecreatetruecolor($max_width, $max_height);
    $src_img = @$image_create($source_file);
    if (function_exists('exif_read_data')) {
        $exif          = @exif_read_data($source_file);
        $another_image = false;
        if (!empty($exif['Orientation'])) {
            switch ($exif['Orientation']) {
                case 3:
                    $src_img = @imagerotate($src_img, 180, 0);
                    @imagejpeg($src_img, $dst_dir, $quality);
                    $another_image = true;
                    break;
                case 6:
                    $src_img = @imagerotate($src_img, -90, 0);
                    @imagejpeg($src_img, $dst_dir, $quality);
                    $another_image = true;
                    break;
                case 8:
                    $src_img = @imagerotate($src_img, 90, 0);
                    @imagejpeg($src_img, $dst_dir, $quality);
                    $another_image = true;
                    break;
            }
        }
        if ($another_image == true) {
            $imgsize = @getimagesize($dst_dir);
            if ($width > 0 && $height > 0) {
                $width  = $imgsize[0];
                $height = $imgsize[1];
            }
        }
    }
    @$width_new = $height * $max_width / $max_height;
    @$height_new = $width * $max_height / $max_width;
    if ($width_new > $width) {
        $h_point = (($height - $height_new) / 2);
        @imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
    } else {
        $w_point = (($width - $width_new) / 2);
        @imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
    }
    @imagejpeg($dst_img, $dst_dir, $quality);
    if ($dst_img)
        @imagedestroy($dst_img);
    if ($src_img)
        @imagedestroy($src_img);
    return true;
}


function Sh_UploadToS3($filename, $config = array()) {
    global $sh;

    if ($sh['config']['amazone_s3'] == 0 && $sh['config']['ftp_upload'] == 0 && $sh['config']['spaces'] == 0 && $sh['config']['cloud_upload'] == 0) {
        return false;
    }

    if ($sh['config']['ftp_upload'] == 1) {
        include_once('assets/libraries/ftp/vendor/autoload.php');
        $ftp = new \FtpClient\FtpClient();
        $ftp->connect($sh['config']['ftp_host'], false, $sh['config']['ftp_port']);
        $login = $ftp->login($sh['config']['ftp_username'], $sh['config']['ftp_password']);

        if ($login) {
            if (!empty($sh['config']['ftp_path'])) {
                if ($sh['config']['ftp_path'] != "./") {
                    $ftp->chdir($sh['config']['ftp_path']);
                }
            }
            $file_path = substr($filename, 0, strrpos( $filename, '/'));
            $file_path_info = explode('/', $file_path);
            $path = '';
            if (!$ftp->isDir($file_path)) {
                foreach ($file_path_info as $key => $value) {
                    if (!empty($path)) {
                        $path .= '/' . $value . '/' ;
                    } else {
                        $path .= $value . '/' ;
                    }
                    if (!$ftp->isDir($path)) {
                        $mkdir = $ftp->mkdir($path);
                    }
                }
            }
            $ftp->chdir($file_path);
            $ftp->pasv(true);
            if ($ftp->putFromPath($filename)) {
                if (empty($config['delete'])) {
                    if (empty($config['amazon'])) {
                        @unlink($filename);
                    }
                }
                $ftp->close();
                return true;
            }
            $ftp->close();
        }
    } else if ($sh['config']['amazone_s3'] == 1){
        if (empty($sh['config']['amazone_s3_key']) || empty($sh['config']['amazone_s3_s_key']) || empty($sh['config']['region']) || empty($sh['config']['bucket_name'])) {
            return false;
        }
        include_once('assets/libraries/s3/aws-autoloader.php');
        $s3 = new S3Client([
            'version'     => 'latest',
            'region'      => $sh['config']['region'],
            'credentials' => [
                'key'    => $sh['config']['amazone_s3_key'],
                'secret' => $sh['config']['amazone_s3_s_key'],
            ]
        ]);
        $s3->putObject([
            'Bucket' => $sh['config']['bucket_name'],
            'Key'    => $filename,
            'Body'   => fopen($filename, 'r+'),
            'ACL'    => 'public-read',
            'CacheControl' => 'max-age=3153600',
        ]);
        if (empty($config['delete'])) {
            if ($s3->doesObjectExist($sh['config']['bucket_name'], $filename)) {
                if (empty($config['amazon'])) {
                    @unlink($filename);
                }
                return true;
            }
        } else {
            return true;
        }
    } else if ($sh['config']['spaces'] == 1) {
        include_once("assets/libraries/spaces/spaces.php");
        $key = $sh['config']['spaces_key'];
        $secret = $sh['config']['spaces_secret'];
        $space_name = $sh['config']['space_name'];
        $region = $sh['config']['space_region'];
        $space = new SpacesConnect($key, $secret, $space_name, $region);
        $upload = $space->UploadFile($filename, "public");
        if ($upload) {
            if (empty($config['delete'])) {
                if ($space->DoesObjectExist($filename)) {
                    if (empty($config['amazon'])) {
                        @unlink($filename);
                    }
                    return true;
                }
            } else {
                return true;
            }
            return true;
        }
    }
    elseif ($sh['config']['cloud_upload'] == 1) {
        require_once 'assets/libraries/cloud/vendor/autoload.php';

        try {
            $storage = new StorageClient([
               'keyFilePath' => $sh['config']['cloud_file_path']
            ]);
            // set which bucket to work in
            $bucket = $storage->bucket($sh['config']['cloud_bucket_name']);
            $fileContent = file_get_contents($filename);

            // upload/replace file
            $storageObject = $bucket->upload(
                                    $fileContent,
                                    ['name' => $filename]
                            );
            if (!empty($storageObject)) {
                if (empty($config['delete'])) {
                    if (empty($config['amazon'])) {
                        @unlink($filename);
                    }
                }
                return true;
            }
        } catch (Exception $e) {
            // maybe invalid private key ?
            // print $e;
            // exit();
            return false;
        }
    }
    return false;
}


function Sh_ShareFile($data = array(), $type = 0, $crop = true) {
    global $sh, $sqlConnect, $s3;

    $allowed = '';

    if (!file_exists('upload/files/' . date('Y'))) {
        @mkdir('upload/files/' . date('Y'), 0777, true);
    }
    if (!file_exists('upload/files/' . date('Y') . '/' . date('m'))) {
        @mkdir('upload/files/' . date('Y') . '/' . date('m'), 0777, true);
    }
    if (!file_exists('upload/photos/' . date('Y'))) {
        @mkdir('upload/photos/' . date('Y'), 0777, true);
    }
    if (!file_exists('upload/photos/' . date('Y') . '/' . date('m'))) {
        @mkdir('upload/photos/' . date('Y') . '/' . date('m'), 0777, true);
    }
    if (!file_exists('upload/videos/' . date('Y'))) {
        @mkdir('upload/videos/' . date('Y'), 0777, true);
    }
    if (!file_exists('upload/videos/' . date('Y') . '/' . date('m'))) {
        @mkdir('upload/videos/' . date('Y') . '/' . date('m'), 0777, true);
    }
    if (!file_exists('upload/sounds/' . date('Y'))) {
        @mkdir('upload/sounds/' . date('Y'), 0777, true);
    }
    if (!file_exists('upload/sounds/' . date('Y') . '/' . date('m'))) {
        @mkdir('upload/sounds/' . date('Y') . '/' . date('m'), 0777, true);
    }
    if (isset($data['file']) && !empty($data['file'])) {
        $data['file'] = $data['file'];
    }
    if (isset($data['name']) && !empty($data['name'])) {
        $data['name'] = Sh_Secure($data['name']);
    }
    if (empty($data)) {
        return false;
    }
    if ($sh['config']['fileSharing'] == 1) {

        if (isset($data['types'])) {
            $allowed = $data['types'];
        } else {
            $allowed = $sh['config']['allowedExtenstion'];
        }

    } else {
        $allowed = 'jpg,png,jpeg,gif,mp4,m4v,webm,flv,mov,mpeg,mp3,wav,doc,docx,xls,xlsx,csv,pptx,ppt';
    }

    $new_string        = pathinfo($data['name'], PATHINFO_FILENAME) . '.' . strtolower(pathinfo($data['name'], PATHINFO_EXTENSION));
    $extension_allowed = explode(',', $allowed);
    $file_extension    = pathinfo($new_string, PATHINFO_EXTENSION);
    if (!in_array($file_extension, $extension_allowed)) {
        return false;
    }
    if ($data['size'] > $sh['config']['maxUpload']) {
        return false;
    }
    if ($file_extension == 'jpg' || $file_extension == 'jpeg' || $file_extension == 'png' || $file_extension == 'gif') {
        $folder   = 'photos';
        $fileType = 'image';
    } else if ($file_extension == 'mp4' || $file_extension == 'mov' || $file_extension == 'webm' || $file_extension == 'flv') {
        $folder   = 'videos';
        $fileType = 'video';
    } else if ($file_extension == 'mp3' || $file_extension == 'wav') {
        $folder   = 'sounds';
        $fileType = 'soundFile';
    } else {
        $folder   = 'files';
        $fileType = 'file';
    }
    if (empty($folder) || empty($fileType)) {
        return false;
    }
    $mime_types = explode(',', str_replace(' ', '', $sh['config']['mime_types'] . ',application/json,application/octet-stream'));
    if (Sh_IsAdmin()) {
        $mime_types = explode(',', str_replace(' ', '', $sh['config']['mime_types'] . ',application/json,application/octet-stream,image/svg+xml'));
    }

    if (!in_array($data['type'], $mime_types)) {
        return false;
    }
    $dir         = "upload/{$folder}/" . date('Y') . '/' . date('m');
    $filename    = $dir . '/' . Sh_GenerateKey() . '_' . date('d') . '_' . md5(time()) . "_{$fileType}.{$file_extension}";
    $second_file = pathinfo($filename, PATHINFO_EXTENSION);
    if (move_uploaded_file($data['file'], $filename)) {
        if ($second_file == 'jpg' || $second_file == 'jpeg' || $second_file == 'png' || $second_file == 'gif') {
            $check_file = getimagesize($filename);
            if (!$check_file) {
                unlink($filename);
            }
            if( $crop == true ){
                if ($type == 1) {
                    if ($second_file != 'gif') {
                        @Sh_CompressImage($filename, $filename, 50);
                    }
                    $explode2  = @end(explode('.', $filename));
                    $explode3  = @explode('.', $filename);
                    $last_file = $explode3[0] . '_small.' . $explode2;

                    if (Sh_Resize_Crop_Image(400, 400, $filename, $last_file, 60)) {
                        if (empty($data['local_upload'])) {
                            if (($sh['config']['amazone_s3'] == 1 || $sh['config']['ftp_upload'] == 1 || $sh['config']['spaces'] == 1 || $sh['config']['cloud_upload'] == 1) && !empty($last_file)) {
                                $upload_s3 = Sh_UploadToS3($last_file);
                            }
                        }
                    }
                } else {
                    if (!isset($data['compress']) && $second_file != 'gif') {
                        @Sh_CompressImage($filename, $filename, 10);

                        if ($sh['config']['watermark'] == 1) {
                          watermark_image($filename);
                        }

                    }
                }
            }
        }
        if (!empty($data['crop'])) {
            $crop_image = Sh_Resize_Crop_Image($data['crop']['width'], $data['crop']['height'], $filename, $filename, 60);
        }
        if (empty($data['local_upload'])) {
            if (($sh['config']['amazone_s3'] == 1 || $sh['config']['ftp_upload'] == 1 || $sh['config']['spaces'] == 1 || $sh['config']['cloud_upload'] == 1) && !empty($filename)) {
                $upload_s3 = Sh_UploadToS3($filename);
            }
        }
        $last_data             = array();
        $last_data['filename'] = $filename;
        $last_data['name']     = $data['name'];
        return $last_data;
    }
}

function watermark_image($target) {
    global $sh;
    include_once('assets/libraries/SimpleImage-master/src/claviska/SimpleImage.php');
    if ($sh['config']['watermark'] != 1) {
        return false;
    }
    try {
      $image = new \claviska\SimpleImage();

      $image
        ->fromFile($target)
        ->autoOrient()
        ->overlay("./themes/{$sh['config']['theme']}/img/icon.png", 'top left', 1, 30, 30)
        ->toFile($target, 'image/jpeg');

      return true;
    } catch(Exception $err) {
      return $err->getMessage();
    }
}


function perpage($count, $per_page = '5',$href) {
  $output = '';
  $paging_id = "link_perpage_box";

  if(!isset($_POST["pagination_offset"])) $_POST["pagination_offset"] = 1;
  if($per_page != 0)
      $pages  = ceil($count/$per_page);
      if($pages>1) {

          if(($_POST["pagination_offset"]-3)>0) {
              if($_POST["pagination_offset"] == 1)
                  $output = $output . '<a href="#" id="1" class="active">1</a>';
                  else
                      $output = $output . '<input type="hidden" name="pagination_offset" value="1" /><input type="button" class="perpage-link" value="1" onClick="listViaAJAX()" />';
          }
          if(($_POST["pagination_offset"]-3)>1) {
              $output = $output . '...';
          }

          for($i=($_POST["pagination_offset"]-2); $i<=($_POST["pagination_offset"]+2); $i++)
          {

              if($i<1) continue;
              if($i>$pages) break;
              if($_POST["pagination_offset"] == $i)
                  $output = $output . '<a href="#" id='.$i.' class="active" >'.$i.'</a>';
                  else
                      $output = $output . '<input type="hidden" name="pagination_offset" value="' . $i . '" /><input type="button" class="perpage-link" value="' . $i . '" onClick="listViaAJAX()" />';
          }

          if(($pages-($_POST["pagination_offset"]+2))>1) {

              $output = $output . '...';
          }

          if(($pages-($_POST["pagination_offset"]+2))>0) {

              if($_POST["pagination_offset"] == $pages)
                  $output = $output . '<a href="#" id=' . ($pages) .' class="active">' . ($pages) .'</a>';
                  else
                      $output = $output . '<input type="hidden" name="pagination_offset" value="' . $pages . '" /><input type="button" class="perpage-link" value="' . $pages . '" onClick="listViaAJAX()" />';

          }

      }
      return $output;

}

function showperpage($count, $per_page = 5, $href){
  $perpage = perpage($count, $per_page,$href);

  return $perpage;

}

 ?>
