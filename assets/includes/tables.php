<?php
// +------------------------------------------------------------------------+
// | @author Hassan Tijani.A (SureCoder)
// | @author_email: gatukurh1@gmail.com
// +------------------------------------------------------------------------+
// | Sourceher - Profile Platform
// | Copyright (c) 2022 Sourceher. All rights reserved.
// +------------------------------------------------------------------------+

define('T_BANNED_IPS', 'Sh_Banned_Ip');
define('T_CONFIG', 'Sh_Config');
define('T_NOTIFY_MESSAGES', 'Sh_Notify_Messages');
define('T_LANGS', 'Sh_Langs');
define('T_APP_SESSIONS', 'Sh_AppsSessions');
define('T_USERS','Sh_Users');
define('T_EMAILS','Sh_Emails');
define('T_USERS_TEST','Sh_Users_test');
define('T_FORM_LIST','Sh_Form_list');
define('T_PAGE_TYPE','Sh_Page_Type');
define('T_TESTER','mydataTester');
define('T_PROFESSIONAL_PLUG','Sh_professional_plug');
define('T_BAD_LOGIN', 'Sh_Bad_Login');
define('T_REVIEW_CAT', 'Sh_review_category');
define('T_REVIEWS', 'Sh_Reviews');
define('T_CATEGORIES', 'Sh_Category');
define('T_CONTACTS_MSG', 'Sh_Contact_Msg');
define('T_SUBSCRIBERS', 'Sh_Subscribers');
define('T_PAGES', 'Sh_Pages');
define('T_CONTACTS', 'Sh_Contacts');

?>
