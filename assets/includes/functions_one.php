<?php

require_once('app_starter.php');

// Get all banned ip_address
function Sh_GetBanned($type = '') {
    global $sqlConnect;
    $data  = array();
    $query = mysqli_query($sqlConnect, "SELECT * FROM " . T_BANNED_IPS . " ORDER BY id DESC");
    if ($type == 'user') {
        while ($fetched_data = mysqli_fetch_assoc($query)) {
            if (filter_var($fetched_data['ip_address'], FILTER_VALIDATE_IP)) {
                $data[] = $fetched_data['ip_address'];
            }
        }
    } else {
        while ($fetched_data = mysqli_fetch_assoc($query)) {
            $data[] = $fetched_data;
        }
    }
    return $data;
}
//
function Sh_UserData($user_id, $password = true) {
    global $sh, $sqlConnect, $cache;
    if (empty($user_id) || !is_numeric($user_id) || $user_id < 0) {
        return false;
    }
    $data = array();
    $user_id  = Sh_Secure($user_id);
    $query_one      = "SELECT * FROM " . T_USERS . " WHERE `user_id` = {$user_id}";
    $hashed_user_Id = md5($user_id);
    if ($sh['config']['cacheSystem'] == 1) {
        $fetched_data = $cache->read($hashed_user_Id . '_U_Data.tmp');
        if (empty($fetched_data)) {
            $sql          = mysqli_query($sqlConnect, $query_one);
            $fetched_data = mysqli_fetch_assoc($sql);
            $cache->write($hashed_user_Id . '_U_Data.tmp', $fetched_data);
        }
    } else {
        $sql          = mysqli_query($sqlConnect, $query_one);
        $fetched_data = mysqli_fetch_assoc($sql);
    }
    if (empty($fetched_data)) {
        return array();
    }
    if ($password == false) {
        unset($fetched_data['password']);
    }
    $fetched_data['avatar_org'] = $fetched_data['avatar'];
    $fetched_data['cover_org']  = $fetched_data['cover'];
    $explode2                   = @end(explode('.', $fetched_data['cover']));
    $explode3                   = @explode('.', $fetched_data['cover']);
    $fetched_data['cover_full'] = $sh['userDefaultCover'];
    if ($fetched_data['cover'] != $sh['userDefaultCover']) {
        @$fetched_data['cover_full'] = $explode3[0] . '_full.' . $explode2;
    }
    $explode2                   = @end(explode('.', $fetched_data['avatar']));
    $explode3                   = @explode('.', $fetched_data['avatar']);
    if ($fetched_data['avatar'] != $sh['userDefaultAvatar']) {
        @$fetched_data['avatar_full'] = $explode3[0] . '_full.' . $explode2;
    }
    $fetched_data['avatar'] = Sh_GetMedia($fetched_data['avatar']) . '?cache=' . $fetched_data['last_avatar_mod'];
    $fetched_data['cover']  = Sh_GetMedia($fetched_data['cover']) . '?cache=' . $fetched_data['last_cover_mod'];
    $fetched_data['id']     = $fetched_data['user_id'];
    $fetched_data['user_platform'] = Sh_GetPlatformFromUser_ID($fetched_data['user_id']);
    $fetched_data['type']   = 'user';
    // $fetched_data['url']    = Sh_SeoLink('index.php?link1=timeline&u=' . $fetched_data['username']);
    $fetched_data['name']   = '';
    if (!empty($fetched_data['first_name'])) {
        if (!empty($fetched_data['last_name'])) {
            $fetched_data['name'] = $fetched_data['first_name'] . ' ' . $fetched_data['last_name'];
        } else {
            $fetched_data['name'] = $fetched_data['first_name'];
        }
    } else {
        $fetched_data['name'] = $fetched_data['username'];
    }
    if (!empty($fetched_data['details'])) {
        $fetched_data['details'] = (Array) json_decode($fetched_data['details']);
    }
    $fetched_data['following_data'] = '';
    $fetched_data['followers_data'] = '';
    $fetched_data['mutual_friends_data'] = '';
    $fetched_data['likes_data'] = '';
    $fetched_data['groups_data'] = '';
    $fetched_data['album_data'] = '';
    if (!empty($fetched_data['sidebar_data'])) {
        $sidebar_data = (Array) json_decode($fetched_data['sidebar_data']);
        if (!empty($sidebar_data['following_data'])) {
            $fetched_data['following_data'] = $sidebar_data['following_data'];
        }
        if (!empty($sidebar_data['followers_data'])) {
            $fetched_data['followers_data'] = $sidebar_data['followers_data'];
        }
        if (!empty($sidebar_data['mutual_friends_data'])) {
            $fetched_data['mutual_friends_data'] = $sidebar_data['mutual_friends_data'];
        }
        if (!empty($sidebar_data['likes_data'])) {
            $fetched_data['likes_data'] = $sidebar_data['likes_data'];
        }
        if (!empty($sidebar_data['groups_data'])) {
            $fetched_data['groups_data'] = $sidebar_data['groups_data'];
        }
        if (!empty($sidebar_data['album_data'])) {
            $fetched_data['album_data'] = $sidebar_data['album_data'];
        }
    }


    $fetched_data['website'] = (strpos($fetched_data['website'], 'http') === false && !empty($fetched_data['website'])) ? 'http://' . $fetched_data['website'] : $fetched_data['website'];
    $fetched_data['working_link'] = (strpos($fetched_data['working_link'], 'http') === false && !empty($fetched_data['working_link'])) ? 'http://' . $fetched_data['working_link'] : $fetched_data['working_link'];
    $fetched_data['lastseen_unix_time'] = $fetched_data['lastseen'];
    $fetched_data['lastseen_status'] = ($fetched_data['lastseen'] > (time() - 60)) ? 'on' : 'off';

    return $fetched_data;
}


function Sh_UserActive($username) {
    global $sqlConnect;
    if (empty($username)) {
        return false;
    }
    $username = Sh_Secure($username);
    $query    = mysqli_query($sqlConnect, "SELECT COUNT(`user_id`) FROM " . T_USERS . "  WHERE (`username` = '{$username}' OR `email` = '{$username}' OR `phone_number` = '{$username}') AND `active` = '1'");
    return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}
function Sh_UserInactive($username) {
    global $sqlConnect;
    if (empty($username)) {
        return false;
    }
    $username = Sh_Secure($username);
    $query    = mysqli_query($sqlConnect, "SELECT COUNT(`user_id`) FROM " . T_USERS . "  WHERE (`username` = '{$username}' OR `email` = '{$username}' OR `phone_number` = '{$username}') AND `active` = '2'");
    return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}
function Sh_UserExists($username) {
    global $sqlConnect;
    if (empty($username)) {
        return false;
    }
    $username = Sh_Secure($username);
    $query    = mysqli_query($sqlConnect, "SELECT COUNT(`user_id`) FROM " . T_USERS . " WHERE `username` = '{$username}'");
    return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}

function Sh_GetMedia($media) {
    global $sh;
    if (empty($media)) {
        return '';
    }
    if ($sh['config']['amazone_s3'] == 1) {
        if (empty($sh['config']['amazone_s3_key']) || empty($sh['config']['amazone_s3_s_key']) || empty($sh['config']['region']) || empty($sh['config']['bucket_name'])) {
            return $sh['config']['site_url'] . '/' . $media;
        }
        return $sh['config']['s3_site_url'] . '/' . $media;
    } else if ($sh['config']['spaces'] == 1) {
        if (empty($sh['config']['spaces_key']) || empty($sh['config']['spaces_secret']) || empty($sh['config']['space_region']) || empty($sh['config']['space_name'])) {
            return $sh['config']['site_url'] . '/' . $media;
        }
        return  'https://' . $sh['config']['space_name'] . '.' . $sh['config']['space_region'] . '.digitaloceanspaces.com/' . $media;
    } else if ($sh['config']['ftp_upload'] == 1) {
        return addhttp($sh['config']['ftp_endpoint']) . '/' . $media;

    } else if ($sh['config']['cloud_upload'] == 1) {
        return 'https://storage.cloud.google.com/'. $sh['config']['cloud_bucket_name'] . '/' . $media;
    }
    return $sh['config']['site_url'] . '/' . $media;
}

function Sh_GetPlatformFromUser_ID($user_id = 0) {
    global $sqlConnect;
    if (empty($user_id)) {
        return false;
    }
    $user_id = Sh_Secure($user_id);
    $query   = mysqli_query($sqlConnect, "SELECT `platform` FROM " . T_APP_SESSIONS . " WHERE `user_id` = '{$user_id}' ORDER BY `time` DESC LIMIT 1");
    $mysqli  = mysqli_fetch_assoc($query);
    return $mysqli['platform'];
}

function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}

function Sh_CreateLoginSession($user_id = 0) {
    global $sqlConnect, $db;
    if (empty($user_id)) {
        return false;
    }
    $user_id   = Sh_Secure($user_id);
    $hash      = sha1(rand(111111111, 999999999)) . md5(microtime()) . rand(11111111, 99999999) . md5(rand(5555, 9999));
    $query_two = mysqli_query($sqlConnect, "DELETE FROM " . T_APP_SESSIONS . " WHERE `session_id` = '{$hash}'");
    if ($query_two) {
        $ua = json_encode(getBrowser());
        $delete_same_session = $db->where('user_id', $user_id)->where('platform_details', $ua)->delete(T_APP_SESSIONS);
        $query_three = mysqli_query($sqlConnect, "INSERT INTO " . T_APP_SESSIONS . " (`user_id`, `session_id`, `platform`, `platform_details`, `time`) VALUES('{$user_id}', '{$hash}', 'web', '$ua'," . time() . ")");
        if ($query_three) {
            return $hash;
        }
    }
}

function Sh_CheckUserSessionID($user_id = 0, $session_id = '', $platform = 'web') {
    global $sh, $sqlConnect;
    if (empty($user_id) || !is_numeric($user_id) || $user_id < 0) {
        return false;
    }
    if (empty($session_id)) {
        return false;
    }
    $platform  = Sh_Secure($platform);
    $query     = mysqli_query($sqlConnect, "SELECT COUNT(`id`) as `session` FROM " . T_APP_SESSIONS . " WHERE `user_id` = '{$user_id}' AND `session_id` = '{$session_id}' AND `platform` = '{$platform}'");
    $query_sql = mysqli_fetch_assoc($query);
    if ($query_sql['session'] > 0) {
        return true;
    }
    return false;
}


function Sh_UserIdFromUsername($username) {
    global $sqlConnect;
    if (empty($username)) {
        return false;
    }
    $username = Sh_Secure($username);
    $query    = mysqli_query($sqlConnect, "SELECT `user_id` FROM " . T_USERS . " WHERE `username` = '{$username}'");
    return Sh_Sql_Result($query, 0, 'user_id');
}


function Sh_IsAdmin($user_id = 0) {
    global $sh, $sqlConnect;
    if ($sh['loggedin'] == false) {
        return false;
    }
    $user_id = Sh_Secure($user_id);
    if (!empty($user_id) && $user_id > 0) {
        $query = mysqli_query($sqlConnect, "SELECT COUNT(`user_id`) as count FROM " . T_USERS . " WHERE admin = '1' AND user_id = {$user_id}");
        $sql   = mysqli_fetch_assoc($query);
        if ($sql['count'] > 0) {
            return true;
        } else {
            return false;
        }
    }
    if ($sh['user']['admin'] == 1) {
        return true;
    }
    return false;
}

function saveUserActions($data){
    global $sh, $sqlConnect;

    if(empty($data) || !isset($data)){
        return false;
    }

    $fields = '`' . implode('`,`', array_keys($data)) . '`';
    $data   = '\'' . implode('\', \'', $data) . '\'';
    $query  = mysqli_query($sqlConnect, "INSERT INTO " . T_ACTIONS_LOG . " ({$fields}) VALUES ({$data})");
    $aCTION_id = mysqli_insert_id($sqlConnect);

    return $aCTION_id;

}

function saveTesterActions($data){
    global $sh, $sqlConnect;

    if(empty($data) || !isset($data)){
        return false;
    }

    $fields = '`' . implode('`,`', array_keys($data)) . '`';
    $data   = '\'' . implode('\', \'', $data) . '\'';
    $query  = mysqli_query($sqlConnect, "INSERT INTO " . T_TESTER . " ({$fields}) VALUES ({$data})");
    $aCTION_id = mysqli_insert_id($sqlConnect);

    return $aCTION_id;

}

function ShAddBadLoginLog() {
    global $sh, $sqlConnect;
    if ($sh['loggedin'] == true) {
        return false;
    }
    $ip = get_ip_address();
    if (empty($ip)) {
        return true;
    }
    $time      = time();
    $query     = mysqli_query($sqlConnect, "INSERT INTO " . T_BAD_LOGIN . " (`ip`, `time`) VALUES ('{$ip}', '{$time}')");
    if ($query) {
        return true;
    }
}

function ShCanLogin() {
    global $sh, $sqlConnect,$db;
    if ($sh['loggedin'] == true) {
        return false;
    }
    $ip = get_ip_address();
    if (empty($ip)) {
        return true;
    }
    if ($sh['config']['lock_time'] < 1) {
        return true;
    }
    if ($sh['config']['bad_login_limit'] < 1) {
        return true;
    }

    $time      = time() - (60 * $sh['config']['lock_time']);
    $login = $db->where('ip',$ip)->get(T_BAD_LOGIN);
    if (count($login) >= $sh['config']['bad_login_limit']) {
        $last = end($login);
        if ($last->time >= $time) {
            return false;
        }
    }
    $db->where('time',time()-(60 * $sh['config']['lock_time'] * 2),'<')->delete(T_BAD_LOGIN);
    return true;
}

function Sh_Login($username, $password) {
    global $sqlConnect;
    if (empty($username) || empty($password)) {
        return false;
    }
    $username            = Sh_Secure($username);
    $query_hash          = mysqli_query($sqlConnect, "SELECT * FROM " . T_USERS . " WHERE (`username` = '{$username}' OR `email` = '{$username}' OR `phone_number` = '{$username}')");
    $mysqli_hash_upgrade = mysqli_fetch_assoc($query_hash);
    $login_password = '';
    $hash                = 'md5';
    if (preg_match('/^[a-f0-9]{32}$/', $mysqli_hash_upgrade['password'])) {
        $hash = 'md5';
    } else if (preg_match('/^[0-9a-f]{40}$/i', $mysqli_hash_upgrade['password'])) {
        $hash = 'sha1';
    } else if (strlen($mysqli_hash_upgrade['password']) == 60) {
        $hash = 'password_hash';
    }
    if ($hash == 'password_hash') {
        if (password_verify($password, $mysqli_hash_upgrade['password'])) {
            return true;
        }
    } else {
        $login_password = Sh_Secure($hash($password));
    }		//edie($login_password);	//echo "SELECT COUNT(`user_id`) FROM " . T_USERS . " WHERE (`username` = '{$username}' OR `email` = '{$username}' OR `phone_number` = '{$username}') AND `password` = '{$login_password}'";
    $query          = mysqli_query($sqlConnect, "SELECT COUNT(`user_id`) FROM " . T_USERS . " WHERE (`username` = '{$username}' OR `email` = '{$username}' OR `phone_number` = '{$username}') AND `password` = '{$login_password}'");
    if (Sh_Sql_Result($query, 0) == 1) {
        if ($hash == 'sha1' || $hash == 'md5') {
            $new_password = Sh_Secure(password_hash($password, PASSWORD_DEFAULT));
            $query_       = mysqli_query($sqlConnect, "UPDATE " . T_USERS . " SET password = '$new_password' WHERE (`username` = '{$username}' OR `email` = '{$username}' OR `phone_number` = '{$username}')");
        }
        return true;
    }
    return false;
}


function Sh_VerfiyIP($username = '') {
    global $sh, $db;
    if (empty($username)) {
        return false;
    }
    if ($sh['config']['login_auth'] == 0) {
        return true;
    }
    $getuser = Sh_UserData(Sh_UserIdForLogin($username));
    $get_ip = get_ip_address();
    $getIpInfo = fetchDataFromURL("http://ip-api.com/json/$get_ip");
    $getIpInfo = json_decode($getIpInfo, true);
    if ($getIpInfo['status'] == 'success' && !empty($getIpInfo['regionName']) && !empty($getIpInfo['countryCode']) && !empty($getIpInfo['timezone']) && !empty($getIpInfo['city'])) {
        $create_new = false;
        $_SESSION['last_login_data'] = $getIpInfo;
        if (empty($getuser['last_login_data'])) {
            $create_new = true;
        } else {
            $lastLoginData = (Array) json_decode($getuser['last_login_data']);
            if (($getIpInfo['regionName'] != $lastLoginData['regionName']) || ($getIpInfo['countryCode'] != $lastLoginData['countryCode']) || ($getIpInfo['timezone'] != $lastLoginData['timezone']) || ($getIpInfo['city'] != $lastLoginData['city'])) {
                // send email
                $code = rand(111111, 999999);
                $hash_code = md5($code);
                $sh['email']['username'] = $getuser['name'];
                $sh['email']['countryCode'] = $getIpInfo['countryCode'];
                $sh['email']['timezone'] = $getIpInfo['timezone'];
                $sh['email']['email'] = $getuser['email'];
                $sh['email']['ip_address'] = $get_ip;
                $sh['email']['code'] = $code;
                $sh['email']['city'] = $getIpInfo['city'];
                $sh['email']['date'] = date("Y-m-d h:i:sa");
                $update_code =  $db->where('user_id', $getuser['user_id'])->update(T_USERS, array('email_code' => $hash_code));
                $email_body = Sh_LoadPage("emails/unusual-login");
                $send_message_data       = array(
                    'from_email' => $sh['config']['siteEmail'],
                    'from_name' => $sh['config']['siteName'],
                    'to_email' => $getuser['email'],
                    'to_name' => $getuser['name'],
                    'subject' => 'Please verify that it’s you',
                    'charSet' => 'utf-8',
                    'message_body' => $email_body,
                    'is_html' => true
                );
                $send = Sh_SendMessage($send_message_data);
                if ($send && !empty($_SESSION['last_login_data'])) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }
        if ($create_new == true) {
            $lastLoginData = json_encode($getIpInfo);
            $update_user = $db->where('user_id', $getuser['user_id'])->update(T_USERS, array('last_login_data' => $lastLoginData));
            return true;
        }
        return false;
    } else {
        return true;
    }
}

function Sh_UserIdForLogin($username) {
    global $sqlConnect;
    if (empty($username)) {
        return false;
    }
    $username =   Sh_Secure($username);
    $query    = mysqli_query($sqlConnect, "SELECT `user_id` FROM " . T_USERS . " WHERE `username` = '{$username}' OR `email` = '{$username}' OR `phone_number` = '{$username}'");
    return Sh_Sql_Result($query, 0, 'user_id');
}

function Sh_TwoFactor($username = '', $id_or_u = 'user') {
    global $sh, $db;
    if (empty($username)) {
        return true;
    }
    if ($sh['config']['tSh_factor'] == 0) {
        return true;
    }

    if ($id_or_u == 'id') {
        $getuser = Sh_UserData($username);
    } else {
        $getuser = Sh_UserData(Sh_UserIdForLogin($username));
    }

    if ($getuser['tSh_factor'] == 0 || $getuser['tSh_factor_verified'] == 0) {
        return true;
    }

    $code = rand(111111, 999999);
    $hash_code = md5($code);
    $update_code =  $db->where('user_id', $getuser['user_id'])->update(T_USERS, array('email_code' => $hash_code));

    $message = "Your confirmation code is: $code";

    if (!empty($getuser['phone_number']) && ($sh['config']['tSh_factor_type'] == 'both' || $sh['config']['tSh_factor_type'] == 'phone')) {
        $send_message = Sh_SendSMSMessage($getuser['phone_number'], $message);
    }
    if ($sh['config']['tSh_factor_type'] == 'both' || $sh['config']['tSh_factor_type'] == 'email') {
        $send_message_data       = array(
            'from_email' => $sh['config']['siteEmail'],
            'from_name' => $sh['config']['siteName'],
            'to_email' => $getuser['email'],
            'to_name' => $getuser['name'],
            'subject' => 'Please verify that it’s you',
            'charSet' => 'utf-8',
            'message_body' => $message,
            'is_html' => true
        );
        $send = Sh_SendMessage($send_message_data);
    }
    return false;
}



function Sh_SendSMSMessage($to, $message) {
	// if($to=="9998887163"){return true;}
    //return true;
	global $sh, $sqlConnect;
    if (empty($to)) {
        return false;
    }
    if ($sh['config']['sms_provider'] == 'twilio' && !empty($sh['config']['sms_twilio_username']) && !empty($sh['config']['sms_twilio_password']) && !empty($sh['config']['sms_t_phone_number'])) {
        include_once('assets/libraries/twilio/vendor/autoload.php');
        $account_sid = $sh['config']['sms_twilio_username'];
        $auth_token  = $sh['config']['sms_twilio_password'];
        $to          = Sh_Secure($to);
        $client      = new Client($account_sid, $auth_token);
        try {
            $send = $client->account->messages->create($to, array(
                'from' => $sh['config']['sms_t_phone_number'],
                'body' => $message
            ));
            if ($send) {
                return true;
            }
        }
        catch (Exception $e) {
            return false;
        }
        return false;
    } else if ($sh['config']['sms_provider'] == 'infobip' && !empty($sh['config']['infobip_username']) && !empty($sh['config']['infobip_password'])) {
        $username = $sh['config']['infobip_username'];
        $password  = $sh['config']['infobip_password'];
        $to          = Sh_Secure($to);
        if (empty($to) || empty($sh['config']['infobip_username']) || empty($sh['config']['infobip_password']) ) {
            return false;
        }
        $postUrl = "https://api.infobip.com/sms/1/text/single";
        $sms = array(
            "from" => $sh['config']['siteName'],
            "to" => $to,
            "text" => $message
        );
        $postDataJson = json_encode($sms);
        try {
            $ch = curl_init();
            $header = array("Content-Type:application/json", "Accept:application/json");
            curl_setopt($ch, CURLOPT_URL, $postUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataJson);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            // response of the POST request
            $response = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $responseBody = json_decode($response);
            curl_close($ch);
            if ($httpCode >= 200 && $httpCode < 300) {
                return true;
            }else{
                return $responseBody->requestError->serviceException->text;
            }
        }
        catch (Exception $e) {
            return false;
        }
        return false;
    } else if ($sh['config']['sms_provider'] == 'bulksms' && !empty($sh['config']['sms_username']) && !empty($sh['config']['sms_password'])) {
        $username = $sh['config']['sms_username'];
        $password = $sh['config']['sms_password'];
        if (empty($to)) {
            return false;
        }
        $to_ = @explode('+', $to);
        if (empty($to_[1])) {
            return false;
        }
        $to      = $to_[1];
        $url     = $sh['config']['eapi'] . '/submission/send_sms/2/2.0';
        $data    = array(
            'username' => $username,
            'password' => $password,
            'msisdn' => $to,
            'message' => $message
        );
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data)
            ),
            'ssl' => array(
                "verify_peer" => false,
                "verify_peer_name" => false
            )
        );
        $context = stream_context_create($options);
        $result  = file_get_contents($url, false, $context);
        if (preg_match('/\bIN_PROGRESS\b/', $result)) {
            return true;
        } else {
            return $result;
        }
    } else if ($sh['config']['sms_provider'] == 'msg91' && !empty($sh['config']['msg91_authKey'])) {
        //Your authentication key
        $authKey = $sh['config']['msg91_authKey'];
        //Multiple mobiles numbers separated by comma
        $mobileNumber = $to;
        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = uniqid();
        //Define route
        $route = "4";
        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );
        //API URL
        $url="http://api.msg91.com/api/sendhttp.php";
        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));
        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        //get response
        $output = curl_exec($ch);
        //Print error if any
        if(curl_errno($ch))
        {
            return false;
        }
        curl_close($ch);
        return true;
    }
    return false;
}

function Sh_CreateSession() {
    $hash = sha1(rand(1111, 9999));
    if (!empty($_SESSION['hash_id'])) {
        $_SESSION['hash_id'] = $_SESSION['hash_id'];
        return $_SESSION['hash_id'];
    }
    $_SESSION['hash_id'] = $hash;
    return $hash;
}

function Sh_CheckSession($hash = '') {
    if (!isset($_SESSION['hash_id']) || empty($_SESSION['hash_id'])) {
        return false;
    }
    if (empty($hash)) {
        return false;
    }
    if ($hash == $_SESSION['hash_id']) {
        return true;
    }
    return false;
}

function Sh_CreateMainSession() {
    $hash = substr(sha1(rand(1111, 9999)), 0, 20);
    if (!empty($_SESSION['main_hash_id'])) {
        $_SESSION['main_hash_id'] = $_SESSION['main_hash_id'];
        return $_SESSION['main_hash_id'];
    }
    $_SESSION['main_hash_id'] = $hash;
    return $hash;
}

function Sh_CheckMainSession($hash = '') {
    if (!isset($_SESSION['main_hash_id']) || empty($_SESSION['main_hash_id'])) {
        return false;
    }
    if (empty($hash)) {
        return false;
    }
    if ($hash == $_SESSION['main_hash_id']) {
        return true;
    }
    return false;
}


function insertRow($table, $options){
    $query = "";

    foreach($options as $key => $option){
        if(empty($option)){
            unset($options[$key]);
        }

        $options[$key] = addslashes($option);
    }

    if(count($options)){
        $columns = implode(',', array_keys($options));
        $values = "'" .implode("','", array_values($options)) . "'";

        $query = "INSERT INTO {$table} ({$columns}) VALUES ({$values})";

    }
    return $query;
}

function Sh_SaveConfig($update_name, $value) {
    global $sh, $config, $sqlConnect;
    if ($sh['loggedin'] == false) {
        return false;
    }
    if (!array_key_exists($update_name, $config)) {
        return false;
    }
    $update_name = Sh_Secure($update_name);
    $value       = mysqli_real_escape_string($sqlConnect, $value);
    $query_one   = " UPDATE " . T_CONFIG . " SET `value` = '{$value}' WHERE `name` = '{$update_name}'";
    $query       = mysqli_query($sqlConnect, $query_one);
    if ($query) {
        return true;
    } else {
        return false;
    }
}


function Sh_IsNameExist($username, $active = 0) {
   global $sh, $sqlConnect;
   $data = array();

   if (empty($username)) {
       return false;
   }

   $active_text = '';
   if ($active == 1) {
       $active_text = "AND (`active` = '1' OR `active` = '0') ";
   }
   $username = Sh_Secure($username);

   $query   = mysqli_query($sqlConnect, "SELECT COUNT(`user_id`) as users FROM " . T_USERS . " WHERE `username` = '{$username}' {$active_text}");
   $fetched_data = mysqli_fetch_assoc($query);
   if ($fetched_data['users'] == 1) {
       return array(
           true,
           'type' => 'user'
       );
   }

   $query   = mysqli_query($sqlConnect, "SELECT COUNT(`id`) as pages FROM " . T_PAGES . " WHERE `status` = 1 AND `slug` = '{$username}' ");
    $fetched_data = mysqli_fetch_assoc($query);
     if ($fetched_data['pages'] == 1) {
        return array(
            true,
            'type' => 'page'
        );
    }

    return array(
       false
   );
}

function Sh_IsBanned($value = '') {
    global $sqlConnect;
    $value = Sh_Secure($value);
    $query_one    = mysqli_query($sqlConnect, "SELECT COUNT(`id`) as count FROM " . T_BANNED_IPS . " WHERE `ip_address` = '{$value}'");
    $fetched_data = mysqli_fetch_assoc($query_one);
    if ($fetched_data['count'] > 0) {
        return true;
    }
    return false;
}


function Sh_CheckIfUserCanRegister($num = 10) {
    global $sh, $sqlConnect;
    if ($sh['loggedin'] == true) {
        return false;
    }
    $ip = get_ip_address();
    if (empty($ip)) {
        return true;
    }
    $time      = time() - 3200;
    $query     = mysqli_query($sqlConnect, "SELECT COUNT(`user_id`) as count FROM " . T_USERS . " WHERE `ip_address` = '{$ip}' AND `joined` > {$time}");
    $sql_query = mysqli_fetch_assoc($query);
    if ($sql_query['count'] > $num) {
        return false;
    }
}

function Sh_EmailExists($email) {
    global $sqlConnect;
    if (empty($email)) {
        return false;
    }
    $email = Sh_Secure($email);
    $query = mysqli_query($sqlConnect, "SELECT COUNT(`user_id`) FROM " . T_USERS . " WHERE `email` = '{$email}'");
    return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}
function Sh_PhoneExists($phone) {
    global $sqlConnect;
    if (empty($phone)) {
        return false;
    }
    $phone = Sh_Secure($phone);
    $query = mysqli_query($sqlConnect, "SELECT COUNT(`user_id`) FROM " . T_USERS . " WHERE `phone_number` = '{$phone}'");
    return (Sh_Sql_Result($query, 0) > 1) ? true : false;
}



function Sh_RegisterUser($registration_data) {
    global $sh, $sqlConnect;

    if (empty($registration_data)) {
        return false;
    }

    if ($sh['config']['user_registration'] == 0) {
       return false;
    }

    $ip  = '0.0.0.0';
    $get_ip = get_ip_address();
    if (!empty($get_ip)) {
        $ip = $get_ip;
    }
    // if ($sh['config']['login_auth'] == 1) {
    //     $getIpInfo = fetchDataFromURL("http://ip-api.com/json/$get_ip");
    //     $getIpInfo = json_decode($getIpInfo, true);
    //     if ($getIpInfo['status'] == 'success' && !empty($getIpInfo['regionName']) && !empty($getIpInfo['countryCode']) && !empty($getIpInfo['timezone']) && !empty($getIpInfo['city'])) {
    //         $registration_data['last_login_data'] = json_encode($getIpInfo);
    //     }
    // }

    $Date = date("Y/m/d h:i:s");

    $registration_data['registered'] = date('n') . '/' . date("Y");
    $registration_data['joined']     = time();
    $registration_data['password']   = Sh_Secure(password_hash($registration_data['password'], PASSWORD_DEFAULT));
    $registration_data['ip_address'] = Sh_Secure($ip);
    $registration_data['language']   = $sh['config']['defualtLang'];
    $registration_data['special_date'] = $Date;
    $registration_data['custom_data'] = "";
    $registration_data['account_type'] = "user";
    $registration_data['user_type'] = 0;


    if (!empty($_SESSION['lang'])) {
        $lang_name = strtolower($_SESSION['lang']);
        $langs = Sh_LangsNamesFromDB();
        if (in_array($lang_name, $langs)) {
            $registration_data['language'] = Sh_Secure($lang_name);
        }
    }


    $fields  = '`' . implode('`,`', array_keys($registration_data)) . '`';
    $data    = '\'' . implode('\', \'', $registration_data) . '\'';
    $query   = mysqli_query($sqlConnect, "INSERT INTO " . T_USERS . " ({$fields}) VALUES ({$data})");
    $user_id = mysqli_insert_id($sqlConnect);

    if ($query) {
        return $user_id;
    } else {
        return 0;
    }

}


function ConfirmPageTypeStatus($title){
  global $sqlConnect;
  if (empty($title)) {
      return false;
  }
  $title = Sh_Secure($title);
  $query    = mysqli_query($sqlConnect, "SELECT COUNT(`id`) FROM " . T_PAGE_TYPE . "  WHERE (`name` = '{$title}' AND `name` LIKE '%{$title}%')  AND `status` = 1");
  return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}

function GetPageTypeData($title){
  global $sqlConnect;
  if (empty($title)) {
      return false;
  }
  $title = Sh_Secure($title);

  $query = mysqli_query($sqlConnect, "SELECT * FROM ". T_PAGE_TYPE. " WHERE (`name` = '{$title}' AND `name` LIKE '%{$title}%')  AND `status` = 1");
  $pquery = mysqli_fetch_assoc($query);

  return $pquery;

}


function createPageTypeAccount($table,$data){
  global $sh, $sqlConnect;

  if(empty($data) || !isset($data)){
      return false;
  }

  if(empty($table) || !isset($table)){
      return false;
  }

  $fields = '`' . implode('`,`', array_keys($data)) . '`';
  $data   = '\'' . implode('\', \'', $data) . '\'';
  $query  = mysqli_query($sqlConnect, "INSERT INTO `".$table."` ({$fields}) VALUES ({$data})");
  $form_id = mysqli_insert_id($sqlConnect);

  return $form_id;

}


function CheckUserPageTypeAccountExist($user_id,$table){
  global $sqlConnect;
  if (empty($user_id)) {
      return false;
  }

  if (empty($table)) {
      return false;
  }

  $table = Sh_Secure($table);
  $query = mysqli_query($sqlConnect, "SELECT COUNT(`id`) FROM `".$table."` WHERE `user_id` = $user_id");
  return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}


function UpdateMyUserPageTypeAccount($table,$user_id,$data){
  global $sh, $sqlConnect;

  if(empty($data) || !isset($data)){
      return false;
  }

  if(empty($table) || !isset($table)){
      return false;
  }

  if(empty($user_id) || !isset($user_id)){
      return false;
  }


  $queryU = "UPDATE `".$table."` SET ";

    foreach($data as $key => $value)
    {
        $i++;

        if(sizeof($data) > $i) {
            $queryU .= "`".$key."`= '{$value}', ";
        } else {
            $queryU .= "`".$key."`= '{$value}' ";
        }

    }
    $queryU .= " WHERE `user_id` = $user_id ";

    $queryUp  = mysqli_query($sqlConnect,$queryU);
    return $queryUp;

}

function getExpertsUserData($condition = ""){
  global $sh, $sqlConnect;

  $query = "SELECT * FROM ". T_USERS." WHERE `account_type` = 'experts' ".$condition;
  $result = mysqli_query($sqlConnect, $query);
  while($user_data = mysqli_fetch_assoc($result)){
      $users[] = Sh_UserData($user_data['user_id']);
  }
  return $users;

}


function GetSingleProfessioanlDataByCol($col,$id){
  global $sqlConnect;
  if (empty($col)) {
      return false;
  }

  if(empty($id) || !isset($id)){
      return false;
  }

  $col = Sh_Secure($col);

  $query = mysqli_query($sqlConnect, "SELECT $col FROM ". T_PROFESSIONAL_PLUG. " WHERE `id` = $id AND `status` = 1");
  $pquery = mysqli_fetch_assoc($query);

  return $pquery[$col];

}



function InsertReview($data){

  global $sh, $sqlConnect;

  if(empty($data) || !isset($data)){
      return false;
  }

  $fields = '`' . implode('`,`', array_keys($data)) . '`';
  $data   = '\'' . implode('\', \'', $data) . '\'';
  $query  = mysqli_query($sqlConnect, "INSERT INTO " . T_REVIEWS . " ({$fields}) VALUES ({$data})");
  $form_id = mysqli_insert_id($sqlConnect);

  return $form_id;

}

function InsertContactMessage($data){

  global $sh, $sqlConnect;

  if(empty($data) || !isset($data)){
      return false;
  }

  $fields = '`' . implode('`,`', array_keys($data)) . '`';
  $data   = '\'' . implode('\', \'', $data) . '\'';
  $query  = mysqli_query($sqlConnect, "INSERT INTO " . T_CONTACTS_MSG . " ({$fields}) VALUES ({$data})");
  $form_id = mysqli_insert_id($sqlConnect);

  return $form_id;

}

function InsertRealContactMessage($data){

  global $sh, $sqlConnect;

  if(empty($data) || !isset($data)){
      return false;
  }

  $fields = '`' . implode('`,`', array_keys($data)) . '`';
  $data   = '\'' . implode('\', \'', $data) . '\'';
  $query  = mysqli_query($sqlConnect, "INSERT INTO " . T_CONTACTS . " ({$fields}) VALUES ({$data})");
  $form_id = mysqli_insert_id($sqlConnect);

  return $form_id;

}

function GetAllReviewsByUser_id($user_id){
  global $sh, $sqlConnect;

  if($user_id == 0 || !isset($user_id) || !is_numeric($user_id)){
      return false;
  }

  $query = mysqli_query($sqlConnect, "SELECT * FROM ". T_REVIEWS. " WHERE `user_id` = $user_id AND `status` = 1");
  while($user_data = mysqli_fetch_assoc($query)){
      $resp[] = $user_data;
  }
  return $resp;

}

function ReviewAndRating($type,$user_id,$r_cat_name = ''){

  global $sh, $sqlConnect;

  if(empty($type) || !isset($type)){
      return false;
  }

  if($user_id == 0 || !isset($user_id) || !is_numeric($user_id)){
      return false;
  }

  if ($type == "total_ratings") {

    $query = mysqli_query($sqlConnect, "SELECT COUNT(`id`) as `total_ratings` FROM " . T_REVIEWS . " WHERE `status` = 1 AND `user_id` = $user_id");
    $pquery = mysqli_fetch_assoc($query);
    $resp = $pquery['total_ratings'];

  }elseif ($type == "total_by_all_r_cat") {

    $query = mysqli_query($sqlConnect, "SELECT * FROM " . T_REVIEWS . " WHERE `status` = 1 AND `user_id` = $user_id");
    while($user_data = mysqli_fetch_assoc($query)){
        $r_cats[] = senderReviewDataCalculation($user_data['review_cat_records']);
    }

    $score = 0;
    $count_row_cats = count($r_cats);

    foreach ($r_cats as $r_ck => $val_r_c) {
      $score += $val_r_c;
    }

    $resp = round($score/$count_row_cats, 1);



  }elseif ($type == "total_by_single_r_cat") {

    if(empty($r_cat_name) || !isset($r_cat_name)){
        return false;
    }

    $new_r_cat = add_to_String($r_cat_name);

    $query = mysqli_query($sqlConnect, "SELECT * FROM " . T_REVIEWS . " WHERE `status` = 1 AND `user_id` = $user_id");
    while($user_data = mysqli_fetch_assoc($query)){
        $r_cats[] = getOneReviewCategorySum($user_data['review_cat_records'],$new_r_cat);
    }

    $sum_t = 0;
    foreach ($r_cats as $ac => $acv) {
      $sum_t += $acv;
    }

    // average calculation
    $TT = count($r_cats) * 10;
    $AVG = ($sum_t / $TT) * 10;

    $resp = round($AVG,1);

  }

  return $resp;

}

function getOneReviewCategorySum($data,$col){

  $decode_r_cats = json_decode($data,true);

  $newArray = array();
  $nat = 0;

  foreach($decode_r_cats as $kkk => $val) {

    if ($kkk == $col) {

      $newArray[] = $val;

      foreach ($newArray as $nak => $nav) {

        $nat += $nav;

      }

    }

  }

  return $nat;
}


function senderReviewDataCalculation($data){

  global $sh, $sqlConnect;

  if(empty($data) || !isset($data)){
      return false;
  }

  $decode_r_cats = json_decode($data,true);
  $r_cat_count = count($decode_r_cats);

  // Add all the ratings from this user
  $sum_of_all_r_c = 0;
  foreach ($decode_r_cats as $d_r_c => $val) {
    $sum_of_all_r_c += $val;
  }

  // final calculation
  $all_r_cat_sum  = ($r_cat_count * 10);
  $total_cal = ($sum_of_all_r_c / $all_r_cat_sum) * 10;
  return $total_cal;


}


function getNumbersOfUserAttachToCat($cat_id){
  global $sqlConnect;

  if(empty($cat_id) || !isset($cat_id)){
      return false;
  }

  $query = mysqli_query($sqlConnect, "SELECT COUNT(`cat_id`) as `total_cat_attached` FROM " . T_USERS . " WHERE `active` = '1' AND `cat_id` = $cat_id");
  $pquery = mysqli_fetch_assoc($query);
  return $pquery['total_cat_attached'];

}

function getNumbersOfUserByStatis($cond = ""){
  global $sqlConnect;

  $query = mysqli_query($sqlConnect, "SELECT COUNT(`user_id`) as `total_user` FROM " . T_USERS . " ".$cond);
  $pquery = mysqli_fetch_assoc($query);
  return $pquery['total_user'];

}

function Sh_ActivateUser($email, $code) {

    global $sqlConnect;
    $email  = Sh_Secure($email);
    $code   = Sh_Secure($code);
    $query  = mysqli_query($sqlConnect, " SELECT COUNT(`user_id`)  FROM " . T_USERS . "  WHERE `email` = '{$email}' AND `email_code` = '{$code}' AND `active` = '0'");
    $result = Sh_Sql_Result($query, 0);
    if ($result == 1) {
        $query_two = mysqli_query($sqlConnect, " UPDATE " . T_USERS . "  SET `active` = '1' WHERE `email` = '{$email}' ");
        if ($query_two) {
            return true;
        }
    } else {
        return false;
    }
}


function Sh_isValidPasswordResetToken($string) {
    global $sqlConnect;
    $string_exp = explode('_', $string);
    $user_id    = Sh_Secure($string_exp[0]);
    $password   = Sh_Secure($string_exp[1]);
    if (empty($user_id) or !is_numeric($user_id) or $user_id < 1) {
        return false;
    }
    if (empty($password)) {
        return false;
    }
    $query = mysqli_query($sqlConnect, " SELECT COUNT(`user_id`) FROM " . T_USERS . " WHERE `user_id` = {$user_id} AND `email_code` = '{$password}' AND `active` = '1' ");
    return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}
function Sh_isValidPasswordResetToken2($string) {
    global $sqlConnect;
    $string_exp = explode('_', $string);
    $user_id    = Sh_Secure($string_exp[0]);
    $password   = Sh_Secure($string_exp[1]);
    if (empty($user_id) or !is_numeric($user_id) or $user_id < 1) {
        return false;
    }
    if (empty($password)) {
        return false;
    }
    $query = mysqli_query($sqlConnect, " SELECT COUNT(`user_id`) FROM " . T_USERS . " WHERE `user_id` = {$user_id} AND `password` = '{$password}' AND `active` = '1' ");
    return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}

function Sh_ResetPassword($user_id, $password) {
    global $sqlConnect;
    if (empty($user_id) || !is_numeric($user_id) || $user_id < 0) {
        return false;
    }
    if (empty($password)) {
        return false;
    }
    $user_id  = Sh_Secure($user_id);
    $password = Sh_Secure(password_hash($password, PASSWORD_DEFAULT));
    $query    = mysqli_query($sqlConnect, " UPDATE " . T_USERS . " SET `password` = '{$password}' WHERE `user_id` = {$user_id} ");
    if ($query) {
        return true;
    } else {
        return false;
    }
}

function GetUserByCol($col,$user_id){
  global $sqlConnect;

  $query = "SELECT $col FROM ". T_USERS." WHERE `user_id` = $user_id";
  $pquery = mysqli_query($sqlConnect,$query);
  $vquery = mysqli_fetch_assoc($pquery);

  return $vquery[$col];
}

function GetAllContactMessageByUser($user_id){
  global $sqlConnect;

  if (empty($user_id) || !is_numeric($user_id) || $user_id < 0) {
      return false;
  }

  $data = array();
  $query = mysqli_query($sqlConnect, "SELECT * FROM " . T_CONTACTS_MSG." WHERE `status` = 1 AND `user_id` = $user_id");

  while ($fetched_data = mysqli_fetch_assoc($query)) {
      $data[] = $fetched_data;
  }

  return $data;
}


function Sh_PageIdFromPagename($page_name = '') {
    global $sqlConnect;
    if (empty($page_name)) {
        return false;
    }
    $page_name = Sh_Secure($page_name);
    $query     = mysqli_query($sqlConnect, "SELECT `id` FROM " . T_PAGES . " WHERE `slug` = '{$page_name}'");
    return Sh_Sql_Result($query, 0, 'id');
}

function Sh_PageData($page_id = 0) {
    global $sh, $sqlConnect, $cache;
    if (empty($page_id) || !is_numeric($page_id) || $page_id < 0) {
        return false;
    }
    $data           = array();
    $page_id        = Sh_Secure($page_id);
    $query_one      = "SELECT * FROM " . T_PAGES . " WHERE `id` = {$page_id}";
    $hashed_page_Id = md5($page_id);
    if ($sh['config']['cacheSystem'] == 1) {
        $fetched_data = $cache->read($hashed_page_Id . '_PAGE_Data.tmp');
        if (empty($fetched_data)) {
            $sql          = mysqli_query($sqlConnect, $query_one);
            $fetched_data = mysqli_fetch_assoc($sql);
            $cache->write($hashed_page_Id . '_PAGE_Data.tmp', $fetched_data);
        }
    } else {
        $sql          = mysqli_query($sqlConnect, $query_one);
        $fetched_data = mysqli_fetch_assoc($sql);
    }
    if (empty($fetched_data)) {
        return array();
    }

    $fetched_data['about']    = $fetched_data['page_description'];
    $fetched_data['id']       = $fetched_data['id'];
    $fetched_data['type']     = 'page';
    $fetched_data['url']      = Sh_Link('index.php?link1=timeline&u=' . $fetched_data['slug']);
    $fetched_data['name']     = $fetched_data['page_title'];
    $fetched_data['page_location']   = $fetched_data['page_location'];
    $fetched_data['username']      = $fetched_data['slug'];

    return $fetched_data;
}

function Sh_PageActive($page_name) {
    global $sqlConnect;
    if (empty($page_name)) {
        return false;
    }
    $page_name = Sh_Secure($page_name);
    $query     = mysqli_query($sqlConnect, "SELECT COUNT(`page_id`) FROM " . T_PAGES . "  WHERE `slug`= '{$page_name}' AND `status` = '1'");
    return (Sh_Sql_Result($query, 0) == 1) ? true : false;
}


 ?>
