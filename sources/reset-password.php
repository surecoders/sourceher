<?php
if ($sh['loggedin'] == true) {
  header("Location: " . $sh['config']['site_url']);
  exit();
}
if (empty($_GET['code'])) {
	header("Location: " . $sh['config']['site_url']);
    exit();
}
$file = 'reset-password';
$validate = Sh_isValidPasswordResetToken($_GET['code']);
if ($validate === false) {
	$validate = Sh_isValidPasswordResetToken2($_GET['code']);
	if ($validate === false) {
		$file = 'invalid-markup';
	}
}
$sh['description'] = $sh['config']['siteDesc'];
$sh['keywords']    = $sh['config']['siteKeywords'];
$sh['page']        = 'welcome';
$sh['title']       = $sh['config']['siteTitle'];
$sh['content']     = Sh_LoadPage('welcome/' . $file);
