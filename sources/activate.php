<?php
$sh['description'] = $sh['config']['siteDesc'];
$sh['keywords']    = $sh['config']['siteKeywords'];
$sh['page']        = 'activate';
$sh['title']       = $sh['lang']['home'] . ' | ' . $sh['config']['siteTitle'];
$sh['content']     = Sh_LoadPage('activate/content');
