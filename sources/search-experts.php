<?php

if (isset($_GET['query'])) {
  $s_query = Sh_Secure($_GET['query']);
}else {
  $s_query = "";
}



$sh['description'] = $sh['config']['siteDesc'];
$sh['keywords']    = $sh['config']['siteKeywords'];
$sh['page']        = 'search-experts';
$sh['query'] = $s_query;
$sh['title']       = $sh['config']['siteTitle'];
$sh['content']     = Sh_LoadPage('search-experts/content');
