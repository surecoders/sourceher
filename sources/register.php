<?php
if ($sh['loggedin'] == true) {
  header("Location: " . $sh['config']['site_url']);
  exit();
}

if ($sh['config']['register_type_system'] == 1 ) {

	header("Location: " . $sh['config']['site_url']);
    exit();

}

$page = 'register';
$sh['description'] = $sh['config']['siteDesc'];
$sh['keywords']    = $sh['config']['siteKeywords'];
$sh['page']        = 'register';
$sh['title']       = $sh['config']['siteTitle'];
$sh['content']     = Sh_LoadPage('welcome/'.$page);
