<?php
if ($sh['loggedin'] == false) {
    // if ($sh['config']['profile_privacy'] == 0) {
        // header("Location: " . Sh_Link('welcome'));
        // exit();
    // }
}

if (isset($_GET['u'])) {
  // check if username exist
  $check_user = Sh_IsNameExist($_GET['u'], 1);

  if (in_array(true, $check_user)) {
    // it means the username is found and most likely active or inactive

    if ($check_user['type'] == 'user') {

      $id                 = $user_id = Sh_UserIdFromUsername($_GET['u']);
      $sh['user_profile'] = Sh_UserData($user_id);
      $type               = 'timeline';
      $about              = $sh['user_profile']['about'];
      $name               = $sh['user_profile']['name'];

    } else if ($check_user['type'] == 'page') {

      $id                 = $page_id = Sh_PageIdFromPagename($_GET['u']);
      $sh['page_profile'] = Sh_PageData($page_id);
      $type               = 'pages';
      $about              = $sh['page_profile']['page_description'];
      $name               = $sh['page_profile']['page_title'];

    }
    // display if active for admin
    // if ($check_user['active'] == '1') {
    //
    //   $id                 = $user_id = Sh_UserIdFromUsername($_GET['u']);
    //   $sh['user_profile'] = Sh_UserData($user_id);
    //   $type               = 'timeline';
    //   $about              = $sh['user_profile']['about'];
    //   $name               = $sh['user_profile']['name'];
    //
    // }else {
    //
    //   if (Sh_IsAdmin()) {
    //
    //     $id                 = $user_id = Sh_UserIdFromUsername($_GET['u']);
    //     $sh['user_profile'] = Sh_UserData($user_id);
    //     $type               = 'timeline';
    //     $about              = $sh['user_profile']['about'];
    //     $name               = $sh['user_profile']['name'];
    //
    //
    //   }else {
    //
    //     header("Location: " . Sh_Link('404'));
    //     exit();
    //
    //   }
    //
    //
    // }


  }

}else{

  header("Location: " . $sh['config']['site_url']);
  exit();

}




if (!empty($_GET['ref'])) {
    if ($_GET['ref'] == 'se') {
        $regsiter_recent = Sh_RegsiterRecent($id, $type);
    }
}

$con2 = 1;

// if ($type == 'timeline') {
//     $user_data = Sh_UpdateUserDetails($sh['user_profile'], true, true, true);
//     if (is_array($user_data)) {
//         $sh['user_profile'] = $user_data;
//         $about  = $sh['user_profile']['about'];
//         $name   = $sh['user_profile']['name'];
//         $sh['user_profile']['fields'] = Sh_UserFieldsData($user_id);
//     }
// }

// if ($type == 'timeline' && $sh['loggedin'] == true) {
//
//     $is_blocked = $sh['is_blocked'] = Sh_IsBlocked($user_id);
//
//     if ($is_blocked) {
//        // $con2 = 0;
//        // if (!isset($came_from)) {
//        //     header("Location: " . $sh['config']['site_url']);
//        //     exit();
//        // } else {
//        //     Sh_RedirectSmooth(Sh_SeoLink('index.php?link1=404'));
//        // }
//     }
//
// }

$can_ = 0;

if ($sh['loggedin'] == true && $sh['config']['profileVisit'] == 1 && $type == 'timeline') {
    if ($sh['user_profile']['user_id'] != $sh['user']['user_id'] && $sh['user']['visit_privacy'] == 0) {

        $can_ = 1;

        if ($sh['user_profile']['visit_privacy'] == 0 && $can_ == 1) {
            $notification_data_array = array(
                'recipient_id' => $sh['user_profile']['user_id'],
                'type' => 'visited_profile',
                'url' => 'index.php?link1=timeline&u=' . $sh['user']['username']
            );
            Sh_RegisterNotification($notification_data_array);
        }
    }
}

if (!empty($_GET['type']) && in_array($_GET['type'], array('activities','mutual_friends','following','followers','videos','photos','likes','groups','family_list','requests'))) {
    $name = $name ." | ".Sh_Secure($_GET['type']);
}
$sh['description'] = $about;
$sh['keywords']    = '';
$sh['page']        = $type;
$sh['title']       = str_replace('&#039;', "'", $name);
$sh['content']     = Sh_LoadPage("$type/content");
