<?php

$sh['description'] = $sh['config']['siteDesc'];
$sh['keywords']    = $sh['config']['siteKeywords'];
$sh['page']        = 'contact-us';
$sh['title']       = $sh['config']['siteTitle'];
$sh['content']     = Sh_LoadPage('contact-us/content');
