<?php
if (empty($_SESSION['code_id'])) {
	 header("Location: " . Sh_Link('welcome'));
    exit();
}
$sh['description'] = '';
$sh['keywords']    = '';
$sh['page']        = 'welcome';
$sh['title']       = $sh['lang']['confirm_your_account'];

if (!empty($_GET['type'])) {
	if ($_GET['type'] == 'two-factor') {
		$sh['lang']['confirm_your_account'] = $sh['lang']['two_factor'];
		$sh['lang']['sign_in_attempt'] = $sh['lang']['to_log_in_two_factor'];
		if ($sh['config']['two_factor_type'] == 'both') {
			$sh['lang']['we_have_sent_you_code'] = $sh['lang']['sent_two_factor_both'];
		} else if ($sh['config']['two_factor_type'] == 'email') {
			$sh['lang']['we_have_sent_you_code'] =  $sh['lang']['sent_two_factor_email'];
		} else if ($sh['config']['two_factor_type'] == 'phone') {
			$sh['lang']['we_have_sent_you_code'] = $sh['lang']['sent_two_factor_phone'];
		}
	} else {
		header("Location: " . Sh_Link('welcome'));
        exit();
	}
}
$sh['content']     = Sh_LoadPage('welcome/unusual-login');
