<?php
if ($sh['loggedin'] == false) {
  header("Location: " . $sh['config']['site_url']);
  exit();
}

if ( empty($_GET['type']) || !isset($_GET['type']) || empty($_GET['mycode']) || !isset($_GET['mycode'])  ) {

  header("Location: " . $sh['config']['site_url']);
  exit();

}else {

  // user id
  $user_id = $sh['user']['user_id'];

  // page type form url
  $pg_type = Sh_Secure($_GET['type']);

  $mycode = $_GET['mycode'];

  // check if the code is the same as users experts code..
  // if code is not correct then redirect to error 404 page.
  $myhash = GetUserByCol('expert_access_code',$user_id);

  // check if page is part of the page type under user form
  $pg_type_check = ConfirmPageTypeStatus($pg_type);


  if ($pg_type_check) {

    if ($myhash == $mycode) {

      $sh['description'] = $sh['config']['siteDesc'];
      $sh['keywords']    = $sh['config']['siteKeywords'];
      $sh['page']        = 'user-form';
      $sh['title']       = ucfirst($pg_type)." Registration Page | ". $sh['config']['siteTitle'];

      $pg_data = GetPageTypeData($pg_type);

      if ($pg_data['name'] != $sh['user']['account_type']) {

        $loadablePage = $pg_type;

        $sh['pg_type'] = $pg_type;
        $sh['pageData'] = getFormBuilderDataByPageType($pg_data['id']);
        $sh['content']     = Sh_LoadPage($loadablePage.'/content');

      }else {

        $loadablePage = "form-already";
        $sh['content']     = Sh_LoadPage($loadablePage.'/content');

      }


    }else {


      $loadablePage = "form-already";
      $sh['content']     = Sh_LoadPage($loadablePage.'/content');

    }


  }else {

    header("Location: " . $sh['config']['site_url']);
    exit();

  }



}
