<?php
if ($sh['loggedin'] == true) {
  header("Location: " . $sh['config']['site_url']);
  exit();
}

if ( $sh['config']['login_type_system'] == 1 ) {

	header("Location: " . $sh['config']['site_url']);
    exit();

}

$page = 'login';
$sh['description'] = $sh['config']['siteDesc'];
$sh['keywords']    = $sh['config']['siteKeywords'];
$sh['page']        = 'login';
$sh['title']       = $sh['config']['siteTitle'];
$sh['content']     = Sh_LoadPage('welcome/'.$page);
