<?php
if ($sh['loggedin'] == false) {
  header("Location: " . Sh_Link('404'));
  exit();
}


if (isset($_GET['u'])) {

    $check_user = Sh_IsNameExist($_GET['u'], 1);

    if (in_array(true, $check_user)) {

        if ($check_user['type'] == 'user') {

            $id = $user_id = Sh_UserIdFromUsername($_GET['u']);
            $sh['user_profile'] = Sh_UserData($user_id);
            $type  = 'timeline';
            $about = $sh['user_profile']['about'];
            $name  = $sh['user_profile']['name'];

        }

    } else {
        header("Location: " . Sh_Link('404'));
        exit();
    }

} else {
    header("Location: " . $sh['config']['site_url']);
    exit();
}

if ($sh['user']['user_id'] == $sh['user_profile']['user_id']) {
  header("Location: " . Sh_Link('404'));
  exit();
}

$sh['description'] = $sh['config']['description'];
$sh['keywords']    = $sh['config']['keywords'];
$sh['page']        = "review";
$sh['title']       = "Write Review on ".GetSingleProfessioanlDataByCol('title',$sh['user_profile']['professional']).". ".$sh['user_profile']['name'];
$sh['content']     = Sh_LoadPage("review/content");
