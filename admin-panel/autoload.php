<?php
$page  = 'dashboard';

// Pages settings
$pages = array(
    'general-settings',
    'site-setting',
    'manage-users',
    'manage-experts',
    'dashboard',
    'form-creator',
    'form-builder',
    'form-builder-editor',
    'professional-plugin',
    'menu-settings',
    'rating-system',
    'category-page',
    'subscribers',
    'create-page',
    'page-list',
    'edit-page',
    'other-settings',
    'home-settings'
);

if (!empty($_GET['page'])) {
    $page = Sh_Secure($_GET['page'], 0);
}

if ($is_admin == false) {
    if (!in_array($page, $mod_pages)) {
        header("Location: " . Sh_Link(''));
        exit();
    }
}

if (in_array($page, $pages)) {
    $page_loaded = Sh_LoadAdminPage("$page/content");
}
if (empty($page_loaded)) {
    header("Location: " . Sh_Link('admin-cp'));
    exit();
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>Admin Panel | <?php echo $sh['config']['siteTitle']; ?></title>
    <link rel="icon" type="image/x-icon" href="<?php echo $wo['config']['theme_url']; ?>/images/icon.png"/>
    <link href="<?= Sh_LoadAdminLink('css/loader.css') ?>" rel="stylesheet" type="text/css" />
    <script src="<?= Sh_LoadAdminLink('js/loader.js') ?>"></script>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&amp;display=swap" rel="stylesheet">
    <link href="<?= Sh_LoadAdminLink('bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?= Sh_LoadAdminLink('css/plugins.css') ?>" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->

    <?php if ($page == 'dashboard' || $page == 'rating-system') { ?>
      <link href="<?= Sh_LoadAdminLink('plugins/apex/apexcharts.css') ?>" rel="stylesheet" type="text/css">
      <link href="<?= Sh_LoadAdminLink('css/dashboard/dash_1.css') ?>" rel="stylesheet" type="text/css" class="dashboard-analytics" />
      <link rel="stylesheet" type="text/css" href="<?= Sh_LoadAdminLink('css/elements/alert.css') ?>">
      <link rel="stylesheet" href="<?= Sh_LoadAdminLink('css/widgets/modules-widgets.css') ?>">
      <link href="<?= Sh_LoadAdminLink('css/components/cards/card.css') ?>" rel="stylesheet" type="text/css" />
    <?php } ?>

    <?php if ($page == 'general-settings') { ?>
      <link href="<?= Sh_LoadAdminLink('css/scrollspyNav.css') ?>" rel="stylesheet" type="text/css" />
      <link href="<?= Sh_LoadAdminLink('css/forms/theme-checkbox-radio.css') ?>" rel="stylesheet" type="text/css">
      <link rel="stylesheet" type="text/css" href="<?= Sh_LoadAdminLink('css/forms/switches.css') ?>">

      <link rel="stylesheet" href="<?= Sh_LoadAdminLink('plugins/coloris/dist/coloris.min.css') ?>" />
      <script src="<?= Sh_LoadAdminLink('plugins/coloris/dist/coloris.min.js') ?>"></script>
    <?php } ?>

    <?php if ($page == 'manage-users' || $page == "page-list" || $page == "manage-experts" || $page == 'subscribers' || $page == 'form-creator' || $page == 'form-builder' || $page == 'form-builder-editor' || $page == 'professional-plugin' || $page == 'category-page' || $page == 'rating-system') { ?>
      <link rel="stylesheet" type="text/css" href="<?= Sh_LoadAdminLink('plugins/table/datatable/datatables.css') ?>">
      <link rel="stylesheet" type="text/css" href="<?= Sh_LoadAdminLink('plugins/table/datatable/dt-global_style.css') ?>">
      <link rel="stylesheet" type="text/css" href="<?= Sh_LoadAdminLink('plugins/table/datatable/custom_dt_custom.css') ?>">
    <?php } ?>

    <?php if ($page == "create-page" || $page == "edit-page") { ?>
      <link rel="stylesheet" href="<?= Sh_LoadAdminLink('plugins/editors/quill/quill.snow.css') ?>">
    <?php } ?>

    <?php if ($page == 'form-builder' || $page == 'form-creator' || $page == 'form-builder-editor' || $page == 'professional-plugin' || $page == 'category-page' || $page == 'rating-system' || $page == 'manage-users' || $page == "manage-experts" || $page == 'subscribers') { ?>
      <link href="<?= Sh_LoadAdminLink('css/apps/invoice-add.css') ?>" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" type="text/css" href="<?= Sh_LoadAdminLink('plugins/dropify/dropify.min.css') ?>">
      <link rel="stylesheet" type="text/css" href="<?= Sh_LoadAdminLink('css/forms/theme-checkbox-radio.css') ?>">

      <link rel="stylesheet" type="text/css" href="<?= Sh_LoadAdminLink('plugins/bootstrap-select/bootstrap-select.min.css') ?>">

      <link href="<?= Sh_LoadAdminLink('plugins/flatpickr/flatpickr.css') ?>" rel="stylesheet" type="text/css">
      <link href="<?= Sh_LoadAdminLink('plugins/flatpickr/custom-flatpickr.css') ?>" rel="stylesheet" type="text/css">




      <script>
       let pageN = "<?= $_GET['page'] ?>";
      </script>
    <?php } ?>

    <link href="<?= Sh_LoadAdminLink('plugins/notification/snackbar/snackbar.min.css') ?>" rel="stylesheet" type="text/css" />

    <?php if ($page == "site-setting" || $page == 'home-settings' || $page == 'other-settings' || $page == 'category-page') { ?>
      <link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet" />
      <link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css" rel="stylesheet" />

      <link href="https://unpkg.com/filepond-plugin-file-poster/dist/filepond-plugin-file-poster.css" rel="stylesheet" />

      <link href="https://unpkg.com/filepond-plugin-image-edit/dist/filepond-plugin-image-edit.css" rel="stylesheet" />
    <?php } ?>


    <link href="<?= Sh_LoadAdminLink('css/components/custom-modal.css') ?>" rel="stylesheet" type="text/css" />

    <script src="https://kit.fontawesome.com/01b9b7eb33.js" crossorigin="anonymous"></script>



    <link rel="stylesheet" type="text/css" href="<?= Sh_LoadAdminLink('plugins/animate/animate.css') ?>">


    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="<?= Sh_LoadAdminLink('plugins/jquery.form.js') ?>"></script>
    <script src="<?= Sh_LoadAdminLink('bootstrap/js/popper.min.js') ?>"></script>
    <script src="<?= Sh_LoadAdminLink('bootstrap/js/bootstrap.min.js') ?>"></script>
    <script src="<?= Sh_LoadAdminLink('plugins/perfect-scrollbar/perfect-scrollbar.min.js') ?>"></script>
    <script src="<?= Sh_LoadAdminLink('js/app.js') ?>"></script>

    <script src="<?= Sh_LoadAdminLink('plugins/notification/snackbar/snackbar.min.js') ?>"></script>

    <script src="<?= Sh_LoadAdminLink('js/pageprocess.js') ?>"></script>

    <link href="<?= Sh_LoadAdminLink('plugins/sweetalerts/sweetalert2.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?= Sh_LoadAdminLink('plugins/sweetalerts/sweetalert.css') ?>" rel="stylesheet" type="text/css" />

    <script>
        $(document).ready(function() {
            App.init();
        });

        function Sh_Ajax_Requests_File(){
            return "<?php echo $sh['config']['site_url'].'/requests.php';?>"
        }
    </script>
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->


    <!-- Pages Javascripts -->

    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <?php if ($page == 'site-setting' || $page == 'home-settings' || $page == 'other-settings' || $page == 'category-page') { ?>
    <!-- include FilePond library -->
    <script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>

    <script src="https://unpkg.com/filepond-plugin-file-encode/dist/filepond-plugin-file-encode.min.js"></script>

    <script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.min.js"></script>

    <script src="https://unpkg.com/filepond-plugin-image-exif-orientation/dist/filepond-plugin-image-exif-orientation.min.js"></script>

    <script src="https://unpkg.com/filepond-plugin-image-crop/dist/filepond-plugin-image-crop.min.js"></script>

    <script src="https://unpkg.com/filepond-plugin-image-resize/dist/filepond-plugin-image-resize.min.js"></script>

    <script src="https://unpkg.com/filepond-plugin-image-transform/dist/filepond-plugin-image-transform.min.js"></script>

    <script src="https://unpkg.com/filepond-plugin-file-poster/dist/filepond-plugin-file-poster.js"></script>

    <!-- include FilePond plugins -->
    <script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.js"></script>

    <script src="https://unpkg.com/filepond-plugin-image-edit/dist/filepond-plugin-image-edit.js"></script>

    <!-- include FilePond jQuery adapter -->
    <script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>

  <?php } ?>
</head>
<body class="dashboard-analytics">

    <!-- BEGIN LOADER -->
    <div id="load_screen"> <div class="loader"> <div class="loader-content">
        <div class="spinner-grow align-self-center"></div>
    </div></div></div>
    <!--  END LOADER -->

    <!--  BEGIN NAVBAR  -->
    <div class="header-container fixed-top">
        <header class="header navbar navbar-expand-sm">
            <ul class="navbar-item flex-row">
                <li class="nav-item align-self-center page-heading">
                    <div class="page-header">
                        <div class="page-title">
                            <h3>Dashboard</h3>
                        </div>
                    </div>
                </li>
            </ul>
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
            </a>

            <ul class="navbar-item flex-row search-ul">
                <li class="nav-item align-self-center search-animated">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search toggle-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>
                    <form class="form-inline search-full form-inline search" role="search">
                        <div class="search-bar">
                            <input type="text" class="form-control search-form-control  ml-lg-auto" placeholder="Type here to search">
                        </div>
                    </form>
                </li>
            </ul>

            <ul class="navbar-item flex-row navbar-dropdown">
                <!-- <li class="nav-item dropdown language-dropdown more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <a class="nav-link dropdown-toggle" href="#" id="customDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?= Sh_LoadAdminLink('images/flag-ca.svg') ?>" class="flag-width" alt="flag"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg></a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="customDropdown">
                            <a class="dropdown-item" data-img-value="flag-de" data-value="German" href="javascript:void(0);"><img src="<?= Sh_LoadAdminLink('images/flag-de.svg') ?>" class="flag-width" alt="flag"> German</a>
                            <a class="dropdown-item" data-img-value="flag-sp" data-value="Japanese" href="javascript:void(0);"><img src="<?= Sh_LoadAdminLink('images/flag-sp.svg') ?>" class="flag-width" alt="flag"> Spanish</a>
                            <a class="dropdown-item" data-img-value="flag-fr" data-value="French" href="javascript:void(0);"><img src="<?= Sh_LoadAdminLink('images/flag-fr.svg') ?>" class="flag-width" alt="flag"> French</a>
                            <a class="dropdown-item" data-img-value="flag-ca" data-value="English" href="javascript:void(0);"><img src="<?= Sh_LoadAdminLink('images/flag-ca.svg') ?>" class="flag-width" alt="flag"> English</a>
                        </div>
                    </div>
                </li> -->

                <!-- <li class="nav-item dropdown message-dropdown">
                    <a href="javascript:void(0);" class="nav-link dropdown-toggle" id="messageDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-circle"><path d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z"></path></svg><span class="badge badge-primary"></span>
                    </a>
                    <div class="dropdown-menu position-absolute" aria-labelledby="messageDropdown">
                        <div class="">
                            <a class="dropdown-item">
                                <div class="">

                                    <div class="media">
                                        <div class="user-img">
                                            <div class="avatar avatar-xl">
                                                <span class="avatar-title rounded-circle">KY</span>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <div class="">
                                                <h5 class="usr-name">Kara Young</h5>
                                                <p class="msg-title">ACCOUNT UPDATE</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </a>
                            <a class="dropdown-item">
                                <div class="">
                                    <div class="media">
                                        <div class="user-img">
                                            <div class="avatar avatar-xl">
                                                <span class="avatar-title rounded-circle">DA</span>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <div class="">
                                                <h5 class="usr-name">Daisy Anderson</h5>
                                                <p class="msg-title">ACCOUNT UPDATE</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="dropdown-item">
                                <div class="">

                                    <div class="media">
                                        <div class="user-img">
                                            <div class="avatar avatar-xl">
                                                <span class="avatar-title rounded-circle">OG</span>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <div class="">
                                                <h5 class="usr-name">Oscar Garner</h5>
                                                <p class="msg-title">ACCOUNT UPDATE</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </a>
                        </div>
                    </div>
                </li> -->

                <!-- <li class="nav-item dropdown notification-dropdown">
                    <a href="javascript:void(0);" class="nav-link dropdown-toggle" id="notificationDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell"><path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path><path d="M13.73 21a2 2 0 0 1-3.46 0"></path></svg><span class="badge badge-success"></span>
                    </a>
                    <div class="dropdown-menu position-absolute" aria-labelledby="notificationDropdown">
                        <div class="notification-scroll">

                            <div class="dropdown-item">
                                <div class="media server-log">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-server"><rect x="2" y="2" width="20" height="8" rx="2" ry="2"></rect><rect x="2" y="14" width="20" height="8" rx="2" ry="2"></rect><line x1="6" y1="6" x2="6" y2="6"></line><line x1="6" y1="18" x2="6" y2="18"></line></svg>
                                    <div class="media-body">
                                        <div class="data-info">
                                            <h6 class="">Server Rebooted</h6>
                                            <p class="">45 min ago</p>
                                        </div>

                                        <div class="icon-status">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="dropdown-item">
                                <div class="media ">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg>
                                    <div class="media-body">
                                        <div class="data-info">
                                            <h6 class="">Licence Expiring Soon</h6>
                                            <p class="">8 hrs ago</p>
                                        </div>

                                        <div class="icon-status">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="dropdown-item">
                                <div class="media file-upload">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                                    <div class="media-body">
                                        <div class="data-info">
                                            <h6 class="">Kelly Portfolio.pdf</h6>
                                            <p class="">670 kb</p>
                                        </div>

                                        <div class="icon-status">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li> -->

            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->

    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

        <!--  BEGIN SIDEBAR  -->
        <div class="sidebar-wrapper sidebar-theme">

            <nav id="compactSidebar">

                <div class="theme-logo">
                    <a href="">
                        <img src="<?= Sh_LoadAdminLink('images/logo.svg') ?>" class="navbar-logo" alt="logo">
                    </a>
                </div>

                <ul class="menu-categories">
                    <li class="menu menu-single active">
                        <a href="<?= Sh_LoadAdminLinkSettings('dashboard') ?>" data-active="true" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                                </div>
                            </div>
                        </a>
                        <div class="tooltip"><span>Dashboard</span></div>
                    </li>

                    <li class="menu">
                        <a href="#accounts" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                                </div>
                            </div>
                        </a>
                        <div class="tooltip"><span>Accounts</span></div>
                    </li>

                    <li class="menu">
                        <a href="#plugins" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-box"><path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path><polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline><line x1="12" y1="22.08" x2="12" y2="12"></line></svg>
                                </div>
                            </div>
                        </a>
                        <div class="tooltip"><span>Plugins</span></div>
                    </li>

                    <li class="menu">
                        <a href="#more" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg>
                                </div>
                            </div>
                        </a>
                        <div class="tooltip"><span>Settings</span></div>
                    </li>
                </ul>

                <div class="sidebar-bottom-actions">

                    <!-- <div class="external-links">
                        <a target="_blank" href="https://designreset.com/cork/documentation/index.html" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-help-circle"><circle cx="12" cy="12" r="10"></circle><path d="M9.09 9a3 3 0 0 1 5.83 1c0 2-3 3-3 3"></path><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>
                            <div class="tooltip"><span>Help</span></div>
                        </a>
                    </div> -->

                    <div class="dropdown user-profile-dropdown">
                        <a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="userProfileDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="<?= $sh['user']['avatar'] ?>" class="img-fluid" alt="avatar">
                        </a>
                        <div class="dropdown-menu position-absolute" aria-labelledby="userProfileDropdown">
                            <div class="dropdown-inner">
                                <div class="user-profile-section">
                                    <div class="media mx-auto">
                                        <img src="<?= $sh['user']['avatar'] ?>" class="img-fluid mr-2" alt="avatar">
                                        <div class="media-body">
                                            <h5><?php echo $sh['user']['name'];?></h5>
                                            <p>Administrator</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="dropdown-item">
                                    <a href="<?= Sh_Link('') ?>">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg> <span> Back to Main Site</span>
                                    </a>
                                </div>
                                <div class="dropdown-item">
                                    <a href="<?= $sh['user']['username'] ?>">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg> <span> Profile</span>
                                    </a>
                                </div>
                                <div class="dropdown-item">
                                    <a href="">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-inbox"><polyline points="22 12 16 12 14 15 10 15 8 12 2 12"></polyline><path d="M5.45 5.11L2 12v6a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2v-6l-3.45-6.89A2 2 0 0 0 16.76 4H7.24a2 2 0 0 0-1.79 1.11z"></path></svg> <span> Inbox</span>
                                    </a>
                                </div>
                                <div class="dropdown-item">
                                    <a href="<?= Sh_Link('logout') ?>">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-out"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg> <span>Log Out</span>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </nav>

            <div id="compact_submenuSidebar" class="submenu-sidebar">

                <div class="submenu" id="accounts">
                    <div class="menu-title">
                        <h3>Account</h3>
                    </div>
                    <ul class="submenu-list" data-parent-element="#more">
                        <li>
                            <a href="<?= Sh_LoadAdminLinkSettings('manage-users') ?>">Manage Users </a>
                        </li>

                        <li>
                            <a href="<?= Sh_LoadAdminLinkSettings('manage-experts') ?>">Manage Experts </a>
                        </li>

                        <li>
                            <a href="<?= Sh_LoadAdminLinkSettings('subscribers') ?>">Subscribers </a>
                        </li>

                    </ul>
                </div>

                <div class="submenu" id="plugins">
                    <div class="menu-title">
                        <h3>Plugins</h3>
                    </div>
                    <ul class="submenu-list" data-parent-element="#more">
                        <li>
                            <a href="<?= Sh_LoadAdminLinkSettings('form-creator') ?>">Form Creator </a>
                        </li>

                        <li>
                            <a href="<?= Sh_LoadAdminLinkSettings('professional-plugin') ?>">Professionals</a>
                        </li>

                        <li>
                            <a href="<?= Sh_LoadAdminLinkSettings('menu-settings') ?>">Menu</a>
                        </li>

                        <li>
                            <a href="<?= Sh_LoadAdminLinkSettings('rating-system') ?>">Rating System</a>
                        </li>

                        <li>
                            <a href="<?= Sh_LoadAdminLinkSettings('category-page') ?>">Categories</a>
                        </li>

                        <li>
                            <a href="<?= Sh_LoadAdminLinkSettings('page-list') ?>">Extral Pages</a>
                        </li>


                    </ul>
                </div>

                <div class="submenu" id="more">
                    <div class="menu-title">
                        <h3>Settings</h3>
                    </div>
                    <ul class="submenu-list" data-parent-element="#more">
                        <li>
                            <a href="<?= Sh_LoadAdminLinkSettings('home-settings') ?>">Home Settings</a>
                        </li>

                        <li>
                            <a href="<?= Sh_LoadAdminLinkSettings('general-settings') ?>">General Settings</a>
                        </li>

                        <li>
                            <a href="<?= Sh_LoadAdminLinkSettings('site-setting') ?>">Site Settings</a>
                        </li>

                        <li>
                            <a href="<?= Sh_LoadAdminLinkSettings('other-settings') ?>">Other Settings</a>
                        </li>
                    </ul>
                </div>

            </div>

        </div>
        <!--  END SIDEBAR  -->

        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">

            <?php echo $page_loaded; ?>

            <div class="footer-wrapper">
                <div class="footer-section f-section-1">
                    <p class=""><?= $sh['config']['site_footer'] ?>, All rights reserved.</p>
                </div>
            </div>


        </div>
        <!--  END CONTENT AREA  -->


    </div>
    <!-- END MAIN CONTAINER -->



    <!-- END GLOBAL MANDATORY SCRIPTS -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
    <?php if ($page == 'form-builder' || $page == 'form-builder-editor' ) { ?>
    <script src="<?= Sh_LoadAdminLink('plugins/apex/apexcharts.min.js') ?>"></script>
    <script src="<?= Sh_LoadAdminLink('js/dashboard/dash_1.js') ?>"></script>
    <script src="<?= Sh_LoadAdminLink('js/widgets/modules-widgets.js') ?>"></script>

      <script src="<?= Sh_LoadAdminLink('plugins/dropify/dropify.min.js') ?>"></script>
      <script src="<?= Sh_LoadAdminLink('plugins/flatpickr/flatpickr.js') ?>"></script>
      <script src="<?= Sh_LoadAdminLink('js/apps/invoice-add.js') ?>"></script>

      <script src="<?= Sh_LoadAdminLink('plugins/bootstrap-select/bootstrap-select.min.js') ?>"></script>

      <script src="<?= Sh_LoadAdminLink('js/form-builder.js') ?>"></script>
    <?php } ?>

    <?php if ($page == 'manage-users' || $page == "manage-experts" || $page == 'subscribers' || $page == 'form-builder' || $page = 'form-builder-editor' || $page == 'professional-plugin' || $page == 'category-page' || $page == 'rating-system') { ?>
      <script src="<?= Sh_LoadAdminLink('plugins/table/datatable/datatables.js') ?>"></script>
    <?php } ?>


    <?php if ($page == "create-page" || $page == "edit-page") { ?>
      <script src="<?= Sh_LoadAdminLink('plugins/editors/quill/quill.js') ?>"></script>
    <?php } ?>



    <script>

      var websiteUrl = "<?= $sh['config']['site_url'] ? $sh['config']['site_url'] : $site_url ?>";

        $('#zero-config, #another-table').DataTable({
            "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
        "<'table-responsive'tr>" +
        "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
            "oLanguage": {
                "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
               "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7
        });

        c2 = $('#style-2').DataTable({
            headerCallback:function(e, a, t, n, s) {
                e.getElementsByTagName("th")[0].innerHTML='<label class="new-control new-checkbox checkbox-outline-primary m-auto">\n<input type="checkbox" class="new-control-input chk-parent select-customers-info" id="customer-all-info">\n<span class="new-control-indicator"></span><span style="visibility:hidden">c</span>\n</label>'
            },
            columnDefs:[ {
                targets:0, width:"30px", className:"", orderable:!1, render:function(e, a, t, n) {
                    return'<label class="new-control new-checkbox checkbox-outline-primary  m-auto">\n<input type="checkbox" class="new-control-input child-chk select-customers-info" id="customer-all-info">\n<span class="new-control-indicator"></span><span style="visibility:hidden">c</span>\n</label>'
                }
            }],
            "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
        "<'table-responsive'tr>" +
        "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
            "oLanguage": {
                "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
               "sLengthMenu": "Results :  _MENU_",
            },
            "lengthMenu": [5, 10, 20, 50],
            "pageLength": 5
        });

        multiCheck(c2);

    </script>

    <script src="<?= Sh_LoadAdminLink('js/custom.js') ?>"></script>

    <link href="<?= Sh_LoadAdminLink('css/waitMe.css') ?>" rel="stylesheet" />
    <script src="<?= Sh_LoadAdminLink('js/waitMe.js') ?>"></script>

    <script src="<?= Sh_LoadAdminLink('plugins/sweetalerts/sweetalert2.min.js') ?>"></script>

    <script src="<?= Sh_LoadAdminLink('plugins/blockui/jquery.blockUI.min.js') ?>"></script>
    <!-- Pages Javascripts -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
    <script src="<?= Sh_LoadAdminLink('js/scrollspyNav.js') ?>"></script>


</body>

</html>
