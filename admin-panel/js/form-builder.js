/*
This Simple Form Builder was Developed by
Hassan Tijani (SureCoder)
*/


console.log(pageN); //registration

if (pageN == "form-builder") {
  $(window).on('load', function() {

    getfieldNameAttr();
    optionNameAttrByClass();
    getOptionValueAttr();
    getTextfieldNameAttr();
    getEmailfieldNameAttr();
    getNumberfieldNameAttr();
    fileImgNameAttrByClass();

  });
}



// slugify
let slug = str => {

    var $slug = '';
    var trimmed = $.trim(str);
    $slug = trimmed.replace(/[^a-z0-9-]/gi, '-').
    replace(/-+/g, '_').
    replace(/^_|_$/g, '');
    return $slug.toLowerCase();

}


// text trimer
let textTrimmer = str => {
  var resultTrimmed = str.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
  return resultTrimmed;
}


//
//
// PARAGRAPH FIELD ATTR
//
//
let getfieldNameAttr = (el = null) => {

  if(typeof(el) != "undefined" && el !== null) {

    var text_val = el.closest('#f_title').find('.question-text').val();
    // set new name as field name
    el.closest('#f_title').siblings('div').find('#text_area').attr('name', slug(text_val) );

    return slug(text_val);

  }else{

    var $fNA = $(".question-text");

    var get_rowid = $($fNA).closest('#f_title').siblings('div').find('#text_area').attr("data-rowid");

    // set new name as field name
    $($fNA).closest('#f_title').siblings('div').find('#text_area').attr('name', slug($fNA.val()) );

    return slug($fNA.val());

  }

}

let changedFieldNameAttr = () => {

  $(document).on('blur keyup input paste cut keypress', '.question-text', function () {
      var $this = $(this);

      // get row id
      var get_rowid = $(this).closest('#f_title').siblings('div').find('#text_area').attr("data-rowid");

      // set new name as field name
      $(this).closest('#f_title').siblings('div').find('#text_area').attr('name', slug($this.val()) );

      return slug($this.val());
  });

}



//
//
// TEXT FIELD ATTR
//
//
let getTextfieldNameAttr = (el = null) => {

  if(typeof(el) != "undefined" && el !== null) {

    var text_val = el.closest('#f_title').find('.question-text').val();
    // set new name as field name
    el.closest('#f_title').siblings('div').find('#text_field').attr('name', slug(text_val) );

    return slug(text_val);

  }else{

    var $fNA = $(".question-text");

    var get_rowid = $($fNA).closest('#f_title').siblings('div').find('#text_field').attr("data-rowid");

    // set new name as field name
    $($fNA).closest('#f_title').siblings('div').find('#text_field').attr('name', slug($fNA.val()) );

    return slug($fNA.val());

  }

}

let changedTextFieldNameAttr = () => {

  $(document).on('blur keyup input paste cut keypress', '.question-text', function () {
      var $this = $(this);

      // get row id
      var get_rowid = $(this).closest('#f_title').siblings('div').find('#text_field').attr("data-rowid");

      // set new name as field name
      $(this).closest('#f_title').siblings('div').find('#text_field').attr('name', slug($this.val()) );

      return slug($this.val());
  });

}



//
//
// EMAIL FIELD ATTR
//
//
let getEmailfieldNameAttr = (el = null) => {

  if(typeof(el) != "undefined" && el !== null) {

    var text_val = el.closest('#f_title').find('.question-text').val();
    // set new name as field name
    el.closest('#f_title').siblings('div').find('#email_field').attr('name', slug(text_val) );

    return slug(text_val);

  }else{

    var $fNA = $(".question-text");

    var get_rowid = $($fNA).closest('#f_title').siblings('div').find('#email_field').attr("data-rowid");

    // set new name as field name
    $($fNA).closest('#f_title').siblings('div').find('#email_field').attr('name', slug($fNA.val()) );

    return slug($fNA.val());

  }

}

let changedEmailFieldNameAttr = () => {

  $(document).on('blur keyup input paste cut keypress', '.question-text', function () {
      var $this = $(this);

      // get row id
      var get_rowid = $(this).closest('#f_title').siblings('div').find('#email_field').attr("data-rowid");

      // set new name as field name
      $(this).closest('#f_title').siblings('div').find('#email_field').attr('name', slug($this.val()) );

      return slug($this.val());
  });

}



//
//
// EMAIL FIELD ATTR
//
//
let getNumberfieldNameAttr = (el = null) => {

  if(typeof(el) != "undefined" && el !== null) {

    var text_val = el.closest('#f_title').find('.question-text').val();
    // set new name as field name
    el.closest('#f_title').siblings('div').find('#number_field').attr('name', slug(text_val) );

    return slug(text_val);

  }else{

    var $fNA = $(".question-text");

    var get_rowid = $($fNA).closest('#f_title').siblings('div').find('#number_field').attr("data-rowid");

    // set new name as field name
    $($fNA).closest('#f_title').siblings('div').find('#number_field').attr('name', slug($fNA.val()) );

    return slug($fNA.val());

  }

}


let changedNumberFieldNameAttr = () => {

  $(document).on('blur keyup input paste cut keypress', '.question-text', function () {
      var $this = $(this);

      // get row id
      var get_rowid = $(this).closest('#f_title').siblings('div').find('#number_field').attr("data-rowid");

      // set new name as field name
      $(this).closest('#f_title').siblings('div').find('#number_field').attr('name', slug($this.val()) );

      return slug($this.val());
  });

}




//
//
//
// RADIO FIELD ATTR
//
//
//
let optionNameAttrByClass = (el = null) => {

  if(typeof(el) != "undefined" && el !== null) {

    var text_val = el.closest('#f_title').find('.question-text').val();
    // set new name as field name
    el.closest('#f_title').siblings('div').find('.inptclass').attr('name', slug(text_val) );

    return slug(text_val);

  }else{

    var $fNA = $(".question-text");

    var get_optrowid = $($fNA).closest('#f_title').siblings('div').find('.inptclass').attr("data-optid");

    // set new name as field name
    $($fNA).closest('#f_title').siblings('div').find('.inptclass').attr('name', slug($fNA.val()) );

    return slug($fNA.val());

  }


}

let getOptionValueAttr = () => {

  var $this = $(this);

  // get row id
  var get_optrowid = $(this).closest('.formRadio').find('.inptclass').attr("data-optid");

  // set new name as field name   (textTrimmer supposed to be used)
  $(this).closest('.formRadio').find('.inptclass').attr('value', slug($this.text()) );

  return $this.text();

}

let changedOptionNameAttr = () => {

  $(document).on('blur keyup input paste cut keypress', '.question-text', function () {
      var $this = $(this);

      // get row id
      var get_optrowid = $(this).closest('#f_title').siblings('div').find('.inptclass').attr("data-optid");

      // set new name as field name
      $(this).closest('#f_title').siblings('div').find('.inptclass').attr('name', slug($this.val()) );

      return slug($this.val());

  });

}

let changeOptionValueAttr = () => {

  $(document).on('blur keyup input paste cut keypress', '.radioLabel', function () {
      var $this = $(this);

      // get row id
      var get_optrowid = $(this).closest('.formRadio').find('.inptclass').attr("data-optid");

      // set new name as field name     (textTrimmer supposed to be used)
      $(this).closest('.formRadio').find('.inptclass').attr('value', slug($this.text()) ) ;

  });

}



//
//
//
//  IMAGE NAME ATTR
//
//
//

let fileImgNameAttrByClass = (el = null) => {

  if(typeof(el) != "undefined" && el !== null) {

    var text_val = el.closest('#f_title').find('.question-text').val();
    // set new name as field name
    el.closest('#f_title').siblings('div').find('#imgfile_field').attr('name', slug(text_val) );

    return slug(text_val);

  }else{

    var $fNA = $(".question-text");

    var get_optrowid = $($fNA).closest('#f_title').siblings('div').find('#imgfile_field').attr("data-optid");

    // set new name as field name
    $($fNA).closest('#f_title').siblings('div').find('#imgfile_field').attr('name', slug($fNA.val()) );

    return slug($fNA.val());

  }


}

let changedImgFileNameAttr = () => {

  $(document).on('blur keyup input paste cut keypress', '.question-text', function () {
      var $this = $(this);

      // get row id
      var get_optrowid = $(this).closest('#f_title').siblings('div').find('#imgfile_field').attr("data-optid");

      // set new name as field name
      $(this).closest('#f_title').siblings('div').find('#imgfile_field').attr('name', slug($this.val()) );

      return slug($this.val());

  });

}

$(function() {

    // check if field title changed and return the values
    changedFieldNameAttr();

    changedTextFieldNameAttr();

    changedEmailFieldNameAttr();

    changedNumberFieldNameAttr();

    changedOptionNameAttr();

    changeOptionValueAttr();

    changedImgFileNameAttr();

    $('#add_q-item').click(function(e) {

        var el = $('#q-item-clone').clone()

        var f_arr = []
        $('#question-field .question-item').each(function() {
            f_arr.push(parseInt($(this).attr('data-item')))
        })
        var i = f_arr.length
        // console.log(i)

        el.find('.question-item').attr('data-item', i)
        el.find('#text_area').attr('data-rowid', (i+1))
        // el.find('textarea').attr('name', 'q[' + i + ']')

        //
        //
        // ADDITIONAL CODES
        //
        //
        let f_val = "Edit Field Name "+(i+1);
        el.find('#field_title').attr('value', f_val)
        el.find('#text_field').attr('name', slug(f_val));
        el.find('#text_field').attr('data-editname', slug(f_val));
        el.find('#text_field').attr('class', 'form-control col-sm-12 bulderInputs');
        // continue existing codes
        $('#question-field').append(el.html())
        $('body,html').animate({ scrollTop: $(this).offset().top }, 'fast')

        _initilize()
    })

     $('#question-field').sortable({
          handle: '.item-move',
          classes: {
              "ui-sortable": "highlight"
          }
      })
    // $("#question-field").disableSelection();





    //
    function _initilize() {

        // $('[contenteditable="true"]').each(function() {
        //     $(this).on("blur focusout", function() {
        //         if ($(this).text() == "") {
        //             $(this).text($(this).attr("title"))
        //         }
        //     })
        //
        // })

        $('.question-item .form-check').find('label').on('keypress keyup paste cut', function() {
            $(this).siblings('input').val($(this).text())
        })

        $('.question-item .req-chk').click(function() {
            if ($(this).siblings('input[type="checkbox"]').is(":checked") == true) {
                $(this).siblings('input[type="checkbox"]').prop("checked", false).trigger("change")
            } else {
                $(this).siblings('input[type="checkbox"]').prop("checked", true).trigger("change")
            }
        })

        $('.rem-q-item').click(function() {

            loader(800,0);

            var fname = $(this).closest('.question-item').find('.bulderInputs').attr('name');

            delete_column(fname);

            $(this).closest('.question-item').remove();

            update_form()

        })

        $('.req-item').change(function() {
            var _parent = $(this).closest('.question-item')
            if ($(this).is(":checked") == true) {
                _parent.find("input").attr('required', true)
                _parent.find("textarea").first().attr('required', true)
                $(this).attr('checked', true)
            } else {
                _parent.find("input").attr('required', false)
                _parent.find("textarea").first().attr('required', false)
                $(this).attr('checked', false)
            }
        })

        $('.choice-option').change(function() {

            var choice = $(this).val()
            var _field = $(this).closest('.question-item').attr('data-item')

            if (choice == "text") {

                TextField($(this), _field)
                getTextfieldNameAttr($(this))

            }else if(choice == "email"){

                EmailField($(this), _field)
                getEmailfieldNameAttr($(this))

            }else if(choice == "number"){

                NumberField($(this), _field)
                getNumberfieldNameAttr($(this))

            }else if (choice == "p") {

                paragraph($(this), _field)
                getfieldNameAttr($(this))

            } else if (choice == "checkbox") {

                // $(this).closest('.question-item').find('.choice-field').html('<button type="button" class="add_chk btn btn-sm btn-default border"><i class="fa fa-plus"></i> Add option</button>')
                // add_checkbox()
                // for (var i = 0; i < 2; i++) {
                //     checkbox_field($(this), _field, "Enter Option")
                // }

            } else if (choice == "radio") {

                $(this).closest('.question-item').find('.choice-field').html('<button type="button" class="add_radio btn btn-sm btn-default border"><i class="fa fa-plus"></i> Add option</button>')
                add_radio()
                for (var i = 0; i < 2; i++) {
                    radio_field($(this), _field, "Enter Option "+ (i+1), (i+1))
                    optionNameAttrByClass($(this));
                }

            } else if (choice == "file") {

                file_field($(this), _field)
                fileImgNameAttrByClass($(this));
            }
            $(this).closest('.question-item').find('.req-item').trigger('change')
        })

    }



    // function add_checkbox() {
    //     $('.add_chk').click(function() {
    //         var _this = $(this)
    //         var _field = _this.closest('.question-item').attr('data-item')
    //         checkbox_field(_this, _field, "Enter Option")
    //     })
    // }

    function add_radio() {

        $('.add_radio').click(function() {
            var _this = $(this)
            var _field = _this.closest('.question-item').attr('data-item')

            var f_arr = []
            $('.choice-field .inptclass').each(function() {
                f_arr.push(parseInt(_this.attr('data-optid')))
            })
            var i = f_arr.length

            radio_field($(this), _field, "Enter Option "+ (i+1), (i+1))

            var text_val = $(this).closest('.card-body').find('.question-text').val();
            // set new name as field name
            $(this).closest('.card-body').find('.inptclass').attr('name', slug(text_val) );

        })
    }

    function paragraph(_this, _field) {

        var el = $('<textarea>')
        el.attr({
            "cols": 30,
            "rows": 5,
            "id": "text_area",
            "data-editname": "",
            "placeholder": "Write your answer here",
            "class": "form-control col-sm-12 bulderInputs"
        })
        _this.closest('.question-item').find('.choice-field').html(el)
    }

    function TextField(_this, _field){
        var el = $('<input>')
        el.attr({
          "type": "text",
          "id": "text_field",
          "data-editname": "",
          "class": "form-control col-sm-12 bulderInputs",
          "placeholder": "write your answer here"
        })
        _this.closest('.question-item').find('.choice-field').html(el)
    }

    function EmailField(_this, _field){
      var el = $('<input>')
        el.attr({
          "type": "email",
          "id": "email_field",
          "data-editname": "",
          "class": "form-control col-sm-12 bulderInputs",
          "placeholder": "write your answer here"
        })
      _this.closest('.question-item').find('.choice-field').html(el)

    }

    function NumberField(_this, _field){
      var el = $('<input>')
        el.attr({
          "type": "number",
          "id": "number_field",
          "data-editname": "",
          "class": "form-control col-sm-12 bulderInputs",
          "placeholder": "write your answer here"
        })
        _this.closest('.question-item').find('.choice-field').html(el)

    }

    function file_field(_this, _field) {
        var el = $('<input>')
        el.attr({
            "type": "file",
            "name": "",
            "data-editname": "",
            "id": "imgfile_field",
            "class": "form-control-file bulderInputs"
        })
        _this.closest('.question-item').find('.choice-field').html(el)
    }

    // function checkbox_field(_this, _field, _text = "option") {
    //     var chk = $("<div>")
    //     var rem = $("<div>")
    //     chk.attr({
    //         "class": "col-sm-11 d-flex align-items-center",
    //     })
    //     rem.attr({
    //         "class": "col-sm-1 rem-on-display",
    //     })
    //     rem.append("<button class='btn btn-sm btn-default' type='button'><span class='fa fa-times'></span></button>")
    //     rem.attr('onclick', "$(this).closest('.row').remove()")
    //     var item = create_checkbox_field(_field, _text)
    //     chk.append(item)
    //     el = $("<div class='row w-100'>")
    //     el.append(rem)
    //     el.append(chk)
    // }

    function radio_field(_this, _field, _text = "option", optid) {

        var chk = $("<div>")
        var rem = $("<div>")
        chk.attr({
            "class": "col-sm-11 d-flex align-items-center",
        })
        rem.attr({
            "class": "col-sm-1 rem-on-display",
        })
        rem.append("<button class='btn btn-sm btn-default' type='button'><span class='fa fa-times'></span></button>")
        rem.attr('onclick', "$(this).closest('.row').remove()")
        // let ondf = optionNameAttrByClass(_this);
        var item = create_radio_field(_field, _text, optid)
        chk.append(item)
        el = $("<div class='row w-100'>")
        el.append(rem)
        el.append(chk)
        _this.closest('.question-item').find('.choice-field .add_radio').before(el)


    }
    //
    //
    //
    // function create_checkbox_field(_field, _text) {
    //
    //     var el = $('<div>')
    //     el.attr({
    //         "class": "form-check q-fc"
    //     })
    //     var inp = $('<input>')
    //     inp.attr({
    //         "class": "form-check-input",
    //         "name": "q[" + _field + "][]",
    //         "type": "checkbox",
    //         "value": _text
    //     })
    //     var label = $('<label>')
    //     label.attr({
    //         "class": "form-check-label",
    //         "contenteditable": true,
    //         "title": "Enter option here"
    //     })
    //     label.text(_text)
    //     el.append(inp)
    //     el.append(label)
    //     return el
    // }

    function create_radio_field(_field, _text, optid) {

        var el = $('<div>')
        el.attr({
            "class": "form-check q-fc formRadio"
        })
        var inp = $('<input>')
        inp.attr({
            "class": "form-check-input inptclass bulderInputs",
            "type": "radio",
            "data-editname": "",
            "data-optid": optid,
            "value": slug(_text)
        })
        var label = $('<label>')
        label.attr({
            "class": "form-check-label radioLabel",
            "contenteditable": true,
            "title": "Enter option here"
        })
        label.text(_text)
        el.append(inp)
        el.append(label)
        return el
    }

    _initilize()
    //

    // Save new form data
    function save_form() {

        var new_el = $('<div>')
        var form_el = $('#form-field').clone()
        var form_code = $("[name='form_code']").length > 0 ? $("[name='form_code']").val() : "";
        var title = $('#form-title').text()
        var description = $('#form-description').text()
        form_el.find("[name='form_code']").remove()
        new_el.append(form_el)

        var values = [];
        $('.bulderInputs').each(function(){
            values.push({ name: this.name, type: this.type, require: this.required, editedName: $(this).attr('data-editname'), option: this.value});
        });

        //use values after the loop
        // console.log(JSON.stringify(values));

      var output = [];

      let arraySum = (data) => {

          data.forEach(function(item) {

            var existing = output.filter(function(v, i) {
              return v.name == item.name;
            });

            if (existing.length) {

              var existingIndex = output.indexOf(existing[0]);
              output[existingIndex].option = output[existingIndex].option.concat(item.option);

            } else {
              if (typeof item.option == 'string')
                item.option = [item.option];
              output.push(item);
            }
          });

          console.dir(JSON.stringify(output));

        }


        arraySum(values);

        loader(800,0);

        // get page form data
        var formTitle = $("#formTitle").val();
        var formDescription = $("#formDescription").val();
        var formCode = $("#formCode").val();
        var pageType = $("#page_type").val();

        let formD = $('#form-data');
        var formData1 = new FormData(document.getElementById('form-data'));

        formData1.append("description", formDescription);
        formData1.append("pageType", pageType);
        formData1.append("title", formTitle);
        formData1.append("form_code", formCode);
        formData1.append("formValues", JSON.stringify(output));
        formData1.append("form_data", new_el.html());

        $.ajax({
            url: Sh_Ajax_Requests_File() + '?f=form_builder&s=make_form',
            method: 'POST',
            data: formData1,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            // error: err => {
            //     console.log(err)
            //     alert("an error occured 1")
            // },
            beforeSend: function(){
              $('#save_form').attr('disabled', true).text("Processing....")
            },
            success: function(resp) {

                loader(800,400);

                $('#save_form').attr('disabled', false).html('<i class="fa fa-save"></i> Save Form');

                if (resp.status == 200) {

                    sureNotify(1,resp.message);

                } else {

                    sureNotify(0,resp.error);

                    console.log(resp)

                }

            }
        })
    }

    // Update form data
    function update_form() {

        var new_el = $('<div>')
        var form_el = $('#form-field').clone()
        var form_code = $("[name='form_code']").length > 0 ? $("[name='form_code']").val() : "";
        var title = $('#form-title').text()
        var description = $('#form-description').text()
        form_el.find("[name='form_code']").remove()
        new_el.append(form_el)

        var values = [];
        $('.bulderInputs').each(function(){
            values.push({ name: this.name, type: this.type, require: this.required, editedName: $(this).attr('data-editname'), option: this.value});
        });

        //use values after the loop
        // console.log(JSON.stringify(values));

      var output = [];

      let arraySum = (data) => {

          data.forEach(function(item) {
            var existing = output.filter(function(v, i) {
              return v.name == item.name;
            });
            if (existing.length) {
              var existingIndex = output.indexOf(existing[0]);
              output[existingIndex].option = output[existingIndex].option.concat(item.option);
            } else {
              if (typeof item.option == 'string')
                item.option = [item.option];
              output.push(item);
            }
          });

          console.dir(JSON.stringify(output));

        }


        arraySum(values);

        loader(800,0);

        // get page form data
        var formTitle = $("#formTitle").val();
        var formDescription = $("#formDescription").val();
        var formCode = $("#formCode").val();
        var pageType = $("#page_type").val();
        var user_id = $("#user_id").val();

        let formD = $('#edit-form-data');
        var formData2 = new FormData(document.getElementById('edit-form-data'));

        formData2.append("description", formDescription);
        formData2.append("pageType", pageType);
        formData2.append("title", formTitle);
        formData2.append("form_code", formCode);
        formData2.append("formValues", JSON.stringify(output));
        formData2.append("form_data", new_el.html());
        formData2.append("user_id", user_id);

        $.ajax({
            url: Sh_Ajax_Requests_File() + '?f=form_builder&s=update_form',
            method: 'POST',
            data: formData2,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
              $('#update_form').attr('disabled', true).text("Processing....")
            },
            success: function(resp) {

                loader(800,400);

                $('#update_form').attr('disabled', false).html('<i class="fa fa-save"></i> Update Form');

                if (resp.status == 200) {

                    sureNotify(1,resp.message);

                } else {

                    sureNotify(0,resp.error);

                    console.log(resp)

                }

            }
        })
    }

    // delete column from table in database
    function delete_column(colName){

      var page_type = $("#page_type").val();
      var page_code = $("#formCode").val();

      $.ajax({
          url: Sh_Ajax_Requests_File() + '?f=form_builder&s=delete_column',
          method: 'POST',
          data: {column: colName, page_type: page_type, page_code: page_code},
          dataType: 'json',
          success: function(resp) {

              loader(800,400);

              if (resp.status == 200) {

                  sureNotify(1,resp.message);

              } else {

                  sureNotify(0,resp.message);

              }

          }
      })
    }

    // save new form triggering button
    $('#save_form').click(function() {
        save_form()
    });

    // Update form triggering button
    $('#update_form').click(function() {
        update_form()
    });


})
